// This is the main DLL file.

#include "stdafx.h"

#include "edge-drawing-wrapper.h"

int* edgedrawingwrapper::Edgedrawingwrapper::WrapperGrayEDV(unsigned char *srcImg, int width, int height, int GRADIENT_THRESH){	
	EdgeMap* map = GrayEDV(srcImg, width, height, PREWITT_OPERATOR, GRADIENT_THRESH);

	int size = 1;             // Number of segments

	size += map->noSegments;  // The left of each segment

	for (int i = 0; i<map->noSegments; i++) size += map->segments[i].noPixels * 2;  // (r,c) pairs for each segment

	int *buffer = new int[size];

	int index = 0;

	buffer[index++] = map->noSegments;

	for (int i = 0; i<map->noSegments; i++) buffer[index++] = map->segments[i].noPixels;

	for (int i = 0; i<map->noSegments; i++){

		for (int k = 0; k<map->segments[i].noPixels; k++){

			buffer[index++] = map->segments[i].pixels[k].r;

			buffer[index++] = map->segments[i].pixels[k].c;

		} //end-for

	} //end-for

	delete map;

	return buffer;
}


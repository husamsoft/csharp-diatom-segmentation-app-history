#pragma once

using namespace System;

// Edge Drawing
// Header Files
#include "../cplusplus-edge-drawing/edge-drawing/ED.h"
#include "../cplusplus-edge-drawing/edge-drawing/EDInternals.h"
#include "../cplusplus-edge-drawing/edge-drawing/GradientOperators.h"
#include "../cplusplus-edge-drawing/edge-drawing/EdgeMap.h"
#include "../cplusplus-edge-drawing/edge-drawing/EdgeSegments.h"
#include "../cplusplus-edge-drawing/edge-drawing/ValidateEdgeSegments.h"
// Cpp Files
#include "../cplusplus-edge-drawing/edge-drawing/ED.cpp"
#include "../cplusplus-edge-drawing/edge-drawing/EDInternals.cpp"
#include "../cplusplus-edge-drawing/edge-drawing/GradientOperators.cpp"
#include "../cplusplus-edge-drawing/edge-drawing/EdgeSegments.cpp"
#include "../cplusplus-edge-drawing/edge-drawing/ValidateEdgeSegments.cpp"

namespace edgedrawingwrapper {
	public struct PixelWrapper { public: int r, c; };

	public struct EdgeSegmentWrapper {
	public:
		PixelWrapper* pixels;       // Pointer to the pixels array
		int noPixels;        // # of pixels in the edge map
	};
	public ref class Edgedrawingwrapper	
	{	
	public:
		int* WrapperGrayEDV(unsigned char *src, int width, int height, int GRADIENT_THRESH);
	};
}

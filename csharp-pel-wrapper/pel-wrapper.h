// pel-wrapper.h

#pragma once

using namespace System;


// PEL
// Header Files
#include "../cplusplus-pel/EdgeMap.h";
#include "../cplusplus-pel/PEL.h";


// Cpp Files
#include "../cplusplus-pel/PEL.cpp";

namespace pelwrapper {

	public ref class PelWrapper
	{
	public:
		int* execute(unsigned char* bem, int width, int height, int MIN_SEGMENT_LEN);
	};
}

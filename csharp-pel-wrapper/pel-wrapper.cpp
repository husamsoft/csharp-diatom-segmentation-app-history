// This is the main DLL file.

#include "stdafx.h"

#include "pel-wrapper.h"


int* pelwrapper::PelWrapper::execute(unsigned char* bem, int width, int height, int MIN_SEGMENT_LEN){
	
	EdgeMap *map = PEL(bem, width, height, MIN_SEGMENT_LEN);

	//SplitEdgeSegments(map);

	int size = 1;             // Number of segments

	size += map->noSegments;  // The left of each segment

	for (int i = 0; i<map->noSegments; i++) size += map->segments[i].noPixels * 2;  // (r,c) pairs for each segment

	int *buffer = new int[size];

	int index = 0;

	buffer[index++] = map->noSegments;

	for (int i = 0; i<map->noSegments; i++) buffer[index++] = map->segments[i].noPixels;

	for (int i = 0; i<map->noSegments; i++){

		for (int k = 0; k<map->segments[i].noPixels; k++){

			buffer[index++] = map->segments[i].pixels[k].r;

			buffer[index++] = map->segments[i].pixels[k].c;

		} //end-for

	} //end-for

	delete map;

	return buffer;	
}


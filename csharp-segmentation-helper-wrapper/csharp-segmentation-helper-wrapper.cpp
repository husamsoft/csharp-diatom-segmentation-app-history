// This is the main DLL file.

#include "stdafx.h"

#include "csharp-segmentation-helper-wrapper.h"


int* csharpsegmentationhelperwrapper::SegmentationWrapper::Extract(unsigned char *srcImg, int width, int height, int* pixelsWrapper, int noPixels){
	
	Pixel* pixels = new Pixel[noPixels];

	int index = 0;
	for (int i = 0; i < noPixels; i++){
		pixels[i].r = pixelsWrapper[index++];
		pixels[i].c = pixelsWrapper[index++];
	}
	
	ROI* roi = ExtractROI(srcImg, width, height, pixels, noPixels);	

	int roiWidth = roi->width;
	int roiHeight = roi->height;

	int size = roiWidth * roiHeight + 8;	

	int *buffer = new int[size];	

	index = 0;
	buffer[index++] = roi->width;
	buffer[index++] = roi->height;
	buffer[index++] = roi->cx;
	buffer[index++] = roi->cy;
	buffer[index++] = roi->left;
	buffer[index++] = roi->right;
	buffer[index++] = roi->up;
	buffer[index++] = roi->down;

	for (int i = 0; i < roiWidth * roiHeight; i++){
		buffer[index++] = roi[0].roiImg[i];
	}

	return buffer;
	
}


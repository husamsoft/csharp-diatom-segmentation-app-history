// csharp-segmentation-helper-wrapper.h

#pragma once

using namespace System;

namespace csharpsegmentationhelperwrapper {


	#include "../csharp-segmentation-helper/csharp-segmentation-helper/csharp-segmentation-helper.h";	
	#include "../csharp-segmentation-helper/csharp-segmentation-helper/csharp-segmentation-helper.cpp";	

	public struct PixelWrapper { int r, c; };


	public ref class SegmentationWrapper
	{

	public:
		int* Extract(unsigned char *srcImg, int width, int height, int* pixelsWrapper, int noPixels);
	};
}

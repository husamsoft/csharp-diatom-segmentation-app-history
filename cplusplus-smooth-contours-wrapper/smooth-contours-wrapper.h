// smooth-contours-wrapper.h

#pragma once

using namespace System;

#include "../cplusplus-smooth-contours/smooth_contours.h";
#include "../cplusplus-smooth-contours/smooth_contours.c";

namespace SCWrapper {

	public ref class ContourOperations
	{
	public:
		int* GetSmoothContours(double* image, int width, int height, double q);
	};
	
}

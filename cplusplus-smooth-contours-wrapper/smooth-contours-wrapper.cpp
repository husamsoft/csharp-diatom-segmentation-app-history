// This is the main DLL file.

#include "stdafx.h"

#include "smooth-contours-wrapper.h"

int* SCWrapper::ContourOperations::GetSmoothContours(double* image, int width, int height, double q)
{		
	double * x ;
	double * y;
	int N; //= (int *)malloc(width * sizeof(int));
	int M; //= (int *)malloc(height * sizeof(int));
	int * curve_limits;

	smooth_contours(&x, &y, &N, &curve_limits, &M, image, width, height, q);

	int *serial = new int[width * height];
	
	/*
	for (int i = 0; i < width * height; i++)
	{
		serial[i] = i;		 
	}*/

	return serial;	
}


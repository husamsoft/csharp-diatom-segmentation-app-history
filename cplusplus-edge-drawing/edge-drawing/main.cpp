#include <exception>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "EdgeMap.h"
#include "ED.h"

using namespace std;

int main() {

    for (int k = 0; k < 100; k++) {

        int width = rand() % 400 + 5;
        int height = rand() % 300 + 5;

        unsigned char *im = new unsigned char[width * height];

        //srand(time(NULL));

        for (int i = 0; i < width * height; i++) {

            im[i] = rand() % 255;
        }

        EdgeMap *map = GrayEDV(im, width, height, PREWITT_OPERATOR, 48);

        //map->ConvertEdgeSegments2EdgeImg();

        unsigned char *edgeImg = map->edgeImg;

        printf("%d segmen sayisi %d \n", k, map->noSegments);

        delete im;
        delete  map;

    }
    return 0;
}
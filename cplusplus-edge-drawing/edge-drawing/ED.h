#ifndef _ED_H_
#define _ED_H_

#include "EdgeMap.h"

///-------------------------- GRAY ED BELOW ------------------------------------------------
/// Detect Edges by Edge Drawing (ED). smoothingSigma must be >= 1.0
EdgeMap *GrayED(unsigned char *srcImg, int width, int height, GradientOperator op=PREWITT_OPERATOR, int GRADIENT_THRESH=20, int ANCHOR_THRESH=4, double smoothingSigma=1.0);

/// Detect Edges by Edge Drawing (ED) and validate the resulting edge segments. smoothingSigma must be >= 1.0
EdgeMap *GrayEDV(unsigned char *srcImg, int width, int height, GradientOperator op=PREWITT_OPERATOR, int GRADIENT_THRESH=20);

/// Detect Edges by Edge Drawing Parameter Free (EDPF). smoothingSigma must be >= 1.0
EdgeMap *GrayEDPF(unsigned char *srcImg, int width, int height, double smoothingSigma=1.0);

///-------------------------- COLOR ED BELOW ------------------------------------------------
/// Detect Edges by Edge Drawing (ED) for color images represented by ch1Img, ch2Img and ch3Img. Uses the multi-image gradient method by DiZenzo for gradient computation. smoothingSigma must be >= 1.0
EdgeMap *ColorED(unsigned char *ch1Img, unsigned char *ch2Img, unsigned char *ch3Img, int width, int height, int GRADIENT_THRESH=20, int ANCHOR_THRESH=4, double smoothingSigma=1.5);

/// ColorED with Validation. smoothingSigma must be >= 1.0
EdgeMap *ColorEDV(unsigned char *ch1Img, unsigned char *ch2Img, unsigned char *ch3Img, int width, int height, int GRADIENT_THRESH=20, double smoothingSigma=1.5);

/// ColorEDPF. smoothingSigma must be >= 1.0
EdgeMap *ColorEDPF(unsigned char *ch1Img, unsigned char *ch2Img, unsigned char *ch3Img, int width, int height, double smoothingSigma=1.0);

///--------------------------- Contour Detection specific functions below -------------------------
/// Run ED over srcImg, but use contourImg to determine the boundaries of the edge areas: Used for grayscale images
EdgeMap *DetectContourEdgeMapByED1(unsigned char *srcImg, unsigned char *contourImg, int width, int height, int CONTOUR_THRESH, int GRADIENT_THRESH, int ANCHOR_THRESH);

/// Run ED over "ch1Img+ch2Img+ch3Img", but use contourImg to determine the boundaries of the edge areas: Used for color images
EdgeMap *DetectContourEdgeMapByED1(unsigned char *ch1Img, unsigned char *ch2Img, unsigned char *ch3Img, unsigned char *contourImg, int width, int height, int CONTOUR_THRESH, int GRADIENT_THRESH, int ANCHOR_THRESH);

#endif
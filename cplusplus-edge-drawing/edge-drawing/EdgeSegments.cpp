#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <memory.h>
#include <math.h>

#include "EdgeSegments.h"



///-------------------------------------------------------------
/// Dump edge segments to file
///
void DumpEdgeSegments2File(EdgeMap *map, char *fname){
  FILE *fp = fopen(fname, "w");

  for (int i=0; i<map->noSegments; i++){
    fprintf(fp, "\n+===SEGMENT: %3d. NoPixels: %d ====+\n", i, map->segments[i].noPixels);

    fprintf(fp, "+------+------+-------+\n");
    fprintf(fp, "|Column| Row  |Segment|\n"); 
    fprintf(fp, "+------+------+-------+\n");

    for (int j=0; j<map->segments[i].noPixels; j++){      
      fprintf(fp, "|%6d|%6d|%7d|\n", map->segments[i].pixels[j].c,  map->segments[i].pixels[j].r, i);     
    } //end-for

    fprintf(fp, "+------+------+-------+\n");
  } //end-for

  fclose(fp);
} //end-DumpEdgeSegments2File2

///----------------------------------------------------------------------------------------------
/// Splits segments into multiple segments if they contain closed objects in them
/// 
void SplitEdgeSegments(EdgeMap *map){
  int noSegments = map->noSegments;

  for (int i=0; i<map->noSegments; i++){
    int sr = map->segments[i].pixels[0].r;
    int sc = map->segments[i].pixels[0].c;

    int er = map->segments[i].pixels[map->segments[i].noPixels-1].r;
    int ec = map->segments[i].pixels[map->segments[i].noPixels-1].c;

    bool withinNewSegment = false;
    int len = 3;
    int j = 3;
    int noPixels = map->segments[i].noPixels;
    while (j<noPixels-3){
      int r = map->segments[i].pixels[j].r;
      int c = map->segments[i].pixels[j].c;    
      len++;
      j++;

      int dr1 = abs(r-sr);
      int dc1 = abs(c-sc);

      int dr2 = abs(r-er);
      int dc2 = abs(c-ec);
      if ((dr1<=1 &&dc1<=1) || (dr2<=1 && dc2<=1)){
        if (withinNewSegment == false) map->segments[i].noPixels = len;
        else {map->segments[noSegments].noPixels = len; noSegments++;}

        map->segments[noSegments].pixels = &map->segments[i].pixels[j];

        withinNewSegment = true;
        len = 3;
        j += 3;
      } //end-if
    } //end-while

    if (j < noPixels) len += noPixels-j;
    else              len -= j-noPixels;

    if (len > 1 && withinNewSegment){map->segments[noSegments].noPixels = len; noSegments++;}
  } //end-for

  map->noSegments = noSegments;
} //end-SplitEdgeSegments


///-------------------------------------------------------------------------------------------
/// Thins down edge segment by removing superfulous pixels in the chain. For example:
///   xx       x 
///  xx   --> x
/// xx       x  
///
///
void ThinEdgeSegments(EdgeMap *map, int MIN_SEGMENT_LEN){
  // Thin the edge segments
  int noSegments = 0;
  for (int i=0; i<map->noSegments; i++){
    int index = 0;

    for (int j=2; j<map->segments[i].noPixels; j++){
      int dx = abs(map->segments[i].pixels[index].c - map->segments[i].pixels[j].c);
      int dy = abs(map->segments[i].pixels[index].r - map->segments[i].pixels[j].r);
      
      if (dx >= 2 || dy >= 2){
//      if (dx+dy >= 2){
        map->segments[i].pixels[++index] = map->segments[i].pixels[j-1];
      } // end-if
    } //end-for

    // Copy the last pixel
    map->segments[i].pixels[++index] = map->segments[i].pixels[map->segments[i].noPixels-1];
    map->segments[i].noPixels = index+1;

    if (map->segments[i].noPixels >= MIN_SEGMENT_LEN) map->segments[noSegments++] = map->segments[i];
  } //end-for

  map->noSegments = noSegments;
} //end-ThinEdgeSegments

///-------------------------------------------------------------------------------------------
/// Thins down edge segment by removing superfulous pixels in the chain. For example:
///   xx       x 
///  xx   --> x
/// xx       x  
///
///
void ThinEdgeSegments2(EdgeMap *map, int MIN_SEGMENT_LEN){
  // Thin the edge segments
  int noSegments = 0;
  for (int i=0; i<map->noSegments; i++){
    int index = 0;

    for (int j=2; j<map->segments[i].noPixels; j++){
      int dx = abs(map->segments[i].pixels[index].c - map->segments[i].pixels[j].c);
      int dy = abs(map->segments[i].pixels[index].r - map->segments[i].pixels[j].r);
      
      if (dx >= 2 || dy >= 2){
        if (!(map->segments[i].pixels[index].r == map->segments[i].pixels[j-2].r && map->segments[i].pixels[index].c == map->segments[i].pixels[j-2].c)){
          if  ((dx == 1 && dy == 2) || (dx == 2 && dy == 1)) map->segments[i].pixels[++index] = map->segments[i].pixels[j-2];
        } //end-if

        map->segments[i].pixels[++index] = map->segments[i].pixels[j-1];
      } // end-if
    } //end-for

    // Copy the last pixel
    map->segments[i].pixels[++index] = map->segments[i].pixels[map->segments[i].noPixels-1];
    map->segments[i].noPixels = index+1;

    if (map->segments[i].noPixels >= MIN_SEGMENT_LEN) map->segments[noSegments++] = map->segments[i];
  } //end-for

  map->noSegments = noSegments;
} //end-ThinEdgeSegments2

///-------------------------------------------------------------------------------------------
/// Sort the edge segments with respect to their length
///
void SortEdgeSegments(EdgeMap *map){
#if 0
  /// Selection sort: Slow
  for (int i=0; i<map->noSegments-1; i++){
    int max = i;
    for (int j=i+1; j<map->noSegments; j++){
      if (map->segments[j].noPixels > map->segments[max].noPixels) max = j;
    } //end-for

    if (max != i){
      EdgeSegment tmp = map->segments[i];
      map->segments[i] = map->segments[max];
      map->segments[max] = tmp;
    } //end-if
  } //end-for

#else
  /// Counting sort: Fast
  int maxLen = 0;
  for (int i=0; i<map->noSegments; i++){
    if (map->segments[i].noPixels > maxLen) maxLen = map->segments[i].noPixels;
  } //end-for

  maxLen++;
  int *C = new int[maxLen];
  memset(C, 0, sizeof(int)*maxLen);
    
  for (int i=0; i<map->noSegments; i++){
    C[map->segments[i].noPixels]++;
  } //end-for

  for (int i=maxLen-1; i>0; i--){
    C[i-1] += C[i];
  } //end-for

  EdgeSegment *newSegments = new EdgeSegment[map->noSegments*4];

  for (int i=0; i<map->noSegments; i++){
    int index = --C[map->segments[i].noPixels];

    newSegments[index] = map->segments[i];
  } //end-for

  delete map->segments;
  map->segments = newSegments;

  delete C;
#endif
} //end-SortSegments

///-------------------------------------------------------------------------------------------
/// Returns the joint points. 
///
unsigned char *FindJointPoints(EdgeMap *map){
  int width = map->width;
  int height = map->height;

  unsigned char *joints = new unsigned char[width*height];
  memset(joints, 0, width*height);

  short *segments = new short[width*height];
  memset(segments, -1, sizeof(short)*width*height);

  for (int i=0; i<map->noSegments; i++){
    for (int j=0; j<map->segments[i].noPixels; j++){
      int r = map->segments[i].pixels[j].r;
      int c = map->segments[i].pixels[j].c;

      segments[r*width+c] = i;
    } //end-for
  } //end-for

  for (int i=0; i<map->noSegments; i++){
    for (int k=0; k<2; k++){
      int r, c;

      if (k==0){
        r = map->segments[i].pixels[0].r;
        c = map->segments[i].pixels[0].c;

      } else {
        r = map->segments[i].pixels[map->segments[i].noPixels-1].r;
        c = map->segments[i].pixels[map->segments[i].noPixels-1].c;
      } //end-else

      if (r <=0 || r>=height-1 || c<=0 || c>=width-1) continue;

      if      (segments[(r-1)*width+c] >= 0 && segments[(r-1)*width+c] != i) joints[(r-1)*width+c] = 255;  // up
      else if (segments[(r+1)*width+c] >= 0 && segments[(r+1)*width+c] != i) joints[(r+1)*width+c] = 255;  // down
      else if (segments[r*width+c-1] >= 0 && segments[r*width+c-1] != i) joints[r*width+c-1] = 255;  // left
      else if (segments[r*width+c+1] >= 0 && segments[r*width+c+1] != i) joints[r*width+c+1] = 255;  // right
      else if (segments[(r-1)*width+c-1] >= 0 && segments[(r-1)*width+c-1] != i) joints[(r-1)*width+c-1] = 255;  // up-left
      else if (segments[(r-1)*width+c+1] >= 0 && segments[(r-1)*width+c+1] != i) joints[(r-1)*width+c+1] = 255;  // up-right
      else if (segments[(r+1)*width+c+1] >= 0 && segments[(r+1)*width+c+1] != i) joints[(r+1)*width+c+1] = 255;  // down-right
      else if (segments[(r+1)*width+c-1] >= 0 && segments[(r+1)*width+c-1] != i) joints[(r+1)*width+c-1] = 255;  // down-left
    } //end-for
  } //end-for

  delete segments;
  return joints;
} //end-FindJointPoints

///-------------------------------------------------------------------------------------------
/// Extend the edge segments
///
struct JointNode {
  Pixel *pixels;  // Pixels making up this joint point
  int noPixels;   // How many pixels does this joint contain?

  int *segments;  // Segments incident to this joint point
  int noSegments; // # of segments incident to this joint point

  // DFS info
  int color;   // Maintains the current DFS iteration 
  int pred;    // predecessor joint
  int len;     // length so far
  int segment; // Segment we used to reach this joint

  JointNode(){
    noPixels = noSegments = 0;
    color = -1;
  } //end-JointNode

  void addSegment(int s){
    for (int i=0; i<noSegments; i++){
      if (segments[i] == s) return;
    } //end-for

    // Insert in sorted order
    int index = noSegments;
    while (index > 0){
      if (s < segments[index-1]) break;
      segments[index] = segments[index-1];
      index--;
    } //end-while

    segments[index] = s;
    noSegments++;
  } //end-addSegment
};

void DumpJoints(JointNode *joints, int noJoints){
  FILE *fp = fopen("OutputImages/joints.txt", "w");

  for (int i=1; i<noJoints; i++){
    fprintf(fp, "================ JOINT: %d, NoPixels: %d, NoSegments: %d ==============\n", i, joints[i].noPixels, joints[i].noSegments);
    fprintf(fp, "+-----+-----+\n");
    fprintf(fp, "|  C  |  R  |\n");
    fprintf(fp, "+-----+-----+\n");
    for (int j=0; j<joints[i].noPixels; j++){
      fprintf(fp, "| %3d | %3d |\n", joints[i].pixels[j].c, joints[i].pixels[j].r);
    } //end-for
    fprintf(fp, "+-----+-----+\n");

    fprintf(fp, "Incident Segments: |");
    for (int j=0; j<joints[i].noSegments; j++){
      fprintf(fp, " %3d |", joints[i].segments[j]);
    } //end-for 
    fprintf(fp, "\n\n");
  } //end-for

  fclose(fp);
} //end-DumpJoints


struct SegmentInfo {
  bool taken;    // Is this segment taken?
  int len;       // Length of this segment in pixels
  int joints[2]; // joints[0] incident to (sx, sy), joints[1] incident to (ex, ey)

  SegmentInfo(){
    taken = false;
    joints[0] = joints[1] = -1;
  } //end-SegmentInfo

  void addStart(int j){
    joints[0] = j;
  } //end-addStart

  bool addEnd(int j){
    if (joints[0] == j || joints[1] == j) return false;
    joints[1] = j;
    return true;
  } //end-addEnd
};


void DumpSegmentInfo(SegmentInfo *segmentInfo, EdgeMap *map){
  FILE *fp = fopen("OutputImages/SegmentInfo.txt", "w");

  for (int i=0; i<map->noSegments; i++){
    fprintf(fp, "------ SEGMENT: %3d, NoPixels: %3d------\n", i, map->segments[i].noPixels);
    if (segmentInfo[i].joints[0] >= 0) fprintf(fp, "Start: %d\n", segmentInfo[i].joints[0]);
    if (segmentInfo[i].joints[1] >= 0) fprintf(fp, "End  : %d\n", segmentInfo[i].joints[1]);
  } //end-for

  fclose(fp);
} //end-DumpSegmentInfo


struct EdgeMapGraph {
  JointNode *joints;
  int noJoints;

  SegmentInfo *segmentInfo;

  Pixel *jointPixels;
  int   *jointSegments;

  // Temporary buffers used during DFS
  int *segments;
  int *nodes;

  EdgeMapGraph(int noJoints, int noSegments){
    joints = new JointNode[noJoints+4*noSegments];
    segmentInfo = new SegmentInfo[noSegments];

    jointPixels = new Pixel[noJoints];
    jointSegments = new int[noSegments*8];

    segments = new int[noSegments];
    nodes = new int[noJoints+1];
  } // end-EdgeMapGraph

  ~EdgeMapGraph(){
    delete joints;
    delete segmentInfo;
  
    delete jointPixels;
    delete jointSegments;
  
    delete segments;
    delete nodes;
  } //end-~EdgeMapGraph
};


/// Returns the neighbor through which we obtain the longest path
void DFSVisit(int root, EdgeMapGraph *graph, int &maxJoint, int iterationNumber){
  JointNode *joints = graph->joints;
  SegmentInfo *segmentInfo = graph->segmentInfo;

  joints[root].color = iterationNumber;
  
  // Did we hit a leaf node? If yes, simply return
  if (joints[root].noSegments == 1){
    if (joints[root].len > joints[maxJoint].len) maxJoint = root;
    return;
  } //end-if

  // Try alternative routes
  for (int i=joints[root].noSegments-1; i>=0; i--){
    int segment = joints[root].segments[i];
    if (segmentInfo[segment].taken) continue;    // Skip already taken segments

    int nextJoint;
    if (segmentInfo[segment].joints[0] == root) nextJoint = segmentInfo[segment].joints[1];
    else                                        nextJoint = segmentInfo[segment].joints[0];

    if (joints[nextJoint].color != iterationNumber){
      // Start a new DFSVisit at the next joint
      joints[nextJoint].pred = root;
      joints[nextJoint].len = joints[root].len + segmentInfo[segment].len;
      joints[nextJoint].segment = segment;

      DFSVisit(nextJoint, graph, maxJoint, iterationNumber);
    } //end-if
  } //end-for

  if (joints[root].len > joints[maxJoint].len) maxJoint = root;
} //end-DFSVisit


#define BOTTOM_UP  0
#define TOP_DOWN   1
void DFS(int root, EdgeMapGraph *graph, int iterationNumber, EdgeMap *map1, EdgeMap *map2, int dir = 0){
  JointNode *joints = graph->joints;
  SegmentInfo *segmentInfo = graph->segmentInfo;

  joints[root].color = iterationNumber;

  int maxJoint = root;
  DFSVisit(root, graph, maxJoint, iterationNumber);

  // Copy the pixels to the new edge map
  int segmentNo = map2->noSegments;
  int len = map2->segments[segmentNo].noPixels;

  int *nodes = graph->nodes;
  int *segments = graph->segments;
  int count = 0;
  while (maxJoint != root){
    segments[count] = joints[maxJoint].segment;
    nodes[count] = maxJoint;
    count++;

    maxJoint = joints[maxJoint].pred;
    nodes[count] = maxJoint;
  } //end-while
  
  if (dir == BOTTOM_UP){
    for (int i=0; i<count; i++){
      int maxJoint = nodes[i];
      int segment = joints[maxJoint].segment;
      segmentInfo[segment].taken = true;
      
      if (segmentInfo[segment].joints[0] == maxJoint){
        // Copy in the forward direction
        for (int k=0; k<map1->segments[segment].noPixels; k++){
          map2->segments[segmentNo].pixels[len++] = map1->segments[segment].pixels[k];
        } //end-for

      } else {
        // Copy in the reverse direction
        for (int k=map1->segments[segment].noPixels-1; k>=0; k--){
          map2->segments[segmentNo].pixels[len++] = map1->segments[segment].pixels[k];
        } //end-for
      } //end-else

#if 1
      maxJoint = nodes[i+1];
      if (joints[maxJoint].noPixels == 1){
        map2->segments[segmentNo].pixels[len].r = joints[maxJoint].pixels[0].r;
        map2->segments[segmentNo].pixels[len].c = joints[maxJoint].pixels[0].c;
        len++;
      } //end-if
#endif
    } //end-while

  } else {
    int maxJoint = root;
    for (int i=count-1; i>=0; i--){
#if 1
      if (joints[maxJoint].noPixels == 1){
        map2->segments[segmentNo].pixels[len].r = joints[maxJoint].pixels[0].r;
        map2->segments[segmentNo].pixels[len].c = joints[maxJoint].pixels[0].c;
        len++;
      } //end-for
#endif

      maxJoint = nodes[i];
      int segment = joints[maxJoint].segment;
      segmentInfo[segment].taken = true;
      
      if (segmentInfo[segment].joints[0] == maxJoint){
        // Copy in the reverse direction
        for (int k=map1->segments[segment].noPixels-1; k>=0; k--){
          map2->segments[segmentNo].pixels[len++] = map1->segments[segment].pixels[k];
        } //end-for

      } else {
        // Copy in the forward direction
        for (int k=0; k<map1->segments[segment].noPixels; k++){
          map2->segments[segmentNo].pixels[len++] = map1->segments[segment].pixels[k];
        } //end-for
      } //end-else
    } //end-while
  } //end-else
  
  map2->segments[segmentNo].noPixels = len;
} //end-DFSLeft


void ExtendSegment(int segment, EdgeMapGraph *graph, int noJoints, EdgeMap *map1, EdgeMap *map2){
  JointNode *joints = graph->joints;
  SegmentInfo *segmentInfo = graph->segmentInfo;

  segmentInfo[segment].taken = true;
  map2->segments[map2->noSegments].noPixels = 0;

  // Can we extend this segment to the left?
  if (segmentInfo[segment].joints[0] >= 0){
    DFS(segmentInfo[segment].joints[0], graph, 2*segment, map1, map2, BOTTOM_UP);
  } //end-else

  int len = map2->segments[map2->noSegments].noPixels;

  // Copy the segment pixels from the old segment
  for (int i=0; i<map1->segments[segment].noPixels; i++){
    map2->segments[map2->noSegments].pixels[len] = map1->segments[segment].pixels[i];
    len++;
  } //end-for

  map2->segments[map2->noSegments].noPixels = len;

  // Now extend to the right
  if (segmentInfo[segment].joints[1] >= 0){
    DFS(segmentInfo[segment].joints[1], graph, 2*segment+1, map1, map2, TOP_DOWN);
  } //end-if

#if 0
  // Check:
  int pr = map2->segments[map2->noSegments].pixels[0].r;
  int pc = map2->segments[map2->noSegments].pixels[0].c;
  
  for (int i=1; i<map2->segments[map2->noSegments].noPixels; i++){
    int r = map2->segments[map2->noSegments].pixels[i].r;
    int c = map2->segments[map2->noSegments].pixels[i].c;

    if (abs(r-pr)>=2 || abs(c-pc)>=2){
      fprintf(stderr, "ERROR: Segments: %d at index: %d, (c: %d, r: %d)\n", map2->noSegments, i, c, r);
      break;
    } //end-if

    pr = r;
    pc = c;
  } //end-for
#endif
} //end-ExtendSegment


EdgeMap *ExtendEdgeSegments(EdgeMap *map){
//  DumpEdgeSegments2File(map, "OutputImages/EdgeSegments1.txt");

  int width = map->width;
  int height = map->height;

#define BLANK    0
#define JOINT   -1
#define SEGMENT_OFFSET -2

  int *status = new int[width*height];
  memset(status, 0, sizeof(int)*width*height);

  for (int i=0; i<map->noSegments; i++){
    for (int j=0; j<map->segments[i].noPixels; j++){
      int r = map->segments[i].pixels[j].r;
      int c = map->segments[i].pixels[j].c;

      status[r*width+c] = SEGMENT_OFFSET-i;
    } //end-for
  } //end-for

  int noJoints = 0;
  for (int i=0; i<map->noSegments; i++){
    int segmentNo = SEGMENT_OFFSET-i;

    for (int k=0; k<2; k++){
      int r, c;

      if (k==0){
        r = map->segments[i].pixels[0].r;
        c = map->segments[i].pixels[0].c;

      } else {
        r = map->segments[i].pixels[map->segments[i].noPixels-1].r;
        c = map->segments[i].pixels[map->segments[i].noPixels-1].c;
      } //end-else

      if      (status[(r-1)*width+c] <= SEGMENT_OFFSET && status[(r-1)*width+c] != segmentNo){status[(r-1)*width+c] = JOINT;  noJoints++;} // up
      else if (status[(r+1)*width+c] <= SEGMENT_OFFSET && status[(r+1)*width+c] != segmentNo){status[(r+1)*width+c] = JOINT; noJoints++;} // down
      else if (status[r*width+c-1] <= SEGMENT_OFFSET && status[r*width+c-1] != segmentNo){status[r*width+c-1] = JOINT; noJoints++;}  // left
      else if (status[r*width+c+1] <= SEGMENT_OFFSET && status[r*width+c+1] != segmentNo){status[r*width+c+1] = JOINT; noJoints++;} // right

      else if (status[(r-1)*width+c-1] <= SEGMENT_OFFSET && status[(r-1)*width+c-1] != segmentNo){status[(r-1)*width+c-1] = JOINT; noJoints++;} // up-left
      else if (status[(r-1)*width+c+1] <= SEGMENT_OFFSET && status[(r-1)*width+c+1] != segmentNo){status[(r-1)*width+c+1] = JOINT; noJoints++;} // up-right
      else if (status[(r+1)*width+c+1] <= SEGMENT_OFFSET && status[(r+1)*width+c+1] != segmentNo){status[(r+1)*width+c+1] = JOINT; noJoints++;} // down-right
      else if (status[(r+1)*width+c-1] <= SEGMENT_OFFSET && status[(r+1)*width+c-1] != segmentNo){status[(r+1)*width+c-1] = JOINT; noJoints++;} // down-left
    } //end-for
  } //end-for
  
  // Very simple idea: Simply walk over each segment & see if it intersects with another segment
  EdgeSegment *newSegments = new EdgeSegment[map->noSegments*8];
  int noNewSegments = 0;

  for (int i=0; i<map->noSegments; i++){   
    int lastIndex = 0;

    for (int j=0; j<map->segments[i].noPixels; j++){
      int r = map->segments[i].pixels[j].r;
      int c = map->segments[i].pixels[j].c;

      if (status[r*width+c] != JOINT) continue;

      newSegments[noNewSegments].noPixels = j-lastIndex;
      newSegments[noNewSegments].pixels = &map->segments[i].pixels[lastIndex];
      lastIndex = j+1;

      // Can't have segments of 0 length
      if (newSegments[noNewSegments].noPixels >= 1) noNewSegments++;
    } //end-for

    if (map->segments[i].noPixels - lastIndex > 0){
      newSegments[noNewSegments].noPixels = map->segments[i].noPixels - lastIndex;
      newSegments[noNewSegments].pixels = &map->segments[i].pixels[lastIndex];
      noNewSegments++;
    } //end-if
  } //end-for

  // Copy the chains back to map
  delete map->segments;
  map->segments = newSegments;
  map->noSegments = noNewSegments;

  // Sort the segments with respect to their length, with the longest at the beginning
  SortEdgeSegments(map);

  // Combine the joint points into super nodes
  EdgeMapGraph *graph = new EdgeMapGraph(noJoints, map->noSegments);
  JointNode *joints = graph->joints;
  int jointNo = 0;  // current joint number    

  Pixel *jointPixels = graph->jointPixels;
  int totalNoPixels = 0;

  Pixel Stack[64];
  for (int i=1; i<height-1; i++){
    for (int j=1; j<width-1; j++){
      if (status[i*width+j] != JOINT) continue;

      status[i*width+j] = ++jointNo;
      joints[jointNo].pixels = jointPixels + totalNoPixels;
      int noPixels = 0;

      Stack[0].r = i;
      Stack[0].c = j;
      int top = 0;

      while (top >= 0){
        int r=Stack[top].r;
        int c=Stack[top].c;
        top--;

        joints[jointNo].pixels[noPixels].r = r;
        joints[jointNo].pixels[noPixels].c = c;
        noPixels++;

        if (status[(r-1)*width+c-1] == JOINT){Stack[++top].r=r-1; Stack[top].c=c-1; status[(r-1)*width+c-1]=jointNo;}
        if (status[(r-1)*width+c] == JOINT){Stack[++top].r=r-1; Stack[top].c=c; status[(r-1)*width+c]=jointNo;}
        if (status[(r-1)*width+c+1] == JOINT){Stack[++top].r=r-1; Stack[top].c=c+1; status[(r-1)*width+c+1]=jointNo;}

        if (status[r*width+c-1] == JOINT){Stack[++top].r=r; Stack[top].c=c-1; status[r*width+c-1]=jointNo;}
        if (status[r*width+c+1] == JOINT){Stack[++top].r=r; Stack[top].c=c+1; status[r*width+c+1]=jointNo;}

        if (status[(r+1)*width+c-1] == JOINT){Stack[++top].r=r+1; Stack[top].c=c-1; status[(r+1)*width+c-1]=jointNo;}
        if (status[(r+1)*width+c] == JOINT){Stack[++top].r=r+1; Stack[top].c=c; status[(r+1)*width+c]=jointNo;}
        if (status[(r+1)*width+c+1] == JOINT){Stack[++top].r=r+1; Stack[top].c=c+1; status[(r+1)*width+c+1]=jointNo;}
      } //end-while

      joints[jointNo].noPixels = noPixels;
      totalNoPixels += noPixels;
    } //end-for1
  } //end-for

  noJoints = jointNo+1;

  // Adjust joint segment pointers
  int *jointSegments = graph->jointSegments;
  int totalNoSegments = 0;
  for (int i=1; i<noJoints; i++){
    joints[i].segments = jointSegments + totalNoSegments;
    joints[i].noSegments = 0;

    totalNoSegments += joints[i].noPixels*4;
  } //end-for

  // Create the graph
  int noSegments = map->noSegments;
  SegmentInfo *segmentInfo = graph->segmentInfo;

  for (int i=0; i<map->noSegments; i++){
    // Update the length
    segmentInfo[i].len = map->segments[i].noPixels;

    // Compute the joints incident to this edge segment at (sx, sy)
    int r = map->segments[i].pixels[0].r;
    int c = map->segments[i].pixels[0].c;

    int j;
    j = status[(r-1)*width+c-1]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}
    j = status[(r-1)*width+c]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}
    j = status[(r-1)*width+c+1]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}

    j = status[r*width+c-1]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}
    j = status[r*width+c+1]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}

    j = status[(r+1)*width+c-1]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}
    j = status[(r+1)*width+c]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}
    j = status[(r+1)*width+c+1]; if (j>0){segmentInfo[i].addStart(j); joints[j].addSegment(i); goto EndPoint;}

EndPoint:
    // Compute the joints incident to this edge segment at (ex, ey)
    r = map->segments[i].pixels[map->segments[i].noPixels-1].r;
    c = map->segments[i].pixels[map->segments[i].noPixels-1].c;

    j = status[(r-1)*width+c-1]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}
    j = status[(r-1)*width+c]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}
    j = status[(r-1)*width+c+1]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}

    j = status[r*width+c-1]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}
    j = status[r*width+c+1]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}

    j = status[(r+1)*width+c-1]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}
    j = status[(r+1)*width+c]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}
    j = status[(r+1)*width+c+1]; if (j>0){if (segmentInfo[i].addEnd(j)){joints[j].addSegment(i); continue;}}
  } //end-for

  // Create the new edgemap
  EdgeMap *map2 = new EdgeMap(width, height);

  // First, extract all edge segments which are not connected to any joint points
  noNewSegments = 0;
  int totalLen = 0;

  for (int i=0; i<noSegments; i++){
    if (segmentInfo[i].taken) continue;

    if (segmentInfo[i].joints[0] < 0 && segmentInfo[i].joints[1] < 0){
      segmentInfo[i].taken = true;
      map2->segments[noNewSegments].pixels = map2->pixels + totalLen;

      for (int j=0; j<map->segments[i].noPixels; j++){
        map2->segments[noNewSegments].pixels[j] = map->segments[i].pixels[j];
      } //end-for

      map2->segments[noNewSegments].noPixels = map->segments[i].noPixels;
      if (map2->segments[noNewSegments].noPixels < 10) continue;

      totalLen += map2->segments[noNewSegments].noPixels;
      noNewSegments++;

    } else if (segmentInfo[i].joints[0] < 0){
      // Add dummy joint nodes that sit on top of the non-connected end-point of an edge segment. This helps us in DFS.
      segmentInfo[i].addStart(noJoints);

      joints[noJoints].segments = jointSegments + totalNoSegments;
      totalNoSegments++;
      joints[noJoints].addSegment(i);
      noJoints++;

    } else if (segmentInfo[i].joints[1] < 0){
      segmentInfo[i].addEnd(noJoints);

      joints[noJoints].segments = jointSegments + totalNoSegments;
      totalNoSegments++;
      joints[noJoints].addSegment(i);
      noJoints++;
    } //end-else
  } //end-for

//  DumpJoints(joints, noJoints);
//  DumpEdgeSegments2File(map, "OutputImages/EdgeSegments2.txt");
//  DumpSegmentInfo(segmentInfo, map);

  // Extend the rest of the segments
  map2->noSegments = noNewSegments;

//  map2->noSegments = 0;
  int count = 0;

  for (int i=0; i<map->noSegments; i++){
    if (segmentInfo[i].taken) continue;
    if (segmentInfo[i].len < 5) continue;

    map2->segments[map2->noSegments].pixels = map2->pixels + totalLen;

    ExtendSegment(i, graph, noJoints, map, map2);

    if (map2->segments[map2->noSegments].noPixels < 10) continue;

    totalLen += map2->segments[map2->noSegments].noPixels;
    map2->noSegments++;
  } //end-for

  delete status;
  delete graph;

  return map2;
} //end-ExtendEdgeSegments

///================ DETERMINE EDGE SEGMENT END-POINTS ========================================
///-------------------------------------------------------------
/// Counts the neighbors of an edge pixel
///
int CountNeighbors(unsigned char *edgeImg, int width, int i, int j){
  int N[9];

  memset(N, 0, sizeof(int)*9);
  if (edgeImg[(i-1)*width+j]){N[0] = -1; N[1]=1; N[2]=-1;}   // UP
  if (edgeImg[(i+1)*width+j]){N[6] = -1; N[7]=1; N[8]=-1;}   // DOWN
  if (edgeImg[i*width+j-1]){N[0] = -1; N[3]=1; N[6]=-1;}     // LEFT
  if (edgeImg[i*width+j+1]){N[2] = -1; N[5]=1; N[8]=-1;}     // RIGHT

  if (edgeImg[(i-1)*width+j-1]) N[0]++;   // NW
  if (edgeImg[(i-1)*width+j+1]) N[2]++;   // NE
  if (edgeImg[(i+1)*width+j-1]) N[6]++;   // SW
  if (edgeImg[(i+1)*width+j+1]) N[8]++;   // SE

  int count = 0;
  for (int k=0; k<9; k++){if(N[k]>0) count += N[k];}

  return count;
} //end-CountNeighbors

///-------------------------------------------------------------------------------------------
/// Given a binary edgemap, determines the end-points of the edge segments
/// We will connect these end-points to link disconneced edge segments (edge linking)
///
unsigned char *FindEdgeSegmentEndPoints(unsigned char *edgeImg, int width, int height){
  unsigned char *endPoints = new unsigned char[width*height]; 
  memset(endPoints, 0, width*height);

  for (int i=1; i<height-1; i++){
    for (int j=1; j<width-1; j++){
      if (edgeImg[i*width+j] == 0) continue;

      if (CountNeighbors(edgeImg, width, i, j) == 1){
        endPoints[i*width+j] = 254;

      } else {
//        endPoints[i*width+j] = 255;
      } //end-else
    } //end-for
  } //end-for

  return endPoints;
} //end-FindEdgeSegmentEndPoints

// Walk directions
#define UP_LEFT    1   // diagonal
#define UP         2   
#define UP_RIGHT   3
#define RIGHT      4
#define DOWN_RIGHT 5
#define DOWN       6
#define DOWN_LEFT  7
#define LEFT       8

int ComputeGradient(unsigned char *smoothImg, int width, int height, int r, int c){
  int com1 = smoothImg[(r+1)*width+c+1] - smoothImg[(r-1)*width+c-1];
  int com2 = smoothImg[(r-1)*width+c+1] - smoothImg[(r+1)*width+c-1];

  int gx = abs(com1 + com2 + (smoothImg[r*width+c+1] - smoothImg[r*width+c-1]));
  int gy = abs(com1 - com2 + (smoothImg[(r+1)*width+c] - smoothImg[(r-1)*width+c]));

  return gx+gy;
} //end-ComputeGradient

void Walk(unsigned char *smoothImg, unsigned char *edgeImg, int width, int height, int r, int c, int dir, int maxWalkDistance){
  while (maxWalkDistance > 0){
    if (r<=0 || r>=height-1) return;
    if (c<=0 || c>=width-1) return;

    edgeImg[r*width+c] = 255;

    int maxGrad = 15;
    int nextDir = -1;
    int grad;
    int nr=r, nc=c;

    if        (dir == UP_LEFT){
      // Left
      if (edgeImg[r*width+c-1]) return;  
      grad = ComputeGradient(smoothImg, width, height, r, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r; nc = c-1; nextDir = LEFT;}

      // Up
      if (edgeImg[(r-1)*width+c]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c; nextDir = UP;}

      // Up-Left
      if (edgeImg[(r-1)*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c-1; nextDir = UP_LEFT;}

    } else if (dir == UP){
      // Up
      if (edgeImg[(r-1)*width+c]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c; nextDir = UP;}

      // Up-Left
      if (edgeImg[(r-1)*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c-1; nextDir = UP_LEFT;}

      // Up-Right
      if (edgeImg[(r-1)*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c+1; nextDir = UP_RIGHT;}

    } else if (dir == UP_RIGHT){
      // Up
      if (edgeImg[(r-1)*width+c]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c; nextDir = UP;}

      // Right
      if (edgeImg[r*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r; nc = c+1; nextDir = RIGHT;}

      // Up-Right
      if (edgeImg[(r-1)*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c+1; nextDir = UP_RIGHT;}

    } else if (dir == RIGHT){
      // Right
      if (edgeImg[r*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r; nc = c+1; nextDir = RIGHT;}

      // Up-Right
      if (edgeImg[(r-1)*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c+1; nextDir = UP_RIGHT;}

      // Down-Right
      if (edgeImg[(r+1)*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c+1; nextDir = DOWN_RIGHT;}

    } else if (dir == DOWN_RIGHT){
      // Right
      if (edgeImg[r*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r; nc = c+1; nextDir = RIGHT;}

      // Down
      if (edgeImg[(r+1)*width+c]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c; nextDir = DOWN;}

      // Down-Right
      if (edgeImg[(r+1)*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c+1; nextDir = DOWN_RIGHT;}

    } else if (dir == DOWN){
      // Down
      if (edgeImg[(r+1)*width+c]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c; nextDir = DOWN;}

      // Down-Right
      if (edgeImg[(r+1)*width+c+1]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c+1);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c+1; nextDir = DOWN_RIGHT;}

      // Down-Left
      if (edgeImg[(r+1)*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c-1; nextDir = DOWN_LEFT;}

    } else if (dir == DOWN_LEFT){
      // Down
      if (edgeImg[(r+1)*width+c]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c; nextDir = DOWN;}

      // Left
      if (edgeImg[r*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r; nc = c-1; nextDir = LEFT;}

      // Down-Left
      if (edgeImg[(r+1)*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c-1; nextDir = DOWN_LEFT;}

    } else { // (dir == LEFT){
      // Left
      if (edgeImg[r*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r; nc = c-1; nextDir = LEFT;}

      // Down-Left
      if (edgeImg[(r+1)*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r+1, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r+1; nc = c-1; nextDir = DOWN_LEFT;}

      // Up-Left
      if (edgeImg[(r-1)*width+c-1]) return;
      grad = ComputeGradient(smoothImg, width, height, r-1, c-1);
      if (grad > maxGrad){maxGrad = grad; nr = r-1; nc = c-1; nextDir = UP_LEFT;}
    } // end-else 

    if (nextDir < 0) break;
    r = nr;
    c = nc;
    dir = nextDir;

    maxWalkDistance--;
  } //end-while
} //end-Walk

///------------------------------------------------------------------
/// Given a binary edge map, fill the gaps between edge end-points by using templates
///
void FillEdgeGaps(unsigned char *smoothImg, unsigned char *edgeImg, int width, int height){
  unsigned char *out = new unsigned char[width*height];
  memset(out, 0, width*height);

  for (int i=1; i<height; i++){
    for (int j=1; j<width; j++){
      if (edgeImg[i*width+j] == 0) continue;
      if (out[i*width+j]) continue;

      Walk(smoothImg, out, width, height, i, j, RIGHT, 8*(width+height));
    } //end-for
  } // end-for

  memcpy(edgeImg, out, width*height);
  return;

  for (int i=1; i<height-1; i++){
    for (int j=1; j<width-1; j++){
      if (edgeImg[i*width+j] == 0) continue;
      if (CountNeighbors(edgeImg, width, i, j) != 1) continue;
      
      // (i, j) is an edge endpoint. 
      // Try to close the gap from (i, j) to its neighbors by using teplates
      // Find the current direction, and start a Walk from here
      int dir;
      if      (edgeImg[(i-1)*width+j-1]) dir = DOWN_RIGHT;
      else if (edgeImg[(i-1)*width+j+1]) dir = DOWN_LEFT;
      else if (edgeImg[(i+1)*width+j+1]) dir = UP_LEFT;
      else if (edgeImg[(i+1)*width+j-1]) dir = UP_RIGHT;
      else if (edgeImg[(i-1)*width+j])   dir = DOWN;
      else if (edgeImg[i*width+j+1])     dir = LEFT;
      else if (edgeImg[(i+1)*width+j])   dir = UP;
      else /* (edgeImg[i*width+j-1]) */  dir = RIGHT;

      Walk(smoothImg, edgeImg, width, height, i, j, dir, 8);
    } //end-for
  } //end-for
} // end-FillEdgeGaps

///---------------------------------------------------------
/// Fix edge segments having one or two pixel fluctuations
/// An example one pixel problem getting fixed:
///  x
/// x x --> xxx
///
/// An example two pixel problem getting fixed:
///  xx
/// x  x --> xxxx
///
void FixEdgeSegments(EdgeMap *map, int noPixels){
  /// First fix one pixel problems: There are four cases
  for (int i=0; i<map->noSegments; i++){
    int cp = map->segments[i].noPixels-2;  // Current pixel index
    int n2 = 0;  // next next pixel index

    while (n2 < map->segments[i].noPixels){
      int n1 = cp+1; // next pixel

      cp = cp % map->segments[i].noPixels; // Roll back to the beginning
      n1 = n1 % map->segments[i].noPixels; // Roll back to the beginning

      int r = map->segments[i].pixels[cp].r;
      int c = map->segments[i].pixels[cp].c;

      int r1 = map->segments[i].pixels[n1].r;
      int c1 = map->segments[i].pixels[n1].c;

      int r2 = map->segments[i].pixels[n2].r;
      int c2 = map->segments[i].pixels[n2].c;

      // 4 cases to fix
      if (r2 == r-2 && c2 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
        } //end-if

        cp = n2;
        n2 += 2;

      } else if (r2 == r+2 && c2 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
        } //end-if

        cp = n2;
        n2 += 2;

      } else if (r2 == r && c2 == c-2){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
        } //end-if

        cp = n2;
        n2 += 2;

      } else if (r2 == r && c2 == c+2){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
        } //end-if

        cp = n2;
        n2 += 2;

      } else {
        cp++;
        n2++;
      } //end-else
    } //end-while
  } // end-for

  if (noPixels <= 1) return;

  // Now fix two pixel problems: There are 4 cases
  for (int i=0; i<map->noSegments; i++){
    int cp = map->segments[i].noPixels-3;  // Current pixel index
    int n3 = 0;                            // next next next pixel index

    while (n3 < map->segments[i].noPixels){
      int n1 = cp+1;  // next pixel
      int n2 = cp+2; // next next pixel

      cp = cp % map->segments[i].noPixels; // Roll back to the beginning
      n1 = n1 % map->segments[i].noPixels; // Roll back to the beginning
      n2 = n2 % map->segments[i].noPixels; // Roll back to the beginning

      int r = map->segments[i].pixels[cp].r;
      int c = map->segments[i].pixels[cp].c;

      int r1 = map->segments[i].pixels[n1].r;
      int c1 = map->segments[i].pixels[n1].c;

      int r2 = map->segments[i].pixels[n2].r;
      int c2 = map->segments[i].pixels[n2].c;

      int r3 = map->segments[i].pixels[n3].r;
      int c3 = map->segments[i].pixels[n3].c;

      // 4 cases to fix
      if (r3 == r-3 && c3 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
          map->segments[i].pixels[n2].c = c;
        } //end-if

        cp = n3;
        n3 += 3;

      } else if (r3 == r+3 && c3 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
          map->segments[i].pixels[n2].c = c;
        } //end-if

        cp = n3;
        n3 += 3;

      } else if (r3 == r && c3 == c-3){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
          map->segments[i].pixels[n2].r = r;
        } //end-if

        cp = n3;
        n3 += 3;

      } else if (r3 == r && c3 == c+3){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
          map->segments[i].pixels[n2].r = r;
        } //end-if

        cp = n3;
        n3 += 3;

      } else {
        cp++;
        n3++;
      } //end-else
    } //end-while
  } // end-for

  if (noPixels <= 2) return;

  // How about fixing three pixel problems? Don't think this is a good idea, but let's try! There are 4 cases
  for (int i=0; i<map->noSegments; i++){
    int cp = map->segments[i].noPixels-4;  // Current pixel index
    int n4 = 0;                            // next next next next pixel index

    while (n4 < map->segments[i].noPixels){
      int n1 = cp+1; // next pixel
      int n2 = cp+2; // next next pixel
      int n3 = cp+3; // next next next pixel

      cp = cp % map->segments[i].noPixels; // Roll back to the beginning
      n1 = n1 % map->segments[i].noPixels; // Roll back to the beginning
      n2 = n2 % map->segments[i].noPixels; // Roll back to the beginning
      n3 = n3 % map->segments[i].noPixels; // Roll back to the beginning

      int r = map->segments[i].pixels[cp].r;
      int c = map->segments[i].pixels[cp].c;

      int r1 = map->segments[i].pixels[n1].r;
      int c1 = map->segments[i].pixels[n1].c;

      int r2 = map->segments[i].pixels[n2].r;
      int c2 = map->segments[i].pixels[n2].c;

      int r3 = map->segments[i].pixels[n3].r;
      int c3 = map->segments[i].pixels[n3].c;

      int r4 = map->segments[i].pixels[n4].r;
      int c4 = map->segments[i].pixels[n4].c;

      // 4 cases to fix
      if (r4 == r-4 && c4 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
          map->segments[i].pixels[n2].c = c;
          map->segments[i].pixels[n3].c = c;
        } //end-if

        cp = n4;
        n4 += 4;

      } else if (r4 == r+4 && c4 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
          map->segments[i].pixels[n2].c = c;
          map->segments[i].pixels[n3].c = c;
        } //end-if

        cp = n4;
        n4 += 4;

      } else if (r4 == r && c4 == c-4){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
          map->segments[i].pixels[n2].r = r;
          map->segments[i].pixels[n3].r = r;
        } //end-if

        cp = n4;
        n4 += 4;

      } else if (r4 == r && c4 == c+4){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
          map->segments[i].pixels[n2].r = r;
          map->segments[i].pixels[n3].r = r;
        } //end-if

        cp = n4;
        n4 += 4;

      } else {
        cp++;
        n4++;
      } //end-else
    } //end-while
  } // end-for

  if (noPixels <= 3) return;

  // Now fix 4 pixel problems: This is definitely a bad idea, but let's try! There are 4 cases
  for (int i=0; i<map->noSegments; i++){
    int cp = map->segments[i].noPixels-5;  // Current pixel index
    int n5 = 0;                            // next next next next pixel index

    while (n5 < map->segments[i].noPixels){
      int n1 = cp+1; // next pixel
      int n2 = cp+2; // next next pixel
      int n3 = cp+3; // next next next pixel
      int n4 = cp+4; // next next next next pixel

      cp = cp % map->segments[i].noPixels; // Roll back to the beginning
      n1 = n1 % map->segments[i].noPixels; // Roll back to the beginning
      n2 = n2 % map->segments[i].noPixels; // Roll back to the beginning
      n3 = n3 % map->segments[i].noPixels; // Roll back to the beginning
      n4 = n4 % map->segments[i].noPixels; // Roll back to the beginning

      int r = map->segments[i].pixels[cp].r;
      int c = map->segments[i].pixels[cp].c;

      int r1 = map->segments[i].pixels[n1].r;
      int c1 = map->segments[i].pixels[n1].c;

      int r2 = map->segments[i].pixels[n2].r;
      int c2 = map->segments[i].pixels[n2].c;

      int r3 = map->segments[i].pixels[n3].r;
      int c3 = map->segments[i].pixels[n3].c;

      int r4 = map->segments[i].pixels[n4].r;
      int c4 = map->segments[i].pixels[n4].c;

      int r5 = map->segments[i].pixels[n5].r;
      int c5 = map->segments[i].pixels[n5].c;

      // 4 cases to fix
      if (r5 == r-5 && c5 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
          map->segments[i].pixels[n2].c = c;
          map->segments[i].pixels[n3].c = c;
          map->segments[i].pixels[n4].c = c;
        } //end-if

        cp = n5;
        n5 += 5;

      } else if (r5 == r+5 && c5 == c){
        if (c1 != c){
          map->segments[i].pixels[n1].c = c;
          map->segments[i].pixels[n2].c = c;
          map->segments[i].pixels[n3].c = c;
          map->segments[i].pixels[n4].c = c;
        } //end-if

        cp = n5;
        n5 += 5;

      } else if (r4 == r && c4 == c-4){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
          map->segments[i].pixels[n2].r = r;
          map->segments[i].pixels[n3].r = r;
          map->segments[i].pixels[n4].r = r;
        } //end-if

        cp = n5;
        n5 += 5;

      } else if (r5 == r && c5 == c+5){
        if (r1 != r){
          map->segments[i].pixels[n1].r = r;
          map->segments[i].pixels[n2].r = r;
          map->segments[i].pixels[n3].r = r;
          map->segments[i].pixels[n4].r = r;
        } //end-if

        cp = n5;
        n5 += 5;

      } else {
        cp++;
        n5++;
      } //end-else
    } //end-while
  } // end-for
} //end-FixEdgeMap

///------------------------------------------------------------------------
/// Close gaps of 1 pixel wide between the end points of an edge map
///
void FillGaps1(unsigned char *edgeImg, int width, int height){
  for (int i=1; i<height-1; i++){
    for (int j=1; j<width-1; j++){
      if (edgeImg[i*width+j] == 0) continue;

      int count = 0;
      if (edgeImg[(i-1)*width+j]) count++;
      if (edgeImg[(i+1)*width+j]) count++;
      if (edgeImg[i*width+j-1]) count++;
      if (edgeImg[i*width+j+1]) count++;

      if (edgeImg[(i-1)*width+j-1] && edgeImg[(i-1)*width+j] == 0 && edgeImg[i*width+j-1] == 0) count++;
      if (edgeImg[(i-1)*width+j+1] && edgeImg[(i-1)*width+j] == 0 && edgeImg[i*width+j+1] == 0) count++;
      if (edgeImg[(i+1)*width+j+1] && edgeImg[(i+1)*width+j] == 0 && edgeImg[i*width+j+1] == 0) count++;
      if (edgeImg[(i+1)*width+j-1] && edgeImg[(i+1)*width+j] == 0 && edgeImg[i*width+j-1] == 0) count++;

      if (count <= 1) edgeImg[i*width+j] = 128;           // Tip of an edge group pixel
    } //end-for
  } //end-for

  // Now use the endpoint information to join endpoints that are one pixel apart from each other
  for (int i=1; i<height-1; i++){
    for (int j=1; j<width-1; j++){
      if (edgeImg[i*width+j] != 128) continue;

      // Search for endpoints that are one pixel apart and connect them
      int r, c;

      r = i-1; c = j+2;
      if (r >= 0 && c < width && edgeImg[r*width+c] == 128){
        int index = (i-1)*width+j+1;

        edgeImg[index] = 255;
      } //end-if

      r = i; c = j+2;
      if (c < width && edgeImg[r*width+c] == 128){
        int index = i*width+j+1;

        edgeImg[index] = 255;
      } //end-if

      r = i+1; c = j+2;
      if (r <height && c < width && edgeImg[r*width+c] == 128){
        int index = (i+1)*width+j+1;

        edgeImg[index] = 255;
      } //end-if

#if 0
      // Right Diagonal
      r = i+2; c = j+2;
      if (r <height && c < width && edgeImg[r*width+c] == 128){
        int index = (i+1)*width+j+1;

        edgeImg[index] = 255;
      } //end-if
#endif

      r = i+2; c = j+1;
      if (r <height && c < width && edgeImg[r*width+c] == 128){
        int index = (i+1)*width+j+1;

        edgeImg[index] = 255;
      } //end-if

      r = i+2; c = j;
      if (r <height && edgeImg[r*width+c] == 128){
        int index = (i+1)*width+j;

        edgeImg[index] = 255;
      } //end-if

      r = i+2; c = j-1;
      if (c >= 0 && r <height && edgeImg[r*width+c] == 128){
        int index = (i+1)*width+j-1;

        edgeImg[index] = 255;
      } //end-if

#if 0
      // Left Diagonal
      r = i+2; c = j-2;
      if (c >= 0 && r <height && edgeImg[r*width+c] == 128){
        int index = (i+1)*width+j-1;

        edgeImg[index] = 255;
      } //end-if
#endif

      r = i+1; c = j-2;
      if (c >= 0 && r <height && edgeImg[r*width+c] == 128){
        int index = (i+1)*width+j-1;

        edgeImg[index] = 255;
      } //end-if

      edgeImg[i*width+j] = 255;
    } //end-for
  } //end-for
} //end-FillGaps1

///---------------------------------------------------------------------------------
/// Close gaps of 1 pixel wide: This joins the tip of an edge group to ANY neighbouring edgel
///
void FillGaps2(unsigned char *edgeImg, int width, int height){
  for (int i=2; i<height-2; i++){
    for (int j=2; j<width-2; j++){
      if (edgeImg[i*width+j] != 255) continue;

      int count = 0;
      int loc = 1;
      if (edgeImg[(i-1)*width+j] == 255) count++;
      if (edgeImg[(i+1)*width+j] == 255){count++; loc = 2;}
      if (edgeImg[i*width+j-1] == 255){count++; loc = 3;}
      if (edgeImg[i*width+j+1] == 255){count++; loc = 4;}

      if (edgeImg[(i-1)*width+j-1] == 255 && edgeImg[(i-1)*width+j] != 255 && edgeImg[i*width+j-1] != 255){count++; loc = 5;}
      if (edgeImg[(i-1)*width+j+1] == 255 && edgeImg[(i-1)*width+j] != 255 && edgeImg[i*width+j+1] != 255){count++; loc = 6;}
      if (edgeImg[(i+1)*width+j+1] == 255 && edgeImg[(i+1)*width+j] != 255 && edgeImg[i*width+j+1] != 255){count++; loc = 7;}
      if (edgeImg[(i+1)*width+j-1] == 255 && edgeImg[(i+1)*width+j] != 255 && edgeImg[i*width+j-1] != 255){count++; loc = 8;}

      if (count == 0 || count > 1) continue;
  
      // Pixel at the tip of an edge group
      if (loc == 1){
        // Going Down
        // P
        // x
        if (edgeImg[(i+2)*width+j] == 255){edgeImg[(i+1)*width+j] = 128; continue;} // Down

        if (edgeImg[(i+2)*width+j+1] == 255 || edgeImg[(i+2)*width+j+2] == 255 || edgeImg[(i+1)*width+j+2] == 255){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right
        if (edgeImg[(i+2)*width+j-1] == 255 || edgeImg[(i+2)*width+j-2] == 255 || edgeImg[(i+1)*width+j-2] == 255){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

      } else if (loc == 2){
        // Going Up
        // x
        // P
        if (edgeImg[(i-2)*width+j] == 255){edgeImg[(i-1)*width+j] = 128; continue;} // Up

        if (edgeImg[(i-2)*width+j+1] == 255 || edgeImg[(i-2)*width+j+2] == 255 || edgeImg[(i-1)*width+j+2] == 255){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right
        if (edgeImg[(i-2)*width+j-1] == 255 || edgeImg[(i-2)*width+j-2] == 255 || edgeImg[(i-1)*width+j-2] == 255){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left

      } else if (loc == 3){
        // Going Right
        // Px
        if (edgeImg[i*width+j+2] == 255){edgeImg[i*width+j+1] = 128; continue;} // Right

        if (edgeImg[(i-2)*width+j+1] == 255 || edgeImg[(i-2)*width+j+2] == 255 || edgeImg[(i-1)*width+j+2] == 255){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right
        if (edgeImg[(i+2)*width+j+1] == 255 || edgeImg[(i+2)*width+j+2] == 255 || edgeImg[(i+1)*width+j+2] == 255){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right

      } else if (loc == 4){
        // Going Left
        // xP
        if (edgeImg[i*width+j-2] == 255){edgeImg[i*width+j-1] = 128; continue;} // Left

        if (edgeImg[(i-2)*width+j-1] == 255 || edgeImg[(i-2)*width+j-2] == 255 || edgeImg[(i-1)*width+j-2] == 255){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left
        if (edgeImg[(i+2)*width+j-1] == 255 || edgeImg[(i+2)*width+j-2] == 255 || edgeImg[(i+1)*width+j-2] == 255){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

      } else if (loc == 5){
        // Going Down-Right
        // P
        //  x
        if (edgeImg[(i+2)*width+j+1] == 255 || edgeImg[(i+2)*width+j+2] == 255 || edgeImg[(i+1)*width+j+2] == 255){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right

        if (edgeImg[i*width+j+2] == 255){edgeImg[i*width+j+1] = 128; continue;} // Down
        if (edgeImg[i*width+j+2] == 255){edgeImg[i*width+j+1] = 128; continue;} // Right

        if (edgeImg[(i+2)*width+j-1] == 255 || edgeImg[(i+2)*width+j-2] == 255 || edgeImg[(i+1)*width+j-2] == 255){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left
        if (edgeImg[(i-2)*width+j+1] == 255 || edgeImg[(i-2)*width+j+2] == 255 || edgeImg[(i-1)*width+j+2] == 255){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right

      } else if (loc == 6){
        // Going Down-Left
        //  P
        // x
        if (edgeImg[(i+2)*width+j-1] == 255 || edgeImg[(i+2)*width+j-2] == 255 || edgeImg[(i+1)*width+j-2] == 255){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

        if (edgeImg[i*width+j+2] == 255){edgeImg[i*width+j+1] = 128; continue;} // Down
        if (edgeImg[i*width+j-2] == 255){edgeImg[i*width+j-1] = 128; continue;} // Left

        if (edgeImg[(i+2)*width+j+1] == 255 || edgeImg[(i+2)*width+j+2] == 255 || edgeImg[(i+1)*width+j+2] == 255){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right
        if (edgeImg[(i-2)*width+j-1] == 255 || edgeImg[(i-2)*width+j-2] == 255 || edgeImg[(i-1)*width+j-2] == 255){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left

      } else if (loc == 7){
        // Going Up-Left
        // x
        //  P
        if (edgeImg[(i-2)*width+j-1] == 255 || edgeImg[(i-2)*width+j-2] == 255 || edgeImg[(i-1)*width+j-2] == 255){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left

        if (edgeImg[(i-2)*width+j] == 255){edgeImg[(i-1)*width+j] = 128; continue;} // Up
        if (edgeImg[i*width+j-2] == 255){edgeImg[i*width+j-1] = 128; continue;} // Left

        if (edgeImg[(i-2)*width+j+1] == 255 || edgeImg[(i-2)*width+j+2] == 255 || edgeImg[(i-1)*width+j+2] == 255){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right
        if (edgeImg[(i+2)*width+j-1] == 255 || edgeImg[(i+2)*width+j-2] == 255 || edgeImg[(i+1)*width+j-2] == 255){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

      } else { //if (loc == 8){
        // Going Up-Right
        //  x
        // P
        if (edgeImg[(i-2)*width+j+1] == 255 || edgeImg[(i-2)*width+j+2] == 255 || edgeImg[(i-1)*width+j+2] == 255){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right

        if (edgeImg[(i-2)*width+j] == 255){edgeImg[(i-1)*width+j] = 128; continue;} // Up
        if (edgeImg[i*width+j+2] == 255){edgeImg[i*width+j+1] = 128; continue;} // Right

        if (edgeImg[(i-2)*width+j-1] == 255 || edgeImg[(i-2)*width+j-2] == 255 || edgeImg[(i-1)*width+j-2] == 255){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left
        if (edgeImg[(i+2)*width+j+1] == 255 || edgeImg[(i+2)*width+j+2] == 255 || edgeImg[(i+1)*width+j+2] == 255){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right
      } //end-else 
    } //end-for
  } //end-for

  for (int i=0; i<width*height; i++) if (edgeImg[i] == 128) edgeImg[i] = 255;
} //end-FillGaps2

///---------------------------------------------------------------
/// Close gaps of up to 2 pixels wide: Joins the tip of an edge group to ANY edgel
///
void FillGaps3(unsigned char *edgeImg, int width, int height){
  for (int i=2; i<height-2; i++){
    for (int j=2; j<width-2; j++){
      if (edgeImg[i*width+j] != 255) continue;

      int count = 0;
      int loc = 1;
      if (edgeImg[(i-1)*width+j] > 128) count++;
      if (edgeImg[(i+1)*width+j] > 128){count++; loc = 2;}
      if (edgeImg[i*width+j-1] > 128){count++; loc = 3;}
      if (edgeImg[i*width+j+1] > 128){count++; loc = 4;}

      if (edgeImg[(i-1)*width+j-1] > 128 && edgeImg[(i-1)*width+j] <= 128 && edgeImg[i*width+j-1] <= 128){count++; loc = 5;}
      if (edgeImg[(i-1)*width+j+1] > 128 && edgeImg[(i-1)*width+j] <= 128 && edgeImg[i*width+j+1] <= 128){count++; loc = 6;}
      if (edgeImg[(i+1)*width+j+1] > 128 && edgeImg[(i+1)*width+j] <= 128 && edgeImg[i*width+j+1] <= 128){count++; loc = 7;}
      if (edgeImg[(i+1)*width+j-1] > 128 && edgeImg[(i+1)*width+j] <= 128 && edgeImg[i*width+j-1] <= 128){count++; loc = 8;}

      if (count == 0 || count > 1) continue;
  
      edgeImg[i*width+j] = 128+loc;

      // Pixel at the tip of an edge group
      if (loc == 1){
        // Going Down
        // P
        // x
        if (edgeImg[(i+2)*width+j] > 128){edgeImg[(i+1)*width+j] = 128; continue;} // Down

        if (edgeImg[(i+2)*width+j+1] > 128 || edgeImg[(i+2)*width+j+2] > 128 || edgeImg[(i+1)*width+j+2] > 128){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right
        if (edgeImg[(i+2)*width+j-1] > 128 || edgeImg[(i+2)*width+j-2] > 128 || edgeImg[(i+1)*width+j-2] > 128){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

      } else if (loc == 2){
        // Going Up
        // x
        // P
        if (edgeImg[(i-2)*width+j] > 128){edgeImg[(i-1)*width+j] = 128; continue;} // Up

        if (edgeImg[(i-2)*width+j+1] > 128 || edgeImg[(i-2)*width+j+2] > 128 || edgeImg[(i-1)*width+j+2] > 128){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right
        if (edgeImg[(i-2)*width+j-1] > 128 || edgeImg[(i-2)*width+j-2] > 128 || edgeImg[(i-1)*width+j-2] > 128){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left

      } else if (loc == 3){
        // Going Right
        // Px
        if (edgeImg[i*width+j+2] > 128){edgeImg[i*width+j+1] = 128; continue;} // Right

        if (edgeImg[(i-2)*width+j+1] > 128 || edgeImg[(i-2)*width+j+2] > 128 || edgeImg[(i-1)*width+j+2] > 128){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right
        if (edgeImg[(i+2)*width+j+1] > 128 || edgeImg[(i+2)*width+j+2] > 128 || edgeImg[(i+1)*width+j+2] > 128){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right

      } else if (loc == 4){
        // Going Left
        // xP
        if (edgeImg[i*width+j-2] > 128){edgeImg[i*width+j-1] = 128; continue;} // Left

        if (edgeImg[(i-2)*width+j-1] > 128 || edgeImg[(i-2)*width+j-2] > 128 || edgeImg[(i-1)*width+j-2] > 128){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left
        if (edgeImg[(i+2)*width+j-1] > 128 || edgeImg[(i+2)*width+j-2] > 128 || edgeImg[(i+1)*width+j-2] > 128){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

      } else if (loc == 5){
        // Going Down-Right
        // P
        //  x
        if (edgeImg[(i+2)*width+j+1] > 128 || edgeImg[(i+2)*width+j+2] > 128 || edgeImg[(i+1)*width+j+2] > 128){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right

        if (edgeImg[i*width+j+2] > 128){edgeImg[i*width+j+1] = 128; continue;} // Down
        if (edgeImg[i*width+j+2] > 128){edgeImg[i*width+j+1] = 128; continue;} // Right

        if (edgeImg[(i+2)*width+j-1] > 128 || edgeImg[(i+2)*width+j-2] > 128 || edgeImg[(i+1)*width+j-2] > 128){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left
        if (edgeImg[(i-2)*width+j+1] > 128 || edgeImg[(i-2)*width+j+2] > 128 || edgeImg[(i-1)*width+j+2] > 128){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right

      } else if (loc == 6){
        // Going Down-Left
        //  P
        // x
        if (edgeImg[(i+2)*width+j-1] > 128 || edgeImg[(i+2)*width+j-2] > 128 || edgeImg[(i+1)*width+j-2] > 128){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

        if (edgeImg[i*width+j+2] > 128){edgeImg[i*width+j+1] = 128; continue;} // Down
        if (edgeImg[i*width+j-2] > 128){edgeImg[i*width+j-1] = 128; continue;} // Left

        if (edgeImg[(i+2)*width+j+1] > 128 || edgeImg[(i+2)*width+j+2] > 128 || edgeImg[(i+1)*width+j+2] > 128){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right
        if (edgeImg[(i-2)*width+j-1] > 128 || edgeImg[(i-2)*width+j-2] > 128 || edgeImg[(i-1)*width+j-2] > 128){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left

      } else if (loc == 7){
        // Going Up-Left
        // x
        //  P
        if (edgeImg[(i-2)*width+j-1] > 128 || edgeImg[(i-2)*width+j-2] > 128 || edgeImg[(i-1)*width+j-2] > 128){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left

        if (edgeImg[(i-2)*width+j] > 128){edgeImg[(i-1)*width+j] = 128; continue;} // Up
        if (edgeImg[i*width+j-2] > 128){edgeImg[i*width+j-1] = 128; continue;} // Left

        if (edgeImg[(i-2)*width+j+1] > 128 || edgeImg[(i-2)*width+j+2] > 128 || edgeImg[(i-1)*width+j+2] > 128){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right
        if (edgeImg[(i+2)*width+j-1] > 128 || edgeImg[(i+2)*width+j-2] > 128 || edgeImg[(i+1)*width+j-2] > 128){edgeImg[(i+1)*width+j-1] = 128; continue;} // Down-Left

      } else { //if (loc == 8){
        // Going Up-Right
        //  x
        // P
        if (edgeImg[(i-2)*width+j+1] > 128 || edgeImg[(i-2)*width+j+2] > 128 || edgeImg[(i-1)*width+j+2] > 128){edgeImg[(i-1)*width+j+1] = 128; continue;} // Up-Right

        if (edgeImg[(i-2)*width+j] > 128){edgeImg[(i-1)*width+j] = 128; continue;} // Up
        if (edgeImg[i*width+j+2] > 128){edgeImg[i*width+j+1] = 128; continue;} // Right

        if (edgeImg[(i-2)*width+j-1] > 128 || edgeImg[(i-2)*width+j-2] > 128 || edgeImg[(i-1)*width+j-2] > 128){edgeImg[(i-1)*width+j-1] = 128; continue;} // Up-Left
        if (edgeImg[(i+2)*width+j+1] > 128 || edgeImg[(i+2)*width+j+2] > 128 || edgeImg[(i+1)*width+j+2] > 128){edgeImg[(i+1)*width+j+1] = 128; continue;} // Down-Right
      } //end-else 
    } //end-for
  } //end-for

#if 0
  // Fill 2 pixel gaps
  for (int i=3; i<height-3; i++){
    for (int j=3; j<width-3; j++){
      if (edgeImg[i*width+j] == 0) continue;
      if (edgeImg[i*width+j] == 255) continue;
      if (edgeImg[i*width+j] == 128) continue;

      if (edgeImg[i*width+j] == 128 + 1){      // Down
        if (edgeImg[(i+3)*width+j] == 128 + 2 && edgeImg[(i+1)*width+j] <= 128 && edgeImg[(i+2)*width+j] <= 128){
          edgeImg[(i+1)*width+j] = 128;
          edgeImg[(i+2)*width+j] = 128;
        } //end-if
      } //end-if

      else if (edgeImg[i*width+j] == 128 + 3){ // Right
        if (edgeImg[i*width+j+3] == 128 + 4 && edgeImg[i*width+j+1] <= 128 && edgeImg[i*width+j+2] <= 128){
          edgeImg[i*width+j+1] = 128;
          edgeImg[i*width+j+2] = 128;
        } //end-if
      } //end-if      

#if 0
      // Diagonals?
      else if (edgeImg[i*width+j] == 128 + 5){ // Down-Right
        if (edgeImg[(i+3)*width+j+3] == 128 + 7 && edgeImg[(i+1)*width+j+1] <= 128 && edgeImg[(i+2)*width+j+2] <= 128){
          edgeImg[(i+1)*width+j+1] = 128;
          edgeImg[(i+2)*width+j+2] = 128;
        } //end-if
      } //end-if      

      else if (edgeImg[i*width+j] == 128 + 6){ // Down-Left
        if (edgeImg[(i+3)*width+j-3] == 128 + 8 && edgeImg[(i+1)*width+j-1] <= 128 && edgeImg[(i+2)*width+j-2] <= 128){
          edgeImg[(i+1)*width+j-1] = 128;
          edgeImg[(i+2)*width+j-2] = 128;
        } //end-if
      } //end-if      
#endif
    } //end-for
  } //end-for
#endif

  for (int i=0; i<width*height; i++) if (edgeImg[i] >= 128) edgeImg[i] = 255;
} //end-FillGaps3

///---------------------------------------------------------------------
/// Clip from the tips of the edge segments if there is a neigboring segment
/// maxClipSize is the maximum # of pixels to clip from the tips of the edge segments
///
void ClipEdgeSegments(EdgeMap *map, int maxClipSize=5){
  int width = map->width;
  int height = map->height;
    
  unsigned char *joints = FindJointPoints(map);

  for (int i=0; i<map->noSegments; i++){
    // The loopy segments should not be broken
    int fr = map->segments[i].pixels[0].r;
    int fc = map->segments[i].pixels[0].c;

    int lr = map->segments[i].pixels[map->segments[i].noPixels-1].r;
    int lc = map->segments[i].pixels[map->segments[i].noPixels-1].c;

    // Skip this segment if it forms a loop
    if (abs(fr-lr) <= 3 && abs(fc-lc) <= 3) continue;

    for (int k=0; k<map->segments[i].noPixels; k++){
      int r = map->segments[i].pixels[k].r;
      int c = map->segments[i].pixels[k].c;  

      if (joints[r*width+c]){
        if (k <= maxClipSize){
          map->segments[i].pixels += k;
          map->segments[i].noPixels -= k;

          joints[r*width+c] = 0;
        } //end-if

        break;
      } //end-if
    } //end-for

    for (int k=map->segments[i].noPixels-1; k>=0; k--){
      int r = map->segments[i].pixels[k].r;
      int c = map->segments[i].pixels[k].c;  

      if (joints[r*width+c]){
        if (map->segments[i].noPixels - k <= maxClipSize){
          map->segments[i].noPixels = k+1;

          joints[r*width+c] = 0;
        } //end-if

        break;
      } //end-if
    } //end-for

  } //end-for

  delete joints;
} //end-ClipEdgeSegments

///---------------------------------------------------------------------
/// Join edge segments whose endpoints are at most 2 pixels away from each other
///
void JoinNeighborEdgeSegments(EdgeMap *map){
  // Clip the tips of the edge segments
  ClipEdgeSegments(map, 5);

  int width = map->width;
  int height = map->height;

  int *segments = new int[width*height];
  memset(segments, 0, sizeof(int)*width*height);

  // Mark the end of the segments on the "segments" array
  for (int i=0; i<map->noSegments; i++){
    int r, c;

    r = map->segments[i].pixels[0].r;
    c = map->segments[i].pixels[0].c;
    segments[r*width+c] = i+1;


    int index = map->segments[i].noPixels-1;
    r = map->segments[i].pixels[index].r;
    c = map->segments[i].pixels[index].c;
    segments[r*width+c] = i+1;
  } //end-for

  // Find the neighbors of each segment in the 2x2 neighborhood
  struct NN {
    bool taken;   // Is this segment already taken?
    int s, e;     // Neighbor from the start and end pixel
  };        

  NN *nn = new NN[map->noSegments];

  // Find the neighbors of each segment
  for (int i=0; i<map->noSegments; i++){
    int r, c;

    nn[i].taken = false;

    r = map->segments[i].pixels[0].r;
    c = map->segments[i].pixels[0].c;

    int neighbor = -1;
    int len = 0;
    for (int m=r-2; m<=r+2; m++){
      if (m < 0 || m >= height) continue;

      for (int n=c-2; n<=c+2; n++){
        if (n < 0 || n >= width) continue;
        if (segments[m*width+n] == 0) continue;
        if (segments[m*width+n] == i+1) continue;

        int s = segments[m*width+n]-1;
        if (map->segments[s].noPixels > len){neighbor=s; len = map->segments[s].noPixels;}
      } //end-for
    } //end-for
    nn[i].s = neighbor;

    int index = map->segments[i].noPixels-1;
    r = map->segments[i].pixels[index].r;
    c = map->segments[i].pixels[index].c;

    neighbor = -1;
    len = 0;
    for (int m=r-2; m<=r+2; m++){
      if (m < 0 || m >= height) continue;

      for (int n=c-2; n<=c+2; n++){
        if (n < 0 || n >= width) continue;
        if (segments[m*width+n] == 0) continue;
        if (segments[m*width+n] == i+1) continue;

        int s = segments[m*width+n]-1;
        if (map->segments[s].noPixels > len){neighbor=s; len = map->segments[s].noPixels;}
      } //end-for
    } //end-for

    nn[i].e = neighbor;
    if (nn[i].e == nn[i].s) nn[i].e = -1;
  } //end-for

  // Now join. Create a new edgemap for the joined edge segments
  int noSegments2 = 0;
  EdgeSegment *segments2 = new EdgeSegment[map->noSegments*4];
  Pixel *pix2 = map->segments[map->noSegments-1].pixels + map->segments[map->noSegments-1].noPixels;
  int *listBuffer = new int[map->noSegments*2];

  for (int i=0; i<map->noSegments; i++){
    // Already taken? Then skip.
    if (nn[i].taken) continue;

    // Segment with no neighbors? Just copy the pixels
    if (nn[i].s == -1 && nn[i].e == -1){
      nn[i].taken = true;

      segments2[noSegments2].pixels = map->segments[i].pixels;
      segments2[noSegments2].noPixels = map->segments[i].noPixels;
      pix2 += segments2[noSegments2].noPixels;
      noSegments2++;

      continue;
    } //end-if

    // At least 2 segments to join. First find the segments, and then join them
    int *list = listBuffer + map->noSegments;
    int listSize = 0;

    if (nn[i].s >= 0 || nn[i].e >= 0){
      int prev = i;

      list--;
      list[0] = prev;
      listSize++;
      nn[prev].taken = true; // Mark this segment as taken

      int curr = nn[i].s;
      while (curr >= 0 && nn[curr].taken == false){
        list--;
        list[0] = curr;
        listSize++;
        nn[curr].taken = true; // Mark this segment as taken

        if (prev == nn[curr].s){prev = curr; curr = nn[curr].e;}
        else {prev = curr; curr = nn[curr].s;}
      } //end-while

      // Walk from "e" to the next neighbors
      prev = i;
      curr = nn[i].e;
      while (curr >= 0 && nn[curr].taken == false){
        list[listSize] = curr;
        listSize++;
        nn[curr].taken = true; // Mark this segment as taken

        if (prev == nn[curr].s){prev = curr; curr = nn[curr].e;}
        else {prev = curr; curr = nn[curr].s;}
      } //end-while
    } //end-if

    // Now join the pixels of the segments properly
    int noPixels = 0;
    segments2[noSegments2].pixels = pix2;

    // Copy the pixels of the first segment in the list
    // This is junction (curr, next)
    int curr = list[0];
    int next = list[1];

    if (nn[curr].e == next){
      // Copy the pixels of the current segment in forward order
      for (int k=0; k<map->segments[curr].noPixels; k++){
        segments2[noSegments2].pixels[noPixels++] = map->segments[curr].pixels[k];
      } //end-for      

    } else {
      // Copy the pixels of the current segment in reverse order
      for (int k=map->segments[curr].noPixels-1; k>=0; k--){
        segments2[noSegments2].pixels[noPixels++] = map->segments[curr].pixels[k];
      } //end-for      
    } // end-else

    // Now copy the pixels of the rest of the segments
    for (int index = 1; index < listSize; index++){
      int prev = list[index]-1;
      curr = list[index];

      // This is junction (prev, curr)
      if (nn[curr].s == prev){
        // Copy the pixels of the current segment in forward order
        for (int k=0; k<map->segments[curr].noPixels; k++){
          segments2[noSegments2].pixels[noPixels++] = map->segments[curr].pixels[k];
        } //end-for      

      } else {
        // Copy the pixels of the current segment in reverse order
        for (int k=map->segments[curr].noPixels-1; k>=0; k--){
          segments2[noSegments2].pixels[noPixels++] = map->segments[curr].pixels[k];
        } //end-for      
      } // end-else
    } // end-while  

    segments2[noSegments2].noPixels = noPixels;
    pix2 += noPixels;
    noSegments2++;
  } //end-for

  map->noSegments = noSegments2;
  delete map->segments;
  map->segments = segments2;

  delete listBuffer;
  delete nn;
  delete segments;
} //end-JoinEdgeSegments



#ifndef _EDGE_SEGMENTS_H_
#define _EDGE_SEGMENTS_H_

#include "EdgeMap.h"

/// Dump edge segments to file
void DumpEdgeSegments2File(EdgeMap *map, char *fname);

/// Splits segments into multiple segments if they contain closed objects in them
void SplitEdgeSegments(EdgeMap *map);

/// Thins down edge segment by removing superfulous pixels in the chain
void ThinEdgeSegments(EdgeMap *map, int MIN_SEGMENT_LEN);

/// Deneme
void ThinEdgeSegments2(EdgeMap *map, int MIN_SEGMENT_LEN);

/// Sorts the edge segments with respect to their length with the longest at the beginning
void SortEdgeSegments(EdgeMap *map);

/// Returns the current set of joint points
unsigned char *FindJointPoints(EdgeMap *map);

/// Given an edgemap, tries to extend the current edge segments
/// Returns the new extended edgemap
EdgeMap *ExtendEdgeSegments(EdgeMap *map);

/// Given a binary edgemap, determines the end-points of the edge segments
/// We will connect these end-points to link disconneced edge segments (edge linking)
/// Returns a new buffer containing the endpoints
unsigned char *FindEdgeSegmentEndPoints(unsigned char *edgeImg, int width, int height);

/// Given a binary edge map, fill the gaps between edge end-points by using templates
void FillEdgeGaps(unsigned char *smoothImg, unsigned char *edgeImg, int width, int height);

/// Given a binary edge map, fills up to 1 pixel gaps between edge groups: Connects only the TIPS of two edge groups
void FillGaps1(unsigned char *edgeImg, int width, int height);

/// Given a binary edge map, fills up to 1 pixel gaps between edge groups.
/// These are different than FillGaps1 in that they connect the tip of an edge group to ANY neighbouring edgel!
void FillGaps2(unsigned char *edgeImg, int width, int height);
void FillGaps3(unsigned char *edgeImg, int width, int height);

/// Fix one or two pixel crookedness in the edge segments
void FixEdgeSegments(EdgeMap *map, int noPixels=1);

/// Join edge segments whose end points are at most 2 pixels away from each other
void JoinNeighborEdgeSegments(EdgeMap *map);

#endif
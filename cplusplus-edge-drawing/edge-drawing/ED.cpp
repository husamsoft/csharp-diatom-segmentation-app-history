#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#include "EDInternals.h"
#include "ED.h"
#include "GradientOperators.h"
#include "ValidateEdgeSegments.h"

#include "EdgeSegments.h"


///----------------------------------------------------------------------------------------------
/// Detect edges by the Edge Drawing method (ED), and validate the resulting edge segments
/// 
EdgeMap *GrayEDV(unsigned char *srcImg, int width, int height, GradientOperator op, int GRADIENT_THRESH){
  // Check parameters for sanity
  if (GRADIENT_THRESH < 1) GRADIENT_THRESH = 1;
  int ANCHOR_THRESH = 0;

  // Allocate space for temporary storage
  unsigned char *dirImg = new unsigned char[width*height];
  short *gradImg = new short[width*height];

  /*------------ COMPUTE GRADIENT & EDGE DIRECTION MAPS -------------------*/
  switch (op){
  case PREWITT_OPERATOR: ComputeGradientMapByPrewitt(srcImg, gradImg, dirImg, width, height, GRADIENT_THRESH); break;
  case SOBEL_OPERATOR:   ComputeGradientMapBySobel(srcImg, gradImg, dirImg, width, height, GRADIENT_THRESH);   break;
  case SCHARR_OPERATOR:  ComputeGradientMapByScharr(srcImg, gradImg, dirImg, width, height, GRADIENT_THRESH);  break;
  } //end-switch

#if 0
  DumpGradImage("OutputImages/GradImgBW.bmp", gradImg, width, height, GRADIENT_THRESH);
#endif

  // Now, detect the edges by ED
  EdgeMap *map = DoDetectEdgesByED(gradImg, dirImg, width, height, GRADIENT_THRESH, ANCHOR_THRESH);

  /*----------- Validate Edge Segments ---------------------*/
#if 0
  ValidateEdgeSegments(map, srcImg);
#elif 1
  // This works and was the previous preferred settings. Don't use?
  ValidateEdgeSegments(map, srcImg, 2.25);  // With Prewitt
#elif 1
  // Removes the noise better compared to the previous setting. Also removes some valid segments
  smoothingSigma /= 2.5;
  SmoothImage(srcImg, smoothImg, width, height, smoothingSigma);
  ValidateEdgeSegments(map, smoothImg, 2.5);  // With Prewitt
#elif 1
  double div2 = 2.5;
  if (smoothingSigma >= 2.0) div2 = 2.25;

  smoothingSigma /= 2.5;
  SmoothImage(srcImg, smoothImg, width, height, smoothingSigma);
  ValidateEdgeSegments(map, smoothImg, div2);  // With Prewitt
#elif 0
  double a1 = 4.828;
  double b1 = 6.423;
  double c1 = 3.307;
  double d = (smoothingSigma-b1)/c1;
  double validationSigma;
  
  if (smoothingSigma <= 6.0) validationSigma = a1 * exp(-(d*d));
  else                       validationSigma = smoothingSigma/1.25;

//  extern double validationSigma;

  SmoothImage(srcImg, smoothImg, width, height, validationSigma);
  ValidateEdgeSegments(map, smoothImg, 3);  // With Prewitt
#endif

  // Clean up
  delete gradImg;
  delete dirImg;

  // Fix 1 pixel errors in the edge map
  FixEdgeSegments(map, 1);

  return map;
} //GrayEDV

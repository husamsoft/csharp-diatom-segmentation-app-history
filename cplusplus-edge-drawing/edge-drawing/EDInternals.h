#ifndef _ED_INTERNALS_H_
#define _ED_INTERNALS_H_

#include "EdgeMap.h"

/// Computes the anchors & links them. Returns the edge map
EdgeMap *DoDetectEdgesByED(short *gradImg, unsigned char *dirImg, int width, int height, int GRADIENT_THRESH, int ANCHOR_THRESH=0, bool selectStableAnchors=false);
EdgeMap *DoDetectEdgesByED4Dirs(short *gradImg, unsigned char *dirImg, int width, int height, int GRADIENT_THRESH, int ANCHOR_THRESH=0, bool selectStableAnchors=false);

#endif
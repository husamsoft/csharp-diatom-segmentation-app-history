﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace csharp_image_segmentation_app
{
    static class Program
    {
        private static string TEST_IMAGE = "IO/Input/000068.bmp";

        private static string RESULT_IMAGE = "IO/Output/result3.bmp";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static unsafe void Main()
        {
            Bitmap temp = (Bitmap)Bitmap.FromFile(TEST_IMAGE);
            /*
            EdgeMap em = EdHelper.ToEdgeMap(temp, 24);

            em.ToBitmap().Save(RESULT_IMAGE, ImageFormat.Bmp);            
            */
            byte[] bytes = Utils.BitmapToGray1DbyteArray(temp);

            double* doubles = (double*)Marshal.AllocHGlobal(temp.Width * temp.Height * sizeof(double));

            for (int i = 0; i < bytes.Length; i++)
            {
                doubles[i] = (double) bytes[i];
            }

            //SCWrapper.ContourOperations op = new SCWrapper.ContourOperations();

            //int* serial = op.GetSmoothContours(doubles, temp.Width, temp.Height, 2);


            for (int i = 0; i < bytes.Length ; i++)
            {
               // Console.WriteLine(serial[i]);
            }
            
            /*
            Console.WriteLine(em.NoSegments);

            GradientOperators op = new GradientOperators();

            byte[] byteImage = Utils.BitmapToGray1DbyteArray(temp);

            GradientMap map = op.ComputeGradientMapByPrewitt2(byteImage, temp.Width, temp.Height);


            map.Evaluate();

            Bitmap b = map.ToBitmap(GradientMap.Types.G);

            b.Save(@"C:\Users\27475003794ana\Desktop\gradinet.bmp", ImageFormat.Bmp);

            b = map.ToBitmap(GradientMap.Types.Gdir);
            b.Save(@"C:\Users\27475003794ana\Desktop\dir.bmp", ImageFormat.Bmp);
            */
            /*
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmBenchmark());               
            */
        }
    }
}


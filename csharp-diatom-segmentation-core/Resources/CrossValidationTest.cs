﻿using csharp_image_segmentation_app.Sources.Classification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Resources
{
    class CrossValidationTest
    {
        public void Test()
        {
            CrossValidationBuilder builder = new CrossValidationBuilder(10);
            for (int i = 1; i < 12; i++)
            {
                Descriptor d = new Descriptor();
                double[] v = new double[3];
                v[0] = 0.1 * i;
                v[1] = 0.1 * i;
                v[2] = 0.1 * i;

                d.Label = 1;
                d.Vector = v;
                builder.Add(d);

            }

            for (int i = 1; i < 15; i++)
            {
                Descriptor d = new Descriptor();
                double[] v = new double[3];
                v[0] = 0.2 * i;
                v[1] = 0.2 * i;
                v[2] = 0.2 * i;

                d.Label = 2;
                d.Vector = v;
                builder.Add(d);
            }

            builder.CreateLabels();
            builder.BuildData(1);
            /*
            foreach (int classid in builder.TestData.Keys)
            {
                Console.WriteLine("Class id : " + classid);

                foreach (Descriptor d in builder.TestData[classid])
                {
                    Console.Write(d.Label + ": ");

                    foreach (double v in d.Vector)
                    {
                        Console.Write(v + " ");
                    }
                    Console.WriteLine();
                    //Console.WriteLine(d.Label);
                }
                Console.WriteLine();
            }

            Console.WriteLine("Train");

            foreach (int classid in builder.TrainData.Keys)
            {
                Console.WriteLine("Class id : " + classid);

                foreach (Descriptor d in builder.TrainData[classid])
                {
                    Console.Write(d.Label + ": ");

                    foreach (double v in d.Vector)
                    {
                        Console.Write(v + " ");
                    }
                    Console.WriteLine();
                    //Console.WriteLine(d.Label);
                }
                Console.WriteLine();
            }
            */
        }

    }
}

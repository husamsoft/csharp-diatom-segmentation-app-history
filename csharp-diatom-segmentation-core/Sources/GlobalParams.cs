﻿using System;
using System.Collections.Generic;
using System.IO;

namespace csharp_image_segmentation_app.Model
{
    public class GlobalParams
    {
        private const string LabelsFileName = @"C:\Users\27475003794ana\Desktop\csharp-diatom-segmentation-app\csharp-image-segmentation-core\csharp-image-segmentation-app\Resources\labels.txt";

        public const string IID_TEST = "iids_test.txt";
        public const string IID_TRAIN = "iids_train.txt";

        public const string FOLDER_IMAGE = "images";
        public const string FOLDER_GT = "human_gt";    
        public const string FOLDER_LABELS = "human_labels";

        // Image
        public const string SuffixEdgeData = "_GT_Edges";
        public const string ExtensionImage = ".bmp";

        // Roi 
        public const string ExtensionLabel = ".txt";
        public const string SuffixLabel = "_Label_Info";
        public const string Eof = "####";

        // Svm Related
        public const string MODEL_ROOT_PATH = @"C:\Users\\27475003794ana\Desktop\models";
        public const string MODEL_NAME = "model";
        public const string MODEL_INFO_NAME = "info.txt";

        public const string CONFUSION_MATRIX_PATH = @"C:\Users\\27475003794ana\Desktop\conf.txt";

        public const int ExtentRation = 5;

        public static Dictionary<int, string> LabelLookUp;
 
        static GlobalParams()
        {
            LabelLookUp = new Dictionary<int, string>();

            string[] lines = null;

            try {
                lines = File.ReadAllLines(@LabelsFileName);
            }
            catch
            {
                throw new Exception(" Cannot read label file!");
            }

            foreach (var line in lines)
            {
                var splits = line.Split('\t');

                if (splits.Length != 2)
                {
                    throw new Exception("Labels text is invalid format!");
                }

                var id = Convert.ToInt32(splits[0]);
                var name = splits[1];

                if (!LabelLookUp.ContainsKey(id))
                {
                    LabelLookUp.Add(id, name);
                }
                else
                {
                    throw new Exception(name + " : Two label in same id!");
                }
            }
        }                
    }
}

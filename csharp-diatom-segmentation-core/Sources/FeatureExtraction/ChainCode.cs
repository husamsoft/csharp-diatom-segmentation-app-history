﻿using System;

namespace csharp_image_segmentation_app.Sources.Helpers
{
    class ChainCode
    {

        /****************************
         *     |  0 | +1 | 2 |      *
         *     |  0 | -1 | 6 |      *
         *     | -1 | +1 | 3 |      *    3  2  1
         *     | -1 | -1 | 5 |      *     \ | /
         *     | +1 | +1 | 1 |      *  4 -- P -- 0
         *     | +1 | -1 | 7 |      *     / | \
         *     | -1 |  0 | 4 |      *    5  6  7
         *     | +1 |  0 | 0 |      *
        ****************************/
        public static double[] Extract(Pixel[] pixels)
        {
            int[] hist = new int[8];

            for (int i = 1; i < pixels.Length; i++)
            {
                int dc = pixels[i - 1].C - pixels[i].C;
                int dr = pixels[i - 1].R - pixels[i].R;

                int code;

                if (dc == 0 && dr == 1)
                {
                    code = 2;
                }
                else if (dc == 0 && dr == -1)
                {
                    code = 6;
                }
                else if (dc == -1 && dr == 1)
                {
                    code = 3;
                }
                else if (dc == -1 && dr == -1)
                {
                    code = 5;
                }
                else if (dc == 1 && dr == 1)
                {
                    code = 1;
                }
                else if (dc == 1 && dr == -1)
                {
                    code = 7;
                }
                else if (dc == -1 && dr == 0)
                {
                    code = 4;
                }
                else if (dc == 1 && dr == 0)
                {
                    code = 0;
                }
                else
                {
                    code = 0;

                    //break;
                    Console.WriteLine("Other");
                }

                hist[code]++;
            }
           
            double[] normal = new double[8];

            double max = double.MinValue;
            double min = double.MaxValue;

            foreach (int h in hist)
            {
                if (h > max) max = h;

                if (h < min) min = h;
            }            

            for (int i = 0;i < 8; i++)
            {
                double smallNumber = 0;

                if (max - min == 0)
                {
                    //Console.WriteLine("Zero division exception");
                    smallNumber = 0.001;
                }                

                normal[i] = ((double)hist[i] - min) / (max - min + smallNumber);

            }
            /*
            for (int j = 0; j < 8; j++)
            {
                Console.Write(normal[j].ToString("0.000") + " ");
            }

            Console.WriteLine();
            */

            return normal;
        }
    }
}

﻿using csharp_image_segmentation_app.Sources.Classification;
using csharp_image_segmentation_app.Sources.Dataset;
using csharp_image_segmentation_app.Sources.Helpers;
using csharp_image_segmentation_app.Sources.TemplateOperations;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Sources.Segmentation
{
    class PreProcess
    {
        private static string PATH_TEMPLATE = @"C:\Users\27475003794ana\Desktop\temp\templates.tm";

        private static string PATH_NEGATIVE_IMAGE = @"C:\Users\\27475003794ana\Desktop\__000094.bmp";

        private static string PATH_NEGATIVE_IMAGE_EDGES = @"C:\Users\\27475003794ana\Desktop\negativeEdges.bmp";

        private static string PATH_TEST_IMAGE = @"C:\Users\\27475003794ana\Desktop\001284.bmp";

        private static string PATH_RESULT_IMAGE = @"C:\Users\\27475003794ana\Desktop\result.bmp";

        private static string PATH_BEFORE_ELIMINATION = @"C:\Users\\27475003794ana\Desktop\beforeelimination.bmp";

        private static string PATH_TEST_IMAGE_ROOT = @"C:\Users\27475003794ana\Documents\Dataset\Diatom\DSDS500\images\test";

        private static string PATH_TEMPORAY_TEST_RESULT_ROOT = @"C:\Users\27475003794ana\Desktop\test";

        public void Work()
        {
            //TemplateBuilder.BuildTemplate(@"C:\Users\27475003794ana\Documents\Dataset\Diatom\DSDS500", @"C:\Users\27475003794ana\Desktop\temp\templates.tm");
            //return;

            List<Descriptor> descriptors = new List<Descriptor>();

            Dictionary<ClassLabel, Template> templates = TemplateBuilder.ReadFromBinaryFile<Dictionary<ClassLabel, Template>>(PATH_TEMPLATE);

            int len = 100, overlapt = 25;

            // Positive Class
            foreach (Template template in templates.Values)
            {
                foreach (EdgeSegment segment in template.EdgeMap.Segments)
                {
                    List<Pixel[]> pixels = Utils.SplitAndOverlaptEdgeSegmentByValues(segment, len, overlapt);

                    foreach (Pixel[] pixel in pixels)
                    {
                        double[] v = ChainCode.Extract(pixel);

                        descriptors.Add(new Descriptor(1, v));
                    }
                }
            }

            // Negative Class
            List<Pixel[]> negative = new List<Pixel[]>();

            Bitmap temp = (Bitmap)Bitmap.FromFile(PATH_NEGATIVE_IMAGE);

            EdgeMap em = EdHelper.ToEdgeMap(temp, 0);

            foreach (EdgeSegment segment in em.Segments)
            {
                List<Pixel[]> pixels = Utils.SplitAndOverlaptEdgeSegmentByValues(segment, len, overlapt);

                foreach (Pixel[] pixel in pixels)
                {
                    double[] v = ChainCode.Extract(pixel);

                    descriptors.Add(new Descriptor(-1, v));
                }
            }

            Utils.Invert(em.ToBitmap()).Save(PATH_NEGATIVE_IMAGE_EDGES, ImageFormat.Bmp);

            // Test
            SvmClassification classification = new SvmClassification();
            classification.Train(descriptors);

            string[] files = Directory.GetFiles(PATH_TEST_IMAGE_ROOT, "*.*", SearchOption.AllDirectories);

            foreach (string path in files)
            //string path = @"C:\Users\27475003794ana\Desktop\001049.bmp";
            {
                Bitmap testImage = (Bitmap)Bitmap.FromFile(path);

                EdgeMap testEdgeMap = EdHelper.ToEdgeMap(testImage, 60);
                EdgeMap dummyEdgeMap = new EdgeMap(testImage.Width, testImage.Height);
                List<EdgeSegment> dummySegments = new List<EdgeSegment>();

                foreach (EdgeSegment segment in testEdgeMap.Segments)
                {
                    List<Pixel[]> pixels = Utils.SplitEdgeSegmentByValues(segment, len);

                    foreach (Pixel[] pixel in pixels)
                    {
                        EdgeSegment dummyEdgeSegment = new EdgeSegment(pixel.Length);
                        dummyEdgeSegment.Pixels = pixel;

                        dummySegments.Add(dummyEdgeSegment);
                    }
                }

                dummyEdgeMap.NoSegments = dummySegments.Count;
                dummyEdgeMap.Segments = dummySegments.ToArray();

                Utils.Invert(dummyEdgeMap.ToBitmap()).Save(PATH_BEFORE_ELIMINATION, ImageFormat.Bmp);

                // Result Image
                List<EdgeSegment> resultList = new List<EdgeSegment>();

                foreach (EdgeSegment segment in dummyEdgeMap.Segments)
                {
                    Pixel[] pixels = segment.Pixels;

                    double[] v = ChainCode.Extract(pixels);

                    Descriptor d = new Descriptor(1, v);

                    double result = classification.Test(d);

                    if (result > 0) resultList.Add(segment);
                }

                EdgeMap resultMap = new EdgeMap(testImage.Width, testImage.Height);
                resultMap.NoSegments = resultList.Count;
                resultMap.Segments = resultList.ToArray();

                Utils.Invert(resultMap.ToBitmap()).Save(PATH_TEMPORAY_TEST_RESULT_ROOT + Path.DirectorySeparatorChar + Path.GetFileName(path), ImageFormat.Bmp);
            }

        }
    }
}

﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace csharp_image_segmentation_app.Helpers
{
    public class OtsuBinarization
    {
         public HashSet<Point> Set;

        public Bitmap ThresholdByOtsu(Bitmap src)
        {
            Set = new HashSet<Point>();

            var dst = (Bitmap)src.Clone();
            
            Grayscale(ref dst);
            
            int otsuThreshold = GetOtsuThreshold(dst) * 3;
            
            Threshold(ref dst, (short)otsuThreshold);
            
            return dst;
        }

        private void Grayscale(ref Bitmap bmp)
        {
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    *ptr = (byte)((ptr[2] * .299) + (ptr[1] * .587) + (ptr[0] * .114));
                    ptr[1] = *ptr;
                    ptr[2] = *ptr;

                    ptr += 3;
                }
            }

            bmp.UnlockBits(bmpData);
        }

        private void Threshold(ref Bitmap bmp, short thresholdValue)
        {
            int MaxVal = 768;

            if (thresholdValue < 0) return;
            else if (thresholdValue > MaxVal) return;

            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                int TotalRGB;

                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                int width = bmp.Width;
                int ind = 0;

                while ((int)ptr != stopAddress)
                {
                    TotalRGB = ptr[0] + ptr[1] + ptr[2];

                    int y = ind % width;
                    int x = ind - y * width;

                    if (TotalRGB <= thresholdValue)
                    {
                        ptr[2] = 255;
                        ptr[1] = 255;
                        ptr[0] = 255;

                        Set.Add(new Point(x, y));
                    }
                    else
                    {
                        ptr[2] = 0;
                        ptr[1] = 0;
                        ptr[0] = 0;
                    }
                    ind++;
                    ptr += 3;
                }
            }

            bmp.UnlockBits(bmpData);
        }

        private float Px(int init, int end, int[] hist)
        {
            int sum = 0;
            int i;

            for (i = init; i <= end; i++)
                sum += hist[i];

            return (float)sum;
        }

        private float Mx(int init, int end, int[] hist)
        {
            int sum = 0;
            int i;

            for (i = init; i <= end; i++)
                sum += i * hist[i];

            return (float)sum;
        }

        private int FindMax(float[] vec, int n)
        {
            float maxVec = 0;
            int idx = 0;
            int i;

            for (i = 1; i < n - 1; i++)
            {
                if (vec[i] > maxVec)
                {
                    maxVec = vec[i];
                    idx = i;
                }
            }

            return idx;
        }

        private int GetOtsuThreshold(Bitmap src)
        {
            byte t = 0;
            float[] vet = new float[256];
            int[] hist = new int[256];
            vet.Initialize();

            float p1, p2, p12;
            int k;

            BitmapData bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                byte* p = (byte*)(void*)bmData.Scan0.ToPointer();

                GetHistogram(p, src.Width, src.Height, bmData.Stride, hist);

                for (k = 1; k != 255; k++)
                {
                    p1 = Px(0, k, hist);
                    p2 = Px(k + 1, 255, hist);
                    p12 = p1 * p2;
                    if (p12 == 0)
                        p12 = 1;
                    float diff = (Mx(0, k, hist) * p2) - (Mx(k + 1, 255, hist) * p1);
                    vet[k] = (float)diff * diff / p12;

                }
            }
            src.UnlockBits(bmData);

            t = (byte)FindMax(vet, 256);

            return t;
        }

        unsafe private void GetHistogram(byte* p, int w, int h, int ws, int[] hist)
        {
            hist.Initialize();

            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w * 3; j += 3)
                {
                    int index = i * ws + j;
                    hist[p[index]]++;
                }
            }
        } 
        
        public void Dispose()
        {
            Set.Clear();
        }     
    }
}

﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace csharp_image_segmentation_app.Sources.EdgeDetection
{
    public class GradientMap
    {
        public double[,] Gx { set; get; }
        public double[,] Gy { set; get; }
        public double[,] G { set; get; }        
        public double[,] Angles { set; get; }

        private int width, height;

        public enum Types { Gx = 0, Gy = 1, G = 2, Gdir = 3, }

        public GradientMap(int width, int height)
        {
            this.width = width;
            this.height = height;

            Gx = new double[width, height];
            Gy = new double[width, height];
            G = new double[width, height];
            Angles = new double[width, height];
        }

        public void Evaluate(){

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    G[i, j] = Math.Sqrt(Gx[i, j] * Gx[i, j] + Gy[i, j] * Gy[i, j]);
                    Angles[i, j] = Math.Atan2(Gy[i, j], Gx[i, j]) * (180 / Math.PI);

                    if (Angles[i, j] < 0) Angles[i, j] += 180;
                }
            }


        }

        public Bitmap ToBitmap(Types type)
        {
            switch(type){
                case Types.G:
                    return ConvertToBitmap(G);                    
                case Types.Gdir:                        
                    return ConvertToBitmap(Angles);                    
                case Types.Gx:
                    return ConvertToBitmap(Gx);                    
                case Types.Gy:
                    return ConvertToBitmap(Gy);                    
                default:
                    throw new Exception("Not avaible type for converting to bitmap!");                    
            }            
        }

        public Bitmap ConvertToBitmap(double[,] src)
        {
            Bitmap dst = new Bitmap(width, height);

            BitmapData bmpData = dst.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            double max = 0;

            // Normalized [0, 255]
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (src[i, j] > max)
                    {
                        max = src[i, j];
                    }
                }                
            }

            byte[,] temp = new byte[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    double d = src[i, j];

                    temp[i, j] = Convert.ToByte(Math.Floor(src[i, j] * 255 / max));
                }
            }

            unsafe
            {
                int ind = 0;

                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    int x = ind % width;
                    int y = ind / width;

                    *ptr = temp[x, y];
                    ptr[1] = *ptr;
                    ptr[2] = *ptr;

                    ptr += 3;
                    ind++;
                }
            }

            dst.UnlockBits(bmpData);

            return dst;
        }    
    }
}

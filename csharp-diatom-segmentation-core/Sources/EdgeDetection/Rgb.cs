﻿using System;

namespace csharp_image_segmentation_app
{
    [Serializable]
    public struct Rgb<T>
    {
        public T R, G, B;

        public Rgb(T r, T g, T b)
        {
            R = r;
            G = g;
            B = b;
        }

        public void SetRgb(T r, T g, T b)
        {
            R = r;
            G = g;
            B = b;
        }
    }
}

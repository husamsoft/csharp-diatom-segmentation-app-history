﻿namespace csharp_image_segmentation_app.Sources.EdgeDetection.Elsd
{
    public class Rect
    {
        /* first and second point of the line segment */
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }
        
        /* rectangle width */
        public double Width { get; set; }

        /* center of the rectangle */
        public double Cx { get; set; }
        public double Cy { get; set; }

        /* angle */
        public double Theta;

        /* (dx,dy) is vector oriented as the line segment */
        public double Dx { get; set; }
        public double Dy { get; set; }

        /* tolerance angle */
        public double Prec;

        /* probability of a point with angle within 'prec' */
        public double P;

        public double Length;          
    }
}

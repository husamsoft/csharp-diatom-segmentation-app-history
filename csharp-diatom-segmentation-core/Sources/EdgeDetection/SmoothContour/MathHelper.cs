﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Sources.EdgeDetection.SmoothContour
{
    public class MathHelper
    {
        /* Euclidean distance between x1,y1 and x2,y2 */
        public static double Dist(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        }
    }
}

﻿namespace csharp_image_segmentation_app.Sources.EdgeDetection.SmoothContour
{
    public class Arc
    {
        public double Xc { get; set; }
        public double Yc { get; set; }
        public double Radius { get; set; }

        public Arc(double xc, double yc, double radius)
        {
            this.Xc = xc;
            this.Yc = yc;
            this.Radius = radius;
        }
    }
}

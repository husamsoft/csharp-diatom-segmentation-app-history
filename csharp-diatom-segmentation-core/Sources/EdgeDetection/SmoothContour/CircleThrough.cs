﻿using csharp_image_segmentation_app.Sources.EdgeDetection.SmoothContour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Sources.EdgeDetection.Elsd
{
    public unsafe class CircleThrough
    {
        private static double M_PI = 3.14159265358979323846;

        private static RawImage image;

        public Arc LineSegmentDetection( double x1, double y1, double x2, double y2, double x3, double y3 )
        {

            double h, k, den, xxyy1, xxyy2, xxyy3;        

            den = x1 * y2 + y1 * x3 + x2 * y3 - x3 * y2 - x2 * y1 - x1 * y3; /* denominator */
            if (den == 0.0) Console.WriteLine("the 3 points are aligned");

            xxyy1 = x1 * x1 + y1 * y1;
            xxyy2 = x2 * x2 + y2 * y2;
            xxyy3 = x3 * x3 + y3 * y3;

            h = xxyy1 * y2 + xxyy3 * y1 + xxyy2 * y3 - xxyy3 * y2 - xxyy2 * y1 - xxyy1 * y3;
            h /= 2.0 * den;

            k = x1 * xxyy2 + x3 * xxyy1 + x2 * xxyy3 - x3 * xxyy2 - x2 * xxyy1 - x1 * xxyy3;
            k /= 2.0 * den;

            return new Arc(h, k, MathHelper.Dist(x1, y1, h, k));

        }

            
        

    }
}

﻿using System.Drawing;
using System.Drawing.Imaging;

namespace csharp_image_segmentation_app.Sources.Viewing
{
    public class EdgeImage
    {
        private static readonly Rgb<byte>[] Colors = { new Rgb<byte>(255, 0, 0), new Rgb<byte>(0, 255, 0), new Rgb<byte>(0, 0, 255)};

        private static readonly byte EdgeValue = 255;

        private int width, height, length;

        private Bitmap rawImage;

        private EdgeMap edgeMap;

        public Rgb<byte>[] ColoredEdge { get; private set; }

        public byte[] BinaryEdge { get; private set; }

        public Bitmap BitmapColor { get; private set; }

        public EdgeImage(Bitmap rawImage, EdgeMap edgeMap)
        {
            this.rawImage = rawImage;
            this.edgeMap = edgeMap;
            this.width = rawImage.Width;
            this.height = rawImage.Height;
            this.length = width * height;

            Initilize();

            OverlayEdgeOnImage();
        }

        public void Initilize()
        {
            ColoredEdge = new Rgb<byte>[length];
            BinaryEdge = new byte[length];

            lock (this)
            {
                for (int i = 0; i < edgeMap.NoSegments; i++)
                {
                    for (int k = 0; k < edgeMap.Segments[i].NoPixels; k++)
                    {
                        byte b = Colors[i % Colors.Length].B;
                        byte g = Colors[i % Colors.Length].G;
                        byte r = Colors[i % Colors.Length].R;

                        int h = edgeMap.Segments[i].Pixels[k].R * width + edgeMap.Segments[i].Pixels[k].C;

                        ColoredEdge[h] = new Rgb<byte>(r, g, b);
                        BinaryEdge[h] = EdgeValue;
                    }
                }
            }
        }

        public unsafe void OverlayEdgeOnImage()
        {
            BitmapColor = (Bitmap)rawImage.Clone();

            var tmpData = BitmapColor.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            
            var scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < length; i++)
            {
                var rgb = ColoredEdge[i];

                if (rgb.R != 0 || rgb.G != 0 || rgb.B != 0)
                {
                    scan0[i * 3] = rgb.B;
                    scan0[i * 3 + 1] = rgb.G;
                    scan0[i * 3 + 2] = rgb.R;
                }
            }

            BitmapColor.UnlockBits(tmpData);
        }
    }
}

﻿using System;

namespace csharp_image_segmentation_app.Sources.EdgeDetection
{
    public enum GradientOperator { PREWITT_OPERATOR = 101, SOBEL_OPERATOR = 102, SCHARR_OPERATOR = 103 };

    public class GradientOperators
    {
        private const int EDGE_VERTICAL = 1;
        private const int EDGE_HORIZONTAL= 2;
        private const int EDGE_45 = 3;
        private const int EDGE_135 = 4;

        public byte[] LocalMax { get; private set; }

        public double[] ArrGradient { get; private set; }

        //L2-Norm 5x5 Sobel
        public void Sobel5By5(Rgb<double>[] src, int w, int h)
        {
            ArrGradient = new double[w * h];

            double[][] kernel = new double[5][] { 
                new double[] { 2, 1, 0, -1, -2 }, 
                new double[] { 3, 2, 0, -2, -3 }, 
                new double[] { 4, 3, 0, -3, -4 }, 
                new double[] { 3, 2, 0, -2, -3 }, 
                new double[] { 2, 1, 0, -1, -2 } };

            //convolve the image
            for (int j = 3; j < h - 3; j++)
            {
                for (int i = 3; i < w - 3; i++)
                {
                    //Vertical Sobel
                    double Gx_R = 0, Gx_G = 0, Gx_B = 0; //RGB

                    for (int k = -2; k <= 2; k++)
                    {
                        for (int m = -2; m <= 2; m++)
                        {
                            Gx_R += src[(j + k) * w + i + m].R * kernel[k + 2][m + 2];
                            Gx_G += src[(j + k) * w + i + m].G * kernel[k + 2][m + 2];
                            Gx_B += src[(j + k) * w + i + m].B * kernel[k + 2][m + 2];
                        }
                    }

                    //Horizontal Sobel
                    double Gy_R = 0, Gy_G = 0, Gy_B = 0; //RGB

                    for (int k = -2; k <= 2; k++)
                    {
                        for (int m = -2; m <= 2; m++)
                        {
                            //swap k and m to rotate kernel 90 degrees
                            Gy_R += src[(j + k) * w + i + m].R * kernel[m + 2][k + 2];
                            Gy_G += src[(j + k) * w + i + m].G * kernel[m + 2][k + 2];
                            Gy_B += src[(j + k) * w + i + m].B * kernel[m + 2][k + 2];
                        }
                    }

                    // L2-Norm: {|G1|^2 + |G2|^2 + |G3|^2}^0.5
                    double Gx = Math.Sqrt(Gx_R * Gx_R + Gx_G * Gx_G + Gx_B * Gx_B);
                    double Gy = Math.Sqrt(Gy_R * Gy_R + Gy_G * Gy_G + Gy_B * Gy_B);

                    ArrGradient[j * w + i] = Math.Sqrt(Gx * Gx + Gy * Gy);
                }
            }

            //scale gradients 0 to 255
            double max = -1;
            for (int i = 0; i < w * h; i++) if (ArrGradient[i] > max) max = ArrGradient[i];

            for (int i = 0; i < w * h; i++) ArrGradient[i] = 255 * ArrGradient[i] / max;

            //Set local maximum matrix
            LocalMax = new byte[w * h];
            for (int j = 1; j < h - 1; j++)
            {
                for (int i = 1; i < w - 1; i++)
                {
                    int index = j * w + i;

                    if (ArrGradient[index] > ArrGradient[index - 1] && ArrGradient[index] > ArrGradient[index + 1])
                        LocalMax[index] = 1;

                    if (ArrGradient[index] > ArrGradient[index - w] && ArrGradient[index] > ArrGradient[index + w])
                        LocalMax[index] = 1;
                }
            }
        }

        ///-----------------------------------------------------------------------
        /// Scaled Prewitt Operator: Scale the gradient values to [0-255]
        ///      
        public void ComputeGradientMapByPrewitt(byte[] src, int width, int height)
        {
            ArrGradient = new double[width * height];

            // Compute the gradient image and edge directions for the rest of the pixels
            int max = 0;
            for (int i = 1; i < height - 1; i++)
            {
                for (int j = 1; j < width - 1; j++)
                {
                    // Prewitt Operator in horizontal and vertical direction
                    // A B C
                    // D x E
                    // F G H
                    // gx = (C-A) + (E-D) + (H-F)
                    // gy = (F-A) + (G-B) + (H-C)
                    //
                    // To make this faster: 
                    // com1 = (H-A)
                    // com2 = (C-F)
                    // Then: gx = com1 + com2 + (E-D) = (H-A) + (C-F) + (E-D) = (C-A) + (E-D) + (H-F)
                    //       gy = com1 - com2 + (G-B) = (H-A) - (C-F) + (G-B) = (F-A) + (G-B) + (H-C)
                    // 
                    int com1 = src[(i + 1) * width + j + 1] - src[(i - 1) * width + j - 1];
                    int com2 = src[(i - 1) * width + j + 1] - src[(i + 1) * width + j - 1];

                    int gx = Math.Abs(com1 + com2 + (src[i * width + j + 1] - src[i * width + j - 1]));
                    int gy = Math.Abs(com1 - com2 + (src[(i + 1) * width + j] - src[(i - 1) * width + j]));

                    int sum = gx + gy;

                    int index = i * width + j;
                    ArrGradient[index] = sum;

                    /*
                    if (gx >= gy)
                        ArrGradient[index] = EDGE_VERTICAL;
                    else
                        ArrGradient[index] = EDGE_HORIZONTAL;
                    */
                    if (sum > max) max = sum;
                    
                } // end-for          
            } // end-for

            // Scale the gradient
            double scale = 255.0 / max;
            for (int i = 0; i < width * height; i++) ArrGradient[i] = (short)(scale * ArrGradient[i]);

            //Set local maximum matrix
            LocalMax = new byte[width * height];
            for (int j = 1; j < height - 1; j++)
            {
                for (int i = 1; i < width - 1; i++)
                {
                    int index = j * width + i;

                    if (ArrGradient[index] > ArrGradient[index - 1] && ArrGradient[index] > ArrGradient[index + 1])
                        LocalMax[index] = 1;

                    if (ArrGradient[index] > ArrGradient[index - width] && ArrGradient[index] > ArrGradient[index + width])
                        LocalMax[index] = 1;
                }
            }

        } //end-ComputeGradientMapByPrewitt

        ///-----------------------------------------------------------------------
        /// Scaled Prewitt Operator: Scale the gradient values to [0-255]
        ///      
        public GradientMap ComputeGradientMapByPrewitt2(byte[] src, int width, int height)
        {
            var gm = new GradientMap(width, height);

            // Compute the gradient image and edge directions for the rest of the pixels
            int max = 0;
            for (int i = 1; i < height - 1; i++)
            {
                for (int j = 1; j < width - 1; j++)
                {
                    // Prewitt Operator in horizontal and vertical direction
                    // A B C
                    // D x E
                    // F G H
                    // gx = (C-A) + (E-D) + (H-F)
                    // gy = (F-A) + (G-B) + (H-C)
                    //
                    // To make this faster: 
                    // com1 = (H-A)
                    // com2 = (C-F)
                    // Then: gx = com1 + com2 + (E-D) = (H-A) + (C-F) + (E-D) = (C-A) + (E-D) + (H-F)
                    //       gy = com1 - com2 + (G-B) = (H-A) - (C-F) + (G-B) = (F-A) + (G-B) + (H-C)
                    // 
                    int com1 = src[(i + 1) * width + j + 1] - src[(i - 1) * width + j - 1];
                    int com2 = src[(i - 1) * width + j + 1] - src[(i + 1) * width + j - 1];

                    //int gx = Math.Abs(com1 + com2 + (src[i * width + j + 1] - src[i * width + j - 1]));
                    //int gy = Math.Abs(com1 - com2 + (src[(i + 1) * width + j] - src[(i - 1) * width + j]));
                    int gx = com1 + com2 + (src[i * width + j + 1] - src[i * width + j - 1]);
                    int gy = com1 - com2 + (src[(i + 1) * width + j] - src[(i - 1) * width + j]);

                    gm.Gx[j, i] = gx;
                    gm.Gy[j, i] = gy;

                } // end-for          
            } // end-for            

            return gm;

        } //end-ComputeGradientMapByPrewitt
    }
}

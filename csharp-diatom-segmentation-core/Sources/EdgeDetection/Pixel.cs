﻿using System;

namespace csharp_image_segmentation_app
{
    [Serializable]
    public class Pixel
    {
        public int R { get; set; }
        public int C { get; set; }

        public Pixel(int r, int c)
        {
            R = r;
            C = c;
        }
    }
}

﻿
using csharp_image_segmentation_app.Sources.EdgeDetection;
using System;
using System.Drawing;

namespace csharp_image_segmentation_app.Sources.Viewing
{
    public class ViewImage
    {
        public Bitmap RawImage { get; private set; }

        public EdgeMap EdgeMap { get; private set; }

        public EdgeImage EdgeImage { get; private set; }

        public GradientOperators GradientOperators { get; private set; }
 
        public byte[] GrayByteImage { get; private set; }

        public Rgb<double>[] RgbDoubleImage { get;  private set; }

        public ViewImage(Bitmap rawImage, GradientOperator op, int threshold)
        {
            RawImage = rawImage;

            Intialize(op, threshold);
        }

        public void Intialize(GradientOperator op, int threshold)
        {
            EdgeMap = new EdgeMap(RawImage, threshold);

            EdgeImage = new EdgeImage(RawImage, EdgeMap);

            GrayByteImage = Utils.BitmapToGray1DbyteArray(RawImage);

            RgbDoubleImage = Utils.BitmapToRgb(RawImage);

            GradientOperators = new GradientOperators();

            EvaluateGradient(op);
        }

        public void Intialize(EdgeMap edgeMap, GradientOperator op, int threshold)
        {
            EdgeMap = edgeMap;

            EdgeImage = new EdgeImage(RawImage, EdgeMap);

            GrayByteImage = Utils.BitmapToGray1DbyteArray(RawImage);

            RgbDoubleImage = Utils.BitmapToRgb(RawImage);

            EvaluateGradient(op);
        }

        private void EvaluateGradient(GradientOperator op)
        {
            switch (op)
            {
                case GradientOperator.PREWITT_OPERATOR:
                    GradientOperators.ComputeGradientMapByPrewitt(GrayByteImage, RawImage.Width, RawImage.Height);
                    break;
                default:
                    throw new Exception("Unknown Gradient Operator!");
            }
        }
    }
}

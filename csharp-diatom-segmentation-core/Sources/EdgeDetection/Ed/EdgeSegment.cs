﻿using System;

namespace csharp_image_segmentation_app
{
    [Serializable]
    public class EdgeSegment : IComparable
    {
        public Pixel[] Pixels { get;  set; }
        public int NoPixels { get; set; }

        public EdgeSegment(int noPixels)
        {
            NoPixels = noPixels;
            Pixels = new Pixel[noPixels];
        }

        public int CompareTo(Object obj)
        {
            if (obj == null) return 1;

            EdgeSegment other = obj as EdgeSegment;
            if ( other!= null)
                return this.NoPixels.CompareTo(other.NoPixels);
            else
                throw new ArgumentException("Object is not a Temperature");
        }
    }
}

﻿using csharp_image_segmentation_app.Sources.Helpers;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace csharp_image_segmentation_app
{
    [Serializable]
    public unsafe class EdgeMap
    {
        private int width, height;

        public EdgeSegment[] Segments { get; set; }
        public int NoSegments { get; set; }

        public EdgeMap(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public EdgeMap(Bitmap bmp, int threshold)
        {            
            this.width = bmp.Width;
            this.height = bmp.Height;

            var edgeMap = EdHelper.ToEdgeMap(bmp, threshold);

            Segments = edgeMap.Segments;
            NoSegments = edgeMap.NoSegments;
        }
        
        public byte* ToPointerEdgeImage()
        {
            byte[] image = new byte[width * height];

            for (int i = 0; i < NoSegments; i++)
            {
                for (int k = 0; k < Segments[i].NoPixels; k++)
                {
                    int h = Segments[i].Pixels[k].R * width + Segments[i].Pixels[k].C;

                    image[h] = 255;
                }
            }
            byte* p = (byte*)Marshal.AllocHGlobal(width * height * sizeof(byte));

            for (int i = 0; i < width * height; i++)
            {
                p[i] = image[i];
            }

            return p;
        }   
        
        public Bitmap ToBitmap()
        {
            Bitmap tmpBmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            int len = width * height;
            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < NoSegments; i++)
            {
                for (int k = 0; k < Segments[i].NoPixels; k++)
                {
                    int h = Segments[i].Pixels[k].R * width + Segments[i].Pixels[k].C;

                    scan0[h * 3] = scan0[h * 3 + 1] = scan0[h * 3 + 2] = 255;
                }
            }
            
            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }
        

        public Bitmap ToColorBitmap()
        {
            Bitmap tmpBmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            Rgb<Byte>[] colors = new Rgb<byte>[50];

            for(int i = 0; i < colors.Length; i++)
            {
                colors[i].R = Convert.ToByte(i * i * 5 % 200 + 55);
                colors[i].G = Convert.ToByte(i * i * 20 % 200 + 55);
                colors[i].B = Convert.ToByte(i * i * 35 % 200 + 55);
            }
            

            int length = width * height;

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < NoSegments; i++)
            {
                //if (Segments[i].NoPixels > 50)
                {
                    for (int k = 0; k < Segments[i].NoPixels; k++)
                    {
                        int h = Segments[i].Pixels[k].R * width + Segments[i].Pixels[k].C;

                        byte red = Convert.ToByte(NoSegments * Segments[i].NoPixels % 255);
                        byte green = Convert.ToByte(NoSegments * Segments[i].NoPixels % 255);
                        byte blue = Convert.ToByte(NoSegments * Segments[i].NoPixels % 255);

                        scan0[h * 3] = colors[i % colors.Length].B;        // Blue
                        scan0[h * 3 + 1] = colors[i % colors.Length].G;    // Gree
                        scan0[h * 3 + 2] = colors[i % colors.Length].R;    // Red
                    }
                }
            }

            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }

        public Bitmap ToLongEdgeSegmentBitmap()
        {
            Bitmap tmpBmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            int length = width * height;

            byte* scan0 = (byte*)tmpData.Scan0;

            int max = 0, segmentId = 0;

            for (int i = 0; i < NoSegments; i++)
            {
                for (int k = 0; k < Segments[i].NoPixels; k++)
                {
                    if(Segments[i].NoPixels >= max)
                    {
                        max = Segments[i].NoPixels;
                        segmentId = i;
                    }
                }
            }

            Console.WriteLine(segmentId + " " + max);

            //for (int i = 0; i < NoSegments; i++)
            {
                for (int k = 0; k < Segments[segmentId].NoPixels; k++)
                {
                    int h = Segments[segmentId].Pixels[k].R * width + Segments[segmentId].Pixels[k].C;

                    scan0[h * 3] = scan0[h * 3 + 1] = scan0[h * 3 + 2] = 255;
                }
            }

            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }
    }
}

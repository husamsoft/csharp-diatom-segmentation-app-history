﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Sources.Classification
{
    class CrossValitationBenchmark
    {

        /*
        Dictionary<ClassLabel, Template>  templates = ReadFromBinaryFile<Dictionary<ClassLabel, Template>>(@"C:\Users\admin\Desktop\templates\" + "temp.txt");

        #region split code, TODO: move another class later
        Dictionary<ClassLabel, List<Pixel[]>> arcs = new Dictionary<ClassLabel, List<Pixel[]>>();                        
        int arclen = 100, step = 50;
        foreach(Template template in templates.Values)
        {
            //template.EdgeMap.ToBitmap().Save(@"C:\Users\admin\Desktop\templates\junk\" + template.ClassLabel.Name + ".bmp", ImageFormat.Bmp);

            if (template.EdgeMap.NoSegments > 1)
            {
                Console.WriteLine("Segment sayisi birden fazla olamaz!");
            }

            List<Pixel[]> pixels = new List<Pixel[]>();

            arcs.Add(template.ClassLabel, pixels);

            for(int i = 0; i < template.EdgeMap.Segments[0].NoPixels - arclen; i+=step)
            {
                Pixel[] p = new Pixel[arclen];

                for (int j = 0; j < arclen ; j++)
                {
                    p[j] = template.EdgeMap.Segments[0].Pixels[j + i];
                }

                arcs[template.ClassLabel].Add(p);                    
            }

        }
        #endregion

        int cvFold = 5;
        CrossValidationBuilder cvb = new CrossValidationBuilder(cvFold);

        foreach (ClassLabel label in arcs.Keys)
        {
            //Console.WriteLine(label.Name + " " + arcs[label].Count);

            foreach (Pixel[] pixels in arcs[label])
            {
                double[] x = new double[pixels.Length];
                double[] y = new double[pixels.Length];

                for (int i = 0;i < pixels.Length; i++)
                {
                    x[i] = pixels[i].C;
                    y[i] = pixels[i].R;
                }

                // Elliptic fourier transform
                EllipticFD fd = new EllipticFD(x, y);

                double[] v = new double[48];
                for (int i = 2; i < fd.efd.Length; i++)
                {
                    v[i - 2] = fd.efd[i];
                }

                Descriptor d = new Descriptor(label.Id, v);


                // Add cross validatin builder
                cvb.Add(d);
            }                
        }

        cvb.CreateLabels();


        for (int i = 1; i <= cvFold; i++)
        {
            MultiClassSvm svm = new MultiClassSvm();

            cvb.BuildData(i);

            svm.Train(cvb.TrainData);
            svm.SaveModel();
            svm.Test(cvb.TestData);
        }
         */

    }
}

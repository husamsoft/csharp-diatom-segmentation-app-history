﻿using csharp_image_segmentation_app.Model;
using LibSVMsharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Sources.Classification
{
    public class ModelHandler
    {

        /// <summary>
        /// <int> as Class Label of +1, other label are -1
        /// <SVMModel> as model of one ageinst all strategies
        /// </summary>
        public Dictionary<int, SVMModel> LookUpModels { get; private set; }

        /// <summary>
        /// <int></int> as label of one
        /// <List<int>> as others
        /// </summary>
        public Dictionary<int, List<int>> LookUpLabels { get; private set; }

        public ModelHandler()
        {
            LookUpModels = new Dictionary<int, SVMModel>();
            LookUpLabels = new Dictionary<int, List<int>>();
        }

        public void Load()
        {
            LookUpLabels.Clear();
            LookUpModels.Clear();

            string[] lines = File.ReadAllLines(GlobalParams.MODEL_ROOT_PATH + Path.DirectorySeparatorChar + GlobalParams.MODEL_INFO_NAME);

            foreach (string line in lines)
            {
                string[] sp1 = line.Split(' ');
                int one = Convert.ToInt32(sp1[0]);

                string[] sp2 = sp1[1].Split(':');
                List<int> others = new List<int>();
                foreach (string s in sp2)
                {
                    others.Add(Convert.ToInt32(s));
                }

                LookUpLabels.Add(one, others);
                LookUpModels.Add(one, SVM.LoadModel(GlobalParams.MODEL_ROOT_PATH + Path.DirectorySeparatorChar + GlobalParams.MODEL_NAME + one));
            }
        }

        public void Save()
        {
            foreach (KeyValuePair<int, SVMModel> item in LookUpModels)
            {
                SVM.SaveModel(item.Value, GlobalParams.MODEL_ROOT_PATH + Path.DirectorySeparatorChar + GlobalParams.MODEL_NAME + item.Key);
            }

            StringBuilder builder = new StringBuilder();

            foreach (KeyValuePair<int, List<int>> item in LookUpLabels)
            {
                builder.Append(item.Key);
                builder.Append(" ");
                int index = 0;
                foreach (int l in item.Value)
                {
                    builder.Append(l);
                    if (index != item.Value.Count - 1) builder.Append(":");
                    index++;
                }
                builder.Append("\n");
            }

            File.WriteAllText(GlobalParams.MODEL_ROOT_PATH + Path.DirectorySeparatorChar + GlobalParams.MODEL_INFO_NAME, builder.ToString());
        }

        public void Add(int labelOne, List<int> labelOthers, SVMModel model)
        {
            LookUpModels.Add(labelOne, model);
            LookUpLabels.Add(labelOne, labelOthers);
        }
    }
}

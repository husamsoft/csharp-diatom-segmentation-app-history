﻿using LibSVMsharp;
using System.Collections.Generic;

namespace csharp_image_segmentation_app.Sources.Classification
{
    public class SvmClassification
    {
        private SVMModel model;

        public SvmClassification()
        {

        }

        public void Train(List<Descriptor> descriptors)
        {
            // Classification
            SVMProblem problem = new SVMProblem();

            SVMParameter param = new SVMParameter();
            param.Type = SVMType.C_SVC;
            param.Kernel = SVMKernelType.POLY;
            param.C = 1;
            param.Degree = 5;

            foreach (Descriptor d in descriptors)
            {
                double[] values = d.Vector;
                SVMNode[] nodes = new SVMNode[values.Length];

                for (int i = 1; i <= values.Length; i++)
                {
                    SVMNode node = new SVMNode();
                    node.Index = i;
                    node.Value = values[i - 1];

                    nodes[i - 1] = node;
                }
                int classId = d.Label;

                if (classId != 1) classId = -1;

                problem.Add(nodes, classId);
            }

            model = SVM.Train(problem, param);
        }

        public double Test(Descriptor descriptor)
        {          
            double[] values = descriptor.Vector;
            SVMNode[] nodes = new SVMNode[values.Length];

            for (int i = 1; i <= values.Length; i++)
            {
                SVMNode node = new SVMNode();
                node.Index = i;
                node.Value = values[i - 1];

                nodes[i - 1] = node;
            }

            return SVM.Predict(model, nodes);            
        }
    }
}

﻿namespace csharp_image_segmentation_app.Sources.Classification
{
    public class Descriptor
    {
        public int Label { get; set; }
        public double[] Vector { get; set; }

        public Descriptor()
        {

        }
        public Descriptor(int label, double[] vector)
        {
            this.Label = label;
            this.Vector = vector;
        }
    }
}

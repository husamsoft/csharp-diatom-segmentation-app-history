﻿using System;
using System.Collections.Generic;

namespace csharp_image_segmentation_app.Sources.Classification
{
    public class CrossValidationBuilder
    {
        // class id, descriptor
        private Dictionary<int, List<Descriptor>> data;
        
        // class id, descriptor
        public  List<Descriptor> TestData; 

        // class id, descriptor
        public List<Descriptor> TrainData; 

        // class id, descriptor
        private Dictionary<int, List<int>> cvlabelsLookup; 

        private int fold;

        public CrossValidationBuilder(int fold)
        {
            this.fold = fold;

            data = new Dictionary<int, List<Descriptor>>();
            TestData = new List<Descriptor>();
            TrainData = new List<Descriptor>();

            cvlabelsLookup = new Dictionary<int, List<int>>();
        }

        public void CreateLabels()
        {            
            foreach(int classid in data.Keys)
            {
                List<Descriptor> descriptors = data[classid];

                int division = descriptors.Count / fold;
                int remain = descriptors.Count - division * fold;                

                List<int> cvlabels = new List<int>();
                
                // division                    
                for(int i = 1; i <= fold; i++)
                {
                    for(int j = 0; j < division; j++)
                    {
                        cvlabels.Add(i);
                    }                                                                 
                }
                // remain
                for(int i = 0; i < remain; i++)
                {
                    cvlabels.Add(fold);
                }

                /*
                for(int i = 0; i < cvlabels.Count; i++)
                {
                    Console.Write(cvlabels[i] + " ");
                }

                Console.WriteLine();
                */

                if (cvlabelsLookup.ContainsKey(classid))
                {
                    throw new Exception("Impossible case, in cv fold");
                }

                cvlabelsLookup.Add(classid, cvlabels);            
            }   
            
           
        }

        public void BuildData(int foldNo)
        {
            foreach (int classid in cvlabelsLookup.Keys)
            {
                int id = 0;
                foreach (int fold in cvlabelsLookup[classid])
                {                    
                    if (fold == foldNo)
                    {
                        TestData.Add(data[classid][id]);               
                    }
                    else
                    {
                        TrainData.Add(data[classid][id]);             
                    }
                    id++;
                }                
            }
        }


        public void Add(Descriptor d)
        {
            if (!data.ContainsKey(d.Label))
            {
                List<Descriptor> descriptors = new List<Descriptor>();
                descriptors.Add(d);
                data.Add(d.Label, descriptors);
            }
            else
            {
                data[d.Label].Add(d);
            }

        }
    }
}

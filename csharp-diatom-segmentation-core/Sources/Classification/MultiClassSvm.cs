﻿using csharp_image_segmentation_app.Model;
using LibSVMsharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace csharp_image_segmentation_app.Sources.Classification
{
    public class MultiClassSvm
    {
        private SortedDictionary<int, List<SVMNode[]>> LookUp;

        private Dictionary<int, int> LookUpLabel;

        private ModelHandler modelHandler;

        private SVMParameter param;

        public MultiClassSvm()
        {
            LookUp = new SortedDictionary<int, List<SVMNode[]>>();

            LookUpLabel = new Dictionary<int, int>();

            modelHandler = new ModelHandler();

            param = new SVMParameter();
            param.Type = SVMType.C_SVC;
            param.Kernel = SVMKernelType.POLY;
            param.C = 1;
            param.Degree = 5;

            //param.Probability = true;            
        }

        public void Train(List<Descriptor> descriptors)
        {

            int ind = 0;

            foreach (Descriptor d in descriptors)
            {
                double[] values = d.Vector;

                SVMNode[] nodes = new SVMNode[values.Length];

                for (int i = 0; i < values.Length; i++)
                {
                    nodes[i] = new SVMNode(i + 1, values[i]);
                }
                int classLabel = d.Label;

                if (!LookUp.ContainsKey(classLabel))
                {
                    LookUp.Add(classLabel, new List<SVMNode[]>());
                }

                LookUp[classLabel].Add(nodes);

                if (!LookUpLabel.ContainsKey(d.Label))
                {
                    LookUpLabel.Add(d.Label, ind);
                    ind++;
                }
            }
        }

        public void SaveModel()
        {
            List<int> allLabels = new List<int>();

            allLabels.AddRange(LookUp.Keys);

            foreach (int one in LookUp.Keys)
            {
                SVMProblem problem = new SVMProblem();

                List<int> others = new List<int>();

                foreach (int other in allLabels)
                {
                    int classLabel = -1;

                    if (one == other) { classLabel = 1; } else { others.Add(other); }

                    foreach (SVMNode[] nodes in LookUp[other])
                    {
                        problem.Add(nodes, classLabel);
                    }
                }

                SVMModel model = SVM.Train(problem, param);

                modelHandler.Add(one, others, model);
            }

            modelHandler.Save();
        }


        public void Test(List<Descriptor> descriptors)
        {
            modelHandler.Load();

            int[,] confusionMatrix = new int[modelHandler.LookUpLabels.Count, modelHandler.LookUpLabels.Count];

            foreach (Descriptor d in descriptors)
            {
                double[] values = d.Vector;
                SVMNode[] nodes = new SVMNode[values.Length];

                for (int i = 0; i < values.Length; i++)
                {
                    nodes[i] = new SVMNode(i + 1, values[i]);
                }

                int classLabel = d.Label;

                foreach (var model in modelHandler.LookUpModels)
                {
                    double[] estimates = new double[2];

                    //double predictLabel2 = SVM.PredictProbability(model.Value, nodes, out estimates);
                    double predictLabel = SVM.Predict(model.Value, nodes);

                    //if (predictLabel != predictLabel2) Console.WriteLine("fv " + fv.DatasetImage.Name + " " + predictLabel + " " + predictLabel2);

                    //Console.Write(fv.DatasetImage.Name + "\t" + fv.DatasetImage.ClassLabel + " \t" + model.Key + "\t" + predictLabel);

                    if (predictLabel == 1)
                    {
                        confusionMatrix[LookUpLabel[classLabel], LookUpLabel[model.Key]]++;
                    }
                    /*
                    foreach (double prob in estimates)
                    {
                        Console.Write("\t" + string.Format(CultureInfo.InvariantCulture, "{0:0.000}", prob));
                    }*/

                    //Console.WriteLine();

                }

                //Console.WriteLine(fv.DatasetImage.Name);

            }


            StringBuilder builder = new StringBuilder();

            int tp = 0;
            int sum = 0;

            builder.Append("CONFUSION MATRIX");
            builder.Append("\n");
            for (int i = 0; i < confusionMatrix.GetLength(0); i++) { builder.Append("----"); }
            builder.Append("\n");
            builder.Append("\t");
            for (int i = 0; i < confusionMatrix.GetLength(0); i++) { builder.Append(i); builder.Append("\t"); }
            builder.Append("\n");
            for (int i = 0; i < confusionMatrix.GetLength(0); i++) { builder.Append("----"); }
            builder.Append("\n");

            for (int i = 0; i < confusionMatrix.GetLength(0); i++)
            {
                builder.Append(i); builder.Append("|"); builder.Append("\t");

                for (int j = 0; j < confusionMatrix.GetLength(1); j++)
                {
                    builder.Append(string.Format("{00}", confusionMatrix[i, j])); builder.Append("\t");
                    sum += confusionMatrix[i, j];

                    if (i == j) { tp += confusionMatrix[i, j]; }
                }

                builder.Append("\n");
            }

            for (int i = 0; i < confusionMatrix.GetLength(0); i++) { builder.Append("----"); }

            string result = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", +(double)100 * (double)tp / (double)(descriptors.Count));

            builder.Append("\n");
            builder.Append(result);

            Console.WriteLine("\n Recongniton rate %" + result + "\n");
            File.WriteAllText(GlobalParams.CONFUSION_MATRIX_PATH, builder.ToString());
        }
    }
}

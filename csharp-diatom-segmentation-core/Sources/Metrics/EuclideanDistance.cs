﻿using System;

namespace csharp_image_segmentation_app.Sources.Metric
{
    public class EuclideanDistance
    {
        public static double GetDistance(int[] x, int[] y)
        {
            double sum = 0.0;
            double max = 0.0;

            for (int i = 0; i < x.Length; i++)
            {
                if(Math.Abs(x[i] - y[i]) > max) max = Math.Abs(x[i] - y[i]);
            }

            for (int i = 0; i < x.Length; i++)
            {
                sum += (Math.Abs(x[i] - y[i])/ max);                
            }

            return (sum/(double)x.Length) * 1000.0;
        }
    }
}



﻿using csharp_image_segmentation_app.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Sources.Dataset
{
    public class DirectoryBuilder
    {
        public string PathImage { get; private set; }
        public string PathAnnotation { get; private set; }
        public string PathLabel { get; private set; }

        public DirectoryBuilder(string root, string name)
        {
            PathImage = root + "/" + name + GlobalParams.ExtensionImage;
            PathAnnotation = root + "/" + name + GlobalParams.SuffixEdgeData + GlobalParams.ExtensionImage;
            PathLabel = root + "/" + name + GlobalParams.SuffixLabel + GlobalParams.ExtensionLabel;

            if (!(File.Exists(PathImage) && File.Exists(PathAnnotation) && File.Exists(PathLabel)))
            {
                throw new Exception("Missing image component : " + name);
            }
        }
    }
}

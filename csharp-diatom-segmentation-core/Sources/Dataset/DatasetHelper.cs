﻿using csharp_image_segmentation_app.Model;
using csharpsegmentationhelperwrapper;
using pelwrapper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace csharp_image_segmentation_app.Sources.Dataset
{
    public class DatasetHelper
    {
        private SegmentationWrapper seg;
        private PelWrapper pel;

        public DatasetHelper()
        {
            seg = new SegmentationWrapper();
            pel = new PelWrapper();
        }

        public unsafe void ExtractGroundTruth(Bitmap srcImage, Bitmap edgeImage, string labels)
        {            
            var width = srcImage.Width;
            var height = srcImage.Height;

            var pEdgeData = Utils.BitmapToPointerEdgeData(edgeImage);
            var pImage = Utils.BitmapToGrayScalePointerByteArray(srcImage);

            var pPelEdgeData = pel.execute(pEdgeData, width, height, 10);

            var edgeMap = Utils.FromSerialToEdgeMap(pPelEdgeData, width, height);

            var lookup = GetClassLabelLookUp(labels);

            foreach (EdgeSegment segment in edgeMap.Segments)
            {
                var serialpx = SerializeSegment(segment);

                var sAnnotationImage = seg.Extract(pImage, width, height, serialpx, segment.NoPixels);

                var gt = new Annotation();

                var index = 0;
                gt.Width = sAnnotationImage[index++];
                gt.Height = sAnnotationImage[index++];
                gt.Cx = sAnnotationImage[index++];
                gt.Cy = sAnnotationImage[index++];
                gt.Left = sAnnotationImage[index++];
                gt.Right = sAnnotationImage[index++];
                gt.Up = sAnnotationImage[index++];
                gt.Down = sAnnotationImage[index++];

                var dummyWidth = gt.Width;
                var offset = dummyWidth % 4;
                if (offset > 0) dummyWidth += 4 - offset;

                var roi = new byte[dummyWidth * gt.Height];

                for (var i = 0; i < gt.Height; i++)
                {
                    for (var j = 0; j < gt.Width; j++)
                    {
                        roi[i * dummyWidth + j] = (byte)sAnnotationImage[index++];
                    }
                }

                //gt.Segment = segment;
                //gt.BmpAnnotation = Utils.ByteToBitmap(roi, dummyWidth, gt.Height);
                gt.ClassLabel = lookup[gt.Key()];

                //Annotations.Add(gt);
            }

            pImage = null;
            pEdgeData = null;
            pPelEdgeData = null;

        }


        private unsafe int* SerializeSegment(EdgeSegment segment)
        {
            var serialpx = (int*)Marshal.AllocHGlobal(segment.NoPixels * 2 * sizeof(int));

            var index = 0;
            for (var i = 0; i < segment.NoPixels; i++)
            {
                serialpx[index++] = segment.Pixels[i].R;
                serialpx[index++] = segment.Pixels[i].C;
            }

            return serialpx;
        }

        private Dictionary<string, ClassLabel> GetClassLabelLookUp(string dir)
        {
            var cache = new Dictionary<string, ClassLabel>();

            string sWidth, sHeight, sLeft, sRight, sUp, sDown, sCx, sCy, sLabel;

            string key;

            var lines = File.ReadAllLines(dir);

            foreach (var line in lines)
            {
                if (!line.Equals(GlobalParams.Eof))
                {
                    sWidth = line.Substring(0, 5);
                    sHeight = line.Substring(5, 5);
                    sLeft = line.Substring(10, 5);
                    sRight = line.Substring(15, 5);
                    sUp = line.Substring(20, 5);
                    sDown = line.Substring(25, 5);
                    sCx = line.Substring(30, 5);
                    sCy = line.Substring(35, 5);
                    sLabel = line.Substring(40, 5);

                    key = sWidth + sHeight + sLeft + sRight + sUp + sDown + sCx + sCy;

                    int id = Convert.ToInt32(sLabel);

                    var label = new ClassLabel(id, GlobalParams.LabelLookUp[id]);

                    cache.Add(key, label);
                }
            }

            return cache;
        }
    }
}

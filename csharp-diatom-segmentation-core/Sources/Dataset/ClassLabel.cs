﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_image_segmentation_app.Sources.Dataset
{
    [Serializable]
    public class ClassLabel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public ClassLabel(int id, string name)
        {
            Id = id;
            Name = name;
        } 
    }
}

﻿using csharp_image_segmentation_app.Model;
using csharp_image_segmentation_app.Sources.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace csharp_image_segmentation_app.Sources.Dataset
{
    public class DatasetImage
    {
        public List<Annotation> Annotations { get; private set; }
        
        private string root, name;       

        public DatasetImage(string root, string name)
        {
            this.root = root;
            this.name = name;

            Annotations = new List<Annotation>();

            FillAnnotation();
        }
      
        private void FillAnnotation()
        {
            var dir = root  + Path.DirectorySeparatorChar + GlobalParams.FOLDER_LABELS + Path.DirectorySeparatorChar + name + GlobalParams.ExtensionLabel;

            var lines = File.ReadAllLines(dir);

            foreach (var line in lines)
            {
                if (!line.Equals(GlobalParams.Eof))
                {
                    var an = new Annotation();

                    an.Width = Convert.ToInt32(line.Substring(0, 5));                                        
                    an.Height = Convert.ToInt32(line.Substring(5, 5));
                    an.Left = Convert.ToInt32(line.Substring(10, 5));
                    an.Right = Convert.ToInt32(line.Substring(15, 5));
                    an.Up = Convert.ToInt32(line.Substring(20, 5));
                    an.Down = Convert.ToInt32(line.Substring(25, 5));
                    an.Cx = Convert.ToInt32(line.Substring(30, 5));
                    an.Cy = Convert.ToInt32(line.Substring(35, 5));                    
                    an.ClassLabel = new ClassLabel(Convert.ToInt32(line.Substring(40, 5)), GlobalParams.LabelLookUp[Convert.ToInt32(line.Substring(40, 5))]);                    

                    Annotations.Add(an);
                }
            }
        }

        public string GetName()
        {
            return name;
        }

        public string GetRoot()
        {
            return root;
        }
    }
}

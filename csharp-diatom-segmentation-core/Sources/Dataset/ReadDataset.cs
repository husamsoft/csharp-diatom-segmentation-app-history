﻿using csharp_image_segmentation_app.Model;
using csharp_image_segmentation_app.Sources.Dataset;
using System.Collections.Generic;
using System.IO;

namespace csharp_image_segmentation_app.Sources.DataSet
{
    public class ReadDataset
    {
        public DirectoryInfo Root { get; private set; }
        public List<DatasetImage> DataSetTestImages { get; set; }
        public List<DatasetImage> DataSetTrainImages { get; set; }

        public ReadDataset(string dir)
        {
            DataSetTestImages = new List<DatasetImage>();
            DataSetTrainImages = new List<DatasetImage>();

            ReadFolder(dir);
        }

        public void ReadFolder(string path)
        {
            Root = new DirectoryInfo(path);
            
            var iidsTest = File.ReadAllLines(path + Path.DirectorySeparatorChar + GlobalParams.IID_TEST);
            var iidsTrain = File.ReadAllLines(path + Path.DirectorySeparatorChar + GlobalParams.IID_TRAIN);            
            
            foreach (var file in iidsTest)
            {
                var dsi = new DatasetImage(path, file);

                DataSetTestImages.Add(dsi);                
            }

            foreach (var file in iidsTrain)
            {
                var dsi = new DatasetImage(path, file);

                DataSetTrainImages.Add(dsi);
            }
        }      
    }
}

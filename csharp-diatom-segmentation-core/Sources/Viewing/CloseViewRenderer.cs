﻿using csharp_image_segmentation_app.Sources.EdgeDetection;
using System;
using System.Drawing;

namespace csharp_image_segmentation_app.Sources.Viewing
{
    public class CloseViewRenderer
    {
        private const int CloseViewWidth = 567;

        public bool IsRgb { get; set; }
        public int ImgWidth { get; set; }
        public int ImgHeight { get; set; }
        public int NoCells { get; set; }
        public int CellSize { get; set; }
        public byte[] LocalMaxData { get; set; }
        public double[] Gradients { get; set; }
        public int GradThr { get; set; }
        public Bitmap BmpCloseView { get; set; }

        public Rgb<double>[] InputRgb { get; set; }

        private SolidBrush[] greyBrushes = new SolidBrush[256];

        public CloseViewRenderer()
        {
            NoCells = 20;
            CellSize = 20;
            GradThr = 16;

            //Init grey brushes
            for (int i = 0; i < 256; i++)
            {
                greyBrushes[i] = new SolidBrush(Color.FromArgb(i, i, i));
            }

            BmpCloseView = new Bitmap(CloseViewWidth, CloseViewWidth);
        }

        public void Initialize(ViewImage viewImage)
        {
            this.ImgWidth = viewImage.RawImage.Width;
            this.ImgHeight = viewImage.RawImage.Height;            
            this.Gradients = viewImage.GradientOperators.ArrGradient;
            this.InputRgb = viewImage.RgbDoubleImage;
            this.LocalMaxData = viewImage.GradientOperators.LocalMax;
        }

        public void Render(int x, int y, byte[] edgeData)
        {
            if (Gradients == null || edgeData == null) return;

            //Close view
            Graphics gxCloseView = Graphics.FromImage(BmpCloseView);
            gxCloseView.Clear(Color.Black);

            Font myFont = new Font(FontFamily.GenericMonospace, 7);
            SolidBrush redBrush = new SolidBrush(Color.Red);
            SolidBrush orangeBrush = new SolidBrush(Color.Orange);
            SolidBrush blueBrush = new SolidBrush(Color.DodgerBlue);
            SolidBrush blackBrush = new SolidBrush(Color.Black);
            SolidBrush whiteBrush = new SolidBrush(Color.White);

            int bias = (NoCells - 1) / 2;
            if (!IsRgb)
            {
                for (int j = 0; j < NoCells; j++)
                {
                    for (int i = 0; i < NoCells; i++)
                    {
                        int iY = y + j - bias, iX = x + i - bias;
                        if (iX < 0 || iX > ImgWidth) continue;
                        if (iY < 0 || iY > ImgHeight) continue;

                        int index = iY * ImgWidth + iX;
                        if (index < 0 || index >= ImgWidth * ImgHeight) continue;

                        string strGrad = String.Format("{0,3:0}", Gradients[index]);

                        if (edgeData[index] == 255)
                        {
                            SolidBrush selBrush = (LocalMaxData[index] != 0) ? redBrush : orangeBrush;

                            gxCloseView.FillRectangle(selBrush, i * CellSize, j * CellSize, CellSize, CellSize);

                            if (Gradients[index] > GradThr)
                                gxCloseView.DrawString(strGrad, myFont, whiteBrush, i * CellSize, j * CellSize + CellSize / 3);
                        }
                        else
                        {
                            gxCloseView.FillRectangle(greyBrushes[(int)Gradients[index]], i * CellSize, j * CellSize, CellSize, CellSize);

                            if (Gradients[index] > GradThr)
                            {
                                SolidBrush selBrush = (LocalMaxData[index] != 0) ? redBrush :
                                    (Gradients[index] < 128) ? whiteBrush : blackBrush;

                                gxCloseView.DrawString(strGrad, myFont, selBrush, i * CellSize, j * CellSize + CellSize / 3);
                            }
                        }
                    }
                }
            }
            else
            {
                for (int j = 0; j < NoCells; j++)
                {
                    for (int i = 0; i < NoCells; i++)
                    {
                        int iY = y + j - bias, iX = x + i - bias;
                        if (iX < 0 || iX > ImgWidth) continue;
                        if (iY < 0 || iY > ImgHeight) continue;

                        int index = iY * ImgWidth + iX;
                        if (index < 0 || index >= ImgWidth * ImgHeight) continue;

                        string strGrad = String.Format("{0,3:0}", Gradients[index]);

                        if (edgeData[index] == 255)
                        {
                            SolidBrush selBrush = (LocalMaxData[index] != 0) ? redBrush : orangeBrush;

                            gxCloseView.FillRectangle(selBrush, i * CellSize, j * CellSize, CellSize, CellSize);

                            if (Gradients[index] > (GradThr))
                                gxCloseView.DrawString(strGrad, myFont, whiteBrush, i * CellSize, j * CellSize + CellSize / 3);
                        }
                        else
                        {
                            SolidBrush colorBrush = new SolidBrush(Color.FromArgb((int)InputRgb[index].R, (int)InputRgb[index].G, (int)InputRgb[index].B));
                            gxCloseView.FillRectangle(colorBrush, i * CellSize, j * CellSize, CellSize, CellSize);

                            int greyValue = (colorBrush.Color.B + colorBrush.Color.G + colorBrush.Color.R) / 3;
                            if (Gradients[index] > GradThr)
                            {
                                SolidBrush selBrush = (LocalMaxData[index] != 0) ? redBrush : (greyValue < 128) ? whiteBrush : blackBrush;

                                gxCloseView.DrawString(strGrad, myFont, selBrush, i * CellSize, j * CellSize + CellSize / 3);
                            }
                        }
                    }
                }
            }

            for (int i = 0; i <= NoCells + 1; i++)
            {
                gxCloseView.DrawLine(new Pen(blueBrush, 1), 0, i * CellSize, CloseViewWidth, i * CellSize);
                gxCloseView.DrawLine(new Pen(blueBrush, 1), i * CellSize, 0, i * CellSize, CloseViewWidth);
            }
        }
    }
}

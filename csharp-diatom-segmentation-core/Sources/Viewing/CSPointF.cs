﻿using System;
using System.Drawing;

namespace csharp_image_segmentation_app.Sources.Viewing
{
    public class CSPointF
    {
        public float X, Y;
        public double tag;

        public CSPointF()
        {
            X = 0;
            Y = 0;
        }

        public CSPointF(float x, float y)
        {
            X = x;
            Y = y;
        }

        public CSPointF(float x, float y, double d)
        {
            X = x;
            Y = y;
            tag = d;
        }

        public CSPointF(PointF point)
        {
            X = point.X;
            Y = point.Y;
        }

        public CSPointF(CSPointF point)
        {
            X = point.X;
            Y = point.Y;
            tag = point.tag;
        }

        public int intX()
        {
            return (int)Math.Round(X);
        }

        public int intY()
        {
            return (int)Math.Round(Y);
        }

        public static CSPointF operator +(CSPointF op1, CSPointF op2)
        {
            CSPointF result = new CSPointF(op1.X + op2.X, op1.Y + op2.Y);
            return result;
        }

        public static CSPointF operator -(CSPointF op1, CSPointF op2)
        {
            CSPointF result = new CSPointF(op1.X - op2.X, op1.Y - op2.Y);
            return result;
        }

        public static CSPointF operator /(CSPointF op1, int op2)
        {
            CSPointF result = new CSPointF(op1.X / op2, op1.Y / op2);
            return result;
        }

        public static CSPointF operator -(CSPointF op1)
        {
            CSPointF result = new CSPointF(-op1.X, -op1.Y);
            return result;
        }

        public static bool operator ==(CSPointF op1, CSPointF op2)
        {
            if (op1.X == op2.X && op1.Y == op2.Y)
                return true;

            return false;
        }

        public static bool operator !=(CSPointF op1, CSPointF op2)
        {
            if (op1.X != op2.X || op1.Y != op2.Y)
                return true;
            else
                return false;
        }

        public void Set(float x, float y)
        {
            X = x;
            Y = y;
        }

        public void Set(float x, float y, double d)
        {
            X = x;
            Y = y;
            tag = d;
        }

        public CSPointF Rotate(double angle)
        {
            double radian = angle * Math.PI / 180.0;
            double[,] rotMat = { {Math.Cos(radian), -Math.Sin(radian)},
                                 {Math.Sin(radian), Math.Cos(radian)}};

            CSPointF result = new CSPointF();
            result.X = (float)(X * rotMat[0, 0] + Y * rotMat[1, 0]);
            result.Y = (float)(X * rotMat[0, 1] + Y * rotMat[1, 1]);

            return result;
        }

        public CSPointF Rotate2(double angle) //seperable matrices
        {
            //Rotation = rotMat1 x rotMat2 x rotMat1
            double radian = angle * Math.PI / 180.0;
            double[,] rotMat1 = {{1, -Math.Tan(radian / 2)},
                                 {0,           1         }};

            double[,] rotMat2 = {{        1       , 0},
                                 {Math.Sin(radian), 1}};

            CSPointF tmp1 = new CSPointF();
            CSPointF tmp2 = new CSPointF();
            CSPointF result = new CSPointF();

            tmp1.X = (float)(X * rotMat1[0, 0] + Y * rotMat1[1, 0]);
            tmp1.Y = (float)(X * rotMat1[0, 1] + Y * rotMat1[1, 1]);

            tmp2.X = (float)(tmp1.X * rotMat2[0, 0] + tmp1.Y * rotMat2[1, 0]);
            tmp2.Y = (float)(tmp1.X * rotMat2[0, 1] + tmp1.Y * rotMat2[1, 1]);

            result.X = (float)(tmp2.X * rotMat1[0, 0] + tmp2.Y * rotMat1[1, 0]);
            result.Y = (float)(tmp2.X * rotMat1[0, 1] + tmp2.Y * rotMat1[1, 1]);

            return result;
        }

        public Point ToPoint()
        {
            Point result = new Point((int)Math.Round(X), (int)Math.Round(Y));
            return result;
        }
        public PointF ToPointF()
        {
            PointF result = new PointF(X, Y);
            return result;
        }

        public CSPointF ReflectY()
        {
            return new CSPointF(X, -Y);
        }

        public PointF[] ToPointFArray(CSPointF[] csArray)
        {
            PointF[] array = new PointF[csArray.GetLength(0)];

            for (int i = 0; i < csArray.GetLength(0); i++)
                array[i] = csArray[i].ToPointF();

            return array;
        }

        public PointF[] ToReflectedPointFArray(CSPointF[] csArray)
        {
            PointF[] array = new PointF[csArray.GetLength(0)];

            for (int i = 0; i < csArray.GetLength(0); i++)
            {
                array[i] = csArray[i].ToPointF();
                csArray[i].Y = -csArray[i].Y;
            }

            return array;
        }

        public int ToRawCoordinates(int width)
        {
            int x = (int)Math.Round(X);
            int y = (int)Math.Round(Y);

            return y * width + x;
        }

        public int ToReflectedRawCoordinates(int width)
        {
            int x = (int)Math.Round(X);
            int y = (int)Math.Round(-Y);

            return y * width + x;
        }

        public override string ToString()
        {
            string str = "[ " + X.ToString("0") + " , " +
                Y.ToString("0") + " ]";

            return str;
        }
    }
}

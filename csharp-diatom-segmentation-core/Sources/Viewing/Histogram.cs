﻿using System;
using System.Drawing;

namespace csharp_image_segmentation_app
{
    class Histogram
    {
        public int[] hstg;
        public double mean, variance, entropy = -1;
        public int r_min, r_max, range, noSamples;  //r_min & r_max = lower and upper bounds of data range

        public Histogram(int[] data, int rMin, int rMax)
        {
            r_min = rMin;
            r_max = rMax;
            noSamples = data.Length;

            //Compute mean
            for (int i = 0; i < noSamples; i++)
                mean += data[i];
            mean /= noSamples;

            //Compute variance
            for (int i = 0; i < noSamples; i++)
                variance += (data[i] - mean) * (data[i] - mean);
            variance /= noSamples;

            if (r_min == -1)
            {
                r_min = data[0];
                for (int i = 0; i < noSamples; i++)
                    if (data[i] < r_min) r_min = data[i];
            }

            if (r_max == -1)
            {
                r_max = data[0];
                for (int i = 0; i < noSamples; i++)
                    if (data[i] > r_max) r_max = data[i];
            }

            range = r_max - r_min;
            hstg = new int[range + 1];

            for (int i = 0; i < noSamples; i++)
                hstg[data[i] - r_min]++;
        }

        public Histogram(byte[] data, int rMin, int rMax)
        {
            r_min = rMin;
            r_max = rMax;
            noSamples = data.Length;

            //Compute mean
            double sum = 0;
            for (int i = 0; i < noSamples; i++)
                sum += data[i];
            mean = sum / noSamples;

            //Compute variance
            for (int i = 0; i < noSamples; i++)
                variance += (data[i] - mean) * (data[i] - mean);
            variance /= noSamples;

            if (r_min == -1)
            {
                r_min = data[0];
                for (int i = 0; i < noSamples; i++)
                    if (data[i] < r_min) r_min = data[i];
            }

            if (r_max == -1)
            {
                r_max = data[0];
                for (int i = 0; i < noSamples; i++)
                    if (data[i] > r_max) r_max = data[i];
            }

            range = r_max - r_min;
            hstg = new int[range + 1];

            for (int i = 0; i < noSamples; i++)
                hstg[data[i] - r_min]++;
        }

        private void ComputeStatParams()
        {
            double sum = 0;

            for (int i = 0; i < noSamples; i++)
                sum += i * hstg[i];

            mean = sum / (double)noSamples;

            sum = 0;
            for (int i = 0; i < range; i++)
                sum += (i - mean) * (i - mean) * hstg[i];

            variance = sum / noSamples;
        }

        private void ComputeEntropy()
        {
            double sum = 0, p;

            for (int i = 0; i < range; i++)
            {
                if (hstg[i] == 0) continue;

                p = (double)hstg[i] / noSamples;
                sum += p * Math.Log(p, 2.0);
            }

            entropy = -sum;
        }

        public void PlotHistogram(Bitmap dest)
        {
            int range = hstg.Length - 1;
            //int range = r_max - r_min;
            float hMax = hstg[0];

            Graphics graphics = Graphics.FromImage(dest);
            graphics.Clear(Color.White);

            for (int i = 1; i <= range; i++)
                if (hMax < hstg[i]) hMax = hstg[i];

            hMax = hMax * 1.1f;
            float height = dest.Height - 2;
            float width = dest.Width - 3;
            float yRate = height / hMax;
            float xRate = width / range;

            Pen penRed = new Pen(Color.Red, 1.5f);
            Pen penRBlue = new Pen(Color.RoyalBlue, 1.5f);
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            for (int i = 0; i < range; i++)
                graphics.DrawLine(penRBlue, i * xRate, height, i * xRate, height - hstg[i] * yRate);

            for (int i = 0; i < range; i++)
                graphics.DrawLine(penRed, i * xRate, height - hstg[i] * yRate - 1, (i + 1) * xRate, height - hstg[i + 1] * yRate - 1);

            graphics.Dispose();
            //dest.Image = bmpGlobalHstg;
        }
        public int[] GetHistogram()
        {
            return hstg;
        }

        public int GetRMin()
        {
            return r_min;
        }

        public int GetRMax()
        {
            return r_max;
        }

        public int GetNoSamples()
        {
            return noSamples;
        }

        public int GetRange()
        {
            return range;
        }

        public double GetMean()
        {
            return mean;
        }

        public double GetVariance()
        {
            return variance;
        }

        public double GetEntropy()
        {
            if (entropy == -1) ComputeEntropy();

            return entropy;
        }
    }
}

﻿using csharp_image_segmentation_app.Model;
using csharp_image_segmentation_app.Sources.Dataset;
using csharpsegmentationhelperwrapper;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace csharp_image_segmentation_app.Sources.Helpers
{
    public unsafe class TemplateCropper
    {
        public List<Template> Templates { get; set; }

        private SegmentationWrapper segmentationWrapper;

        private EdgeMap edgeMap;

        private Bitmap bitmap;

        public TemplateCropper()
        {
            segmentationWrapper = new SegmentationWrapper();
            Templates = new List<Template>();
        }

        public void Build(DatasetImage dsi)
        {            
            var dir = dsi.GetRoot() + Path.DirectorySeparatorChar + GlobalParams.FOLDER_GT + Path.DirectorySeparatorChar + dsi.GetName() + GlobalParams.ExtensionImage;

            bitmap = (Bitmap)Bitmap.FromFile(dir);

            edgeMap = PelHelper.ToEdgeMap(bitmap, 36, 10);

            Dictionary<string, ClassLabel> dictionaryLabel = new Dictionary<string, ClassLabel>();

            foreach(Annotation an in dsi.Annotations)
            {
                dictionaryLabel.Add(an.Key(), an.ClassLabel);
            }

            for(int i = 0;i < edgeMap.NoSegments; i++)
            {
                Template template = ExtractTemplate(i);
                template.ClassLabel = dictionaryLabel[template.Key()];

                Templates.Add(template);
            }                       
        }

        public Template ExtractTemplate(int segmentIndex)
        {
            var pByteGrayImage = Utils.BitmapToGrayScalePointerByteArray(bitmap);

            var pixelsWrapper = (int*)Marshal.AllocHGlobal(edgeMap.Segments[segmentIndex].NoPixels * 2 * sizeof(int));

            var index = 0;
            for (var i = 0; i < edgeMap.Segments[segmentIndex].NoPixels; i++)
            {
                pixelsWrapper[index++] = edgeMap.Segments[segmentIndex].Pixels[i].R;
                pixelsWrapper[index++] = edgeMap.Segments[segmentIndex].Pixels[i].C;
            }

            var serial = segmentationWrapper.Extract(pByteGrayImage, bitmap.Width, bitmap.Height, pixelsWrapper, edgeMap.Segments[segmentIndex].NoPixels);

            index = 0;
            var width = serial[index++];
            var height = serial[index++];
            var cx = serial[index++];
            var cy = serial[index++];
            var left = serial[index++];
            var right = serial[index++];
            var up = serial[index++];
            var down = serial[index++];

            int virtualWidth = width;

            if (width % 4 != 0)
            {
                virtualWidth = width + (4 - width % 4);
            }

            var em = new EdgeMap(virtualWidth, height);
            em.Segments = new EdgeSegment[1];
            em.NoSegments = 1;
            em.Segments[0] = edgeMap.Segments[segmentIndex];

            EdgeSegment segment = new EdgeSegment(edgeMap.Segments[segmentIndex].NoPixels);

            for(int i = 0; i < edgeMap.Segments[segmentIndex].NoPixels; i++)
            {                
                segment.Pixels[i] = new Pixel(edgeMap.Segments[segmentIndex].Pixels[i].R - up, edgeMap.Segments[segmentIndex].Pixels[i].C - left);
            }
            em.Segments[0] = segment;

            var template = new Template();

            template.Width = width;
            template.Height = height;
            template.Cx = cx;
            template.Cy = cy;
            template.Left = left;
            template.Right = right;
            template.Up = up;
            template.Down = down;
            template.EdgeMap = em;

            //em.ToBitmap().Save(@"C:\Users\admin\Desktop\templates\junk\" + 1 +".bmp", ImageFormat.Bmp);
            //Console.WriteLine((right - left).ToString() + "\t" + width + " - " + (down - up).ToString() + " - " + height);

            return template;
        }
    }
}

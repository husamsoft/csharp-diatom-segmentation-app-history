﻿using csharp_image_segmentation_app.Sources.Dataset;
using System;
using System.Text;

namespace csharp_image_segmentation_app.Sources.Helpers
{
    [Serializable]
    public unsafe class Template
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int Cx { get; set; }
        public int Cy { get; set; }
        public int Left { get; set; }
        public int Right { get; set; }
        public int Up { get; set; }
        public int Down { get; set; }
        public EdgeMap EdgeMap { get; set; }
        public ClassLabel ClassLabel { get; set; }

        public Template()
        {
        }

        public string Key()
        {
            var builder = new StringBuilder();

            builder.Append(Width.ToString("00000"));
            builder.Append(Height.ToString("00000"));
            builder.Append(Left.ToString("00000"));
            builder.Append(Right.ToString("00000"));
            builder.Append(Up.ToString("00000"));
            builder.Append(Down.ToString("00000"));
            builder.Append(Cx.ToString("00000"));
            builder.Append(Cy.ToString("00000"));

            return builder.ToString();
        }
    }
}

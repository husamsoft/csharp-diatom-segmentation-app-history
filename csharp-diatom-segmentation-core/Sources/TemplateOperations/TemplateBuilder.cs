﻿using csharp_image_segmentation_app.Model;
using csharp_image_segmentation_app.Sources.Dataset;
using csharp_image_segmentation_app.Sources.DataSet;
using csharp_image_segmentation_app.Sources.Helpers;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace csharp_image_segmentation_app.Sources.TemplateOperations
{
    public class TemplateBuilder
    {
        public static void BuildTemplate(string srcRootPath, string dstPath)
        {
            var dataset = new ReadDataset(srcRootPath);

            var templates = new Dictionary<ClassLabel, Template>();

            var set = new HashSet<int>();

            for (int i = 0; i < 100; i++)
            {
                var dsi = dataset.DataSetTestImages[i];

                var dir = dsi.GetRoot() + Path.DirectorySeparatorChar + GlobalParams.FOLDER_IMAGE + Path.DirectorySeparatorChar + "test\\" + dsi.GetName() + GlobalParams.ExtensionImage;

                var bitmap = (Bitmap)Bitmap.FromFile(dir);

                TemplateCropper extractor = new TemplateCropper();

                extractor.Build(dsi);

                foreach (Template template in extractor.Templates)
                {
                    if (!set.Contains(template.ClassLabel.Id))
                    {
                        templates.Add(template.ClassLabel, template);
                        set.Add(template.ClassLabel.Id);                        
                    }
                }
            }

            WriteToBinaryFile<Dictionary<ClassLabel, Template>>(dstPath, templates);
            /*
            foreach(Template template in templates.Values)
            {
                Bitmap bmp = template.EdgeMap.ToBitmap();
                bmp = Utils.Invert(bmp);

                bmp.Save(@"C:\Users\27475003794ana\Desktop\templates\" + template.ClassLabel.Name + ".bmp", ImageFormat.Bmp);
            }*/                        
        }

        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        public static T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }
    }
}

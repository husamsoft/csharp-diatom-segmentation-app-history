﻿using csharp_image_segmentation_app.Sources.Dataset;
using csharp_image_segmentation_app.Sources.Helpers;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace csharp_image_segmentation_app.Sources.DetectionBenchmark
{
    public class SegmentByED
    {
        private Bitmap bitmap, dstBitmap;

        private int threshold;

        private List<Annotation> annotations;

        private EdgeMap edgeMap;

        public SegmentByED(Bitmap bitmap, List<Annotation> annotations, int threshold)
        {
            this.bitmap = bitmap;
            this.threshold = threshold;
            this.annotations = annotations;

            dstBitmap = new Bitmap(bitmap.Width, bitmap.Height, PixelFormat.Format24bppRgb);

            SegmentationByImage();
        }
        /*
        private void SegmentationByAnnotation ()
        {
            using (Graphics grfx = Graphics.FromImage(dstBitmap))
            {
                foreach (Annotation an in annotations)
                {
                    var dummyLeft = an.Left - (int)Math.Ceiling((double)an.Left * (double)GlobalParams.ExtentRation / (double)100.0);
                    var dummyRight = an.Right + (int)Math.Ceiling((double)an.Right * (double)GlobalParams.ExtentRation / (double)100.0);
                    var dummyUp = an.Up - (int)Math.Ceiling((double)an.Up * (double)GlobalParams.ExtentRation / (double)100.0);
                    var dummyDown = an.Down + (int)Math.Ceiling((double)an.Down * (double)GlobalParams.ExtentRation / (double)100.0);

                    if (dummyLeft < 0) dummyLeft = an.Left;
                    if (dummyUp < 0) dummyUp = an.Up;
                    if (dummyRight > bitmap.Width) dummyRight = an.Right;
                    if (dummyDown > bitmap.Height) dummyDown = an.Down; 

                    var roi = new Rectangle(dummyLeft, dummyUp, dummyRight - dummyLeft, dummyDown - dummyUp);

                    var bitmapCrop = Utils.BitmapRegulation(bitmap.Clone(roi, bitmap.PixelFormat));                  

                    edgeMap = EdHelper.ToEdgeMap(bitmapCrop, threshold);
                   
                    var binaryImage = edgeMap.ToColorBitmap();
                                             
                    grfx.DrawImage(binaryImage, dummyLeft, dummyUp);                    
                }
            }
        }
        */
        private void SegmentationByImage()
        {
            edgeMap = EdHelper.ToEdgeMap(bitmap, threshold);
        }

        public unsafe Bitmap ToEdgeBitmap()
        {
            edgeMap = ToSplittedEdgeMap(200);

            dstBitmap = edgeMap.ToColorBitmap();

            return dstBitmap;
        }

        public EdgeMap ToSplittedEdgeMap(int len)
        {
            EdgeMap splittedMap = new EdgeMap(bitmap.Width, bitmap.Height);

            List<EdgeSegment> segmentList = new List<EdgeSegment>();

            foreach (EdgeSegment segment in edgeMap.Segments)
            {
                List<Pixel> pixels = new List<Pixel>();

                int index = 0;
                for (int i = 0; i < segment.NoPixels; i++)
                {
                    if (index > len)
                    {
                        EdgeSegment splittedSegment = new EdgeSegment(pixels.Count);
                        splittedSegment.Pixels = pixels.ToArray();
                        segmentList.Add(splittedSegment);

                        pixels.Clear();

                        index = 0;
                    }

                    pixels.Add(new Pixel(segment.Pixels[i].R, segment.Pixels[i].C));

                    index++;
                }
            }

            splittedMap.NoSegments = segmentList.Count;
            splittedMap.Segments = segmentList.ToArray();

            return splittedMap;
        }
    }
}
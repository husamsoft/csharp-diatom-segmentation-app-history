﻿using csharp_image_segmentation_app.Helpers;
using csharp_image_segmentation_app.Model;
using csharp_image_segmentation_app.Sources.Dataset;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace csharp_image_segmentation_app.Sources.DetectionBenchmark
{
    public class SegmentByOtsu
    {
        private Bitmap srcBitmap, dstBitmap;

        private List<Annotation> annotations;
        private OtsuBinarization binarization;

        public SegmentByOtsu(Bitmap bitmap, List<Annotation> annotations)
        {
            this.srcBitmap = bitmap;
            this.annotations = annotations;

            binarization = new OtsuBinarization();
            dstBitmap = new Bitmap(bitmap.Width, bitmap.Height, PixelFormat.Format24bppRgb);

            Segmentation();
        }

        private unsafe void Segmentation()
        {
            var w = srcBitmap.Width;
            var h = srcBitmap.Height;

            using (Graphics grfx = Graphics.FromImage(dstBitmap))
            {
                foreach (Annotation an in annotations)
                {
                    var dummyLeft = an.Left - (int)Math.Ceiling((double)an.Left * (double)GlobalParams.ExtentRation / (double)100.0);
                    var dummyRight = an.Right + (int)Math.Ceiling((double)an.Right * (double)GlobalParams.ExtentRation / (double)100.0);
                    var dummyUp = an.Up - (int)Math.Ceiling((double)an.Up * (double)GlobalParams.ExtentRation / (double)100.0);
                    var dummyDown = an.Down + (int)Math.Ceiling((double)an.Down * (double)GlobalParams.ExtentRation / (double)100.0);

                    if (dummyLeft < 0) dummyLeft = an.Left;
                    if (dummyUp < 0) dummyUp = an.Up;
                    if (dummyRight > srcBitmap.Width) dummyRight = an.Right;
                    if (dummyDown > srcBitmap.Height) dummyDown = an.Down;

                    var roi = new Rectangle(dummyLeft, dummyUp, dummyRight - dummyLeft, dummyDown - dummyUp);

                    var bitmapRoi = srcBitmap.Clone(roi, srcBitmap.PixelFormat);

                    var bitmapCrop = Utils.BitmapRegulation(bitmapRoi);

                    var bitmapCropBinary = binarization.ThresholdByOtsu(bitmapCrop);

                    grfx.DrawImage(bitmapCropBinary, dummyLeft, dummyUp);

                    bitmapCrop.Dispose();
                    bitmapCropBinary.Dispose();
                    bitmapRoi.Dispose();                   
                }

                grfx.Dispose();
            }            
        }

        public Bitmap ToEdgeBitmap()
        {
            return dstBitmap;         
        }

        public void Dispose()
        {
            dstBitmap.Dispose();
            annotations.Clear();
            binarization.Dispose();
        } 

    }
}

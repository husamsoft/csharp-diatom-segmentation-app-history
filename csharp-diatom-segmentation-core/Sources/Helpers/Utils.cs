﻿using edgedrawingwrapper;
using pelwrapper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace csharp_image_segmentation_app
{
    public static unsafe class Utils
    {
        public static PelWrapper PelWrapper = new PelWrapper();
        public static Edgedrawingwrapper EdWrapper = new Edgedrawingwrapper();

        public static Bitmap BitmapRegulation(Bitmap bmpRawImg)
        {
            int virtualWidth = bmpRawImg.Width;

            if (bmpRawImg.Width % 4 != 0)
            {
                virtualWidth = bmpRawImg.Width - (4 + bmpRawImg.Width % 4);
            }

            return bmpRawImg.Clone(new Rectangle(0, 0, virtualWidth, bmpRawImg.Height), bmpRawImg.PixelFormat);
        }

        public static Bitmap ToGrayscale(Bitmap src)
        {
            Bitmap dst = (Bitmap)src.Clone();

            BitmapData bmpData = dst.LockBits(new Rectangle(0, 0, dst.Width, dst.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    *ptr = (byte)((ptr[2] * .299) + (ptr[1] * .587) + (ptr[0] * .114));
                    ptr[1] = *ptr;
                    ptr[2] = *ptr;

                    ptr += 3;
                }
            }

            dst.UnlockBits(bmpData);

            return dst;
        }

        internal static void Byte2dArrayToBitmap(double[,] angles)
        {
            throw new NotImplementedException();
        }

        public static Bitmap Invert(Bitmap src)
        {
            Bitmap dst = (Bitmap)src.Clone();

            BitmapData bmpData = dst.LockBits(new Rectangle(0, 0, dst.Width, dst.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    if (ptr[2] != 0 || ptr[1] != 0 || ptr[0] != 0)
                    {
                        *ptr = (byte)(0);
                        ptr[1] = *ptr;
                        ptr[2] = *ptr;
                    }
                    else
                    {
                        *ptr = (byte)(255);
                        ptr[1] = *ptr;
                        ptr[2] = *ptr;
                    }

                    ptr += 3;
                }
            }

            dst.UnlockBits(bmpData);

            return dst;
        }

        public static byte[] BitmapToGray1DbyteArray(Bitmap src)
        {
            int index = 0;

            byte[] dst = new byte[src.Width * src.Height];

            BitmapData bmpData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    /*
                    *ptr = (byte)((ptr[2] * .299) + (ptr[1] * .587) + (ptr[0] * .114));
                    ptr[1] = *ptr;
                    ptr[2] = *ptr;
                    */
                    /*
                    if(ptr[0] != 0 || ptr[1] != 0 || ptr[2] != 0)
                    {
                        dst[index] = 255;
                    }
                    */
                    dst[index] = (byte)((ptr[2] * 0.2989) + (ptr[1] * 0.5870) + (ptr[0] * 0.1140));
                    index++;

                    ptr += 3;
                }
            }

            src.UnlockBits(bmpData);

            return dst;
        }

        public static byte* BitmapToGrayScalePointerByteArray(Bitmap src)
        {
            byte* p = (byte*)Marshal.AllocHGlobal(src.Width * src.Height * sizeof(byte));

            int w = src.Width;
            int h = src.Height;

            Rgb<double>[] imgRGB = new Rgb<double>[w * h];
            BitmapData tmpData = src.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
            {
                p[i] = (byte)(Math.Round((scan0[i * 3 + 2] * 0.2989) + (scan0[i * 3 + 1] * 0.5870) + (scan0[i * 3] * 0.1140)));
            }
            src.UnlockBits(tmpData);

            return p;
        }

        public static Rgb<double>[] BitmapTo1Drgb(Bitmap src)
        {
            int w = src.Width;
            int h = src.Height;

            Rgb<double>[] imgRGB = new Rgb<double>[w * h];
            BitmapData tmpData = src.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
            {
                imgRGB[i].B = scan0[i * 3];
                imgRGB[i].G = scan0[i * 3 + 1];
                imgRGB[i].R = scan0[i * 3 + 2];
            }
            src.UnlockBits(tmpData);

            return imgRGB;
        }

        public static Bitmap RgbToBitmap(Rgb<double>[] img, int w, int h)
        {
            Bitmap tmpBmp = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
            {
                scan0[i * 3] = (byte)img[i].B;
                scan0[i * 3 + 1] = (byte)img[i].G;
                scan0[i * 3 + 2] = (byte)img[i].R;
            }
            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }
        public static Bitmap ByteToBitmap(byte[] img, int w, int h)
        {
            Bitmap tmpBmp = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
                scan0[i * 3] = scan0[i * 3 + 1] = scan0[i * 3 + 2] = (img[i] != (byte)0 ? (byte)255 : (byte)0);

            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }
        public static Bitmap PointerToBitmap(int* img, int w, int h)
        {
            Bitmap tmpBmp = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
                scan0[i * 3] = scan0[i * 3 + 1] = scan0[i * 3 + 2] = (byte)img[i];

            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }

        public static Bitmap ByteToEdgeBitmap(byte[] img, int w, int h)
        {
            Bitmap tmpBmp = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
            {
                if (img[i] != 0)
                {
                    scan0[i * 3] = 0;
                    scan0[i * 3 + 1] = 0;
                    scan0[i * 3 + 2] = 255;
                }
            }

            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }
        public static Rgb<double>[] BitmapToRgb(Bitmap src)
        {
            int w = src.Width;
            int h = src.Height;

            Rgb<double>[] imgRGB = new Rgb<double>[w * h];
            BitmapData tmpData = src.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
            {
                imgRGB[i].B = scan0[i * 3];
                imgRGB[i].G = scan0[i * 3 + 1];
                imgRGB[i].R = scan0[i * 3 + 2];
            }
            src.UnlockBits(tmpData);

            return imgRGB;
        }
        public static Bitmap DoubleToBitmap(double[] src, int w, int h)
        {
            double max = 0;

            for (int i = 0; i < w * h; i++) if (src[i] > max) max = src[i];

            Bitmap tmpBmp = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            BitmapData tmpData = tmpBmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)tmpData.Scan0;

            for (int i = 0; i < w * h; i++)
            {
                byte tmp = (byte)(src[i] / max * 255.0);
                scan0[i * 3] = scan0[i * 3 + 1] = scan0[i * 3 + 2] = tmp;
            }
            tmpBmp.UnlockBits(tmpData);

            return tmpBmp;
        }

        public static byte[] BitmapToEdgeData(Bitmap src)
        {
            var imgLength = src.Width * src.Height;
            var rectangle = new Rectangle(0, 0, src.Width, src.Height);
            var overlayBmpData = src.LockBits(rectangle, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            var edgeData = new byte[imgLength];

            byte* scan0 = (byte*)overlayBmpData.Scan0;
            for (int i = 0; i < imgLength * 3; i += 3)
            {
                if (scan0[i] == 0 && scan0[i + 1] == 0 && scan0[i + 2] == 255)
                {
                    edgeData[i / 3] = 255;
                }
            }

            src.UnlockBits(overlayBmpData);

            return edgeData;
        }

        public static byte* BitmapToPointerEdgeData(Bitmap src)
        {
            byte* p = (byte*)Marshal.AllocHGlobal(src.Width * src.Height * sizeof(byte));

            var imgLength = src.Width * src.Height;
            var rectangle = new Rectangle(0, 0, src.Width, src.Height);
            var overlayBmpData = src.LockBits(rectangle, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

            byte* scan0 = (byte*)overlayBmpData.Scan0;
            for (int i = 0; i < imgLength * 3; i += 3)
            {
                if (scan0[i] == 0 && scan0[i + 1] == 0 && scan0[i + 2] != 0)
                {
                    p[i / 3] = 255;
                }
            }

            src.UnlockBits(overlayBmpData);

            return p;
        }

        public static EdgeMap FromSerialToEdgeMap(int* serialData, int width, int height)
        {
            EdgeMap edgeMap = new EdgeMap(width, height);

            int noSegments = serialData[0];

            EdgeSegment[] segments = new EdgeSegment[noSegments];

            int index = 1;
            for (int i = 0; i < noSegments; i++)
            {
                int noPixel = serialData[index++];
                segments[i] = new EdgeSegment(noPixel);
            }

            for (int i = 0; i < noSegments; i++)
            {
                for (int k = 0; k < segments[i].NoPixels; k++)
                {
                    int r = serialData[index++];
                    int c = serialData[index++];

                    segments[i].Pixels[k] = new Pixel(r, c);
                }
            }

            edgeMap.Segments = segments;
            edgeMap.NoSegments = noSegments;            

           //Marshal.FreeCoTaskMem((IntPtr)serialData);

            return edgeMap;
        }

        public static byte[,] BitmapTo2Darray(Bitmap src)
        {
            int width = src.Width;
            int height = src.Height;

            byte[,] dst = new byte[width, height];

            BitmapData bmpData = src.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                int ind = 0;

                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    int x = ind % width;
                    int y = ind / width;

                    dst[x, y] = ptr[0];

                    ptr += 3;

                    ind++;
                }
            }

            src.UnlockBits(bmpData);

            return dst;
        }

        public static byte[,] BitmapToGray2Darray(Bitmap src)
        {           
            int width = src.Width;
            int height = src.Height;

            byte[,] dst = new byte[src.Width, src.Height];

            BitmapData bmpData = src.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            
            byte* scan0 = (byte*)bmpData.Scan0;

            int ind = 0;
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    dst[i, j] = (byte)(Math.Round((double)scan0[ind * 3 + 2] * 0.2989) + ((double)scan0[ind * 3 + 1] * 0.5870) + ((double)scan0[ind * 3 + 0] * 0.1140));

                    ind++;
                }
            }
            
            src.UnlockBits(bmpData);


            return dst;

        }

        public static Bitmap Byte2dArrayToBitmap(byte[,] src)
        {
            int width = src.GetLength(0);
            int height = src.GetLength(1);

            Bitmap dst = new Bitmap(width, height);

            BitmapData bmpData = dst.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                int ind = 0;

                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    int x = ind % width;
                    int y = ind / width;

                    *ptr = (byte)(src[x, y]);
                    ptr[1] = *ptr;
                    ptr[2] = *ptr;

                    ptr += 3;
                    ind++;
                }
            }

            dst.UnlockBits(bmpData);

            return dst;
        }

        public static Bitmap Double1dArrayToBitmap(double[] src, int width, int height)
        {
            double max = 0f;

            for (int i = 0; i < src.Length; i++) max = (src[i] > max) ? src[i] : max;

            Bitmap dst = new Bitmap(width, height);

            BitmapData bmpData = dst.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                int ind = 0;

                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {                    
                    *ptr = (byte)(255*(src[ind]/max));
                    ptr[1] = *ptr;
                    ptr[2] = *ptr;

                    ptr += 3;
                    ind++;
                }
            }

            dst.UnlockBits(bmpData);

            return dst;
        }

        public static Bitmap Double2dArrayToBitmap(double[,] src)
        {            
            int width = src.GetLength(0);
            int height = src.GetLength(1);

            double max = 0;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (src[i, j] > max) max = src[i, j];
                }
            }
   
            Bitmap dst = new Bitmap(width, height);

            BitmapData bmpData = dst.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            unsafe
            {
                int ind = 0;

                byte* ptr = (byte*)bmpData.Scan0.ToPointer();
                int stopAddress = (int)ptr + bmpData.Stride * bmpData.Height;

                while ((int)ptr != stopAddress)
                {
                    int x = ind % width;
                    int y = ind / width;

                    *ptr = (byte)(255.0 * src[x, y] / max);
                    ptr[1] = *ptr;
                    ptr[2] = *ptr;

                    ptr += 3;
                    ind++;
                }
            }

            dst.UnlockBits(bmpData);

            return dst;
        }

        public static byte* BitmapToGrayScalePointerBytes(Bitmap src)
        {
            byte[] byteImage =  BitmapToGray1DbyteArray(src);

            int length = src.Width * src.Height;

            byte* p = (byte*)Marshal.AllocHGlobal(length * sizeof(byte));

            for (int i = 0; i < length; i++)
            {
                p[i] = byteImage[i];
            }

            return p;
        }

        public static List<Pixel[]> SplitAndOverlaptEdgeSegmentByValues(EdgeSegment edgeSegment, int splitlen, int splitoverlapt)
        {
            List<Pixel[]> pixels = new List<Pixel[]>();

            for (int i = 0; i < edgeSegment.NoPixels - splitlen; i += splitoverlapt)
            {
                Pixel[] p = new Pixel[splitlen];

                for (int j = 0; j < splitlen; j++)
                {
                    p[j] = edgeSegment.Pixels[j + i];
                }

                pixels.Add(p);
            }

            return pixels;
        }

        public static List<Pixel[]> SplitEdgeSegmentByValues(EdgeSegment edgeSegment, int splitlen)
        {
            List<Pixel[]> pixels = new List<Pixel[]>();

            int division = edgeSegment.NoPixels / splitlen;

            for(int i = 0; i < division; i++)
            {                                                
                Pixel[] p = new Pixel[splitlen];

                for (int j = 0; j < splitlen; j++)
                {
                    p[j] = edgeSegment.Pixels[i * splitlen + j];
                }

                pixels.Add(p);
               
            }

            int remain = edgeSegment.NoPixels - division * splitlen;
            
            if ( remain != 0)
            {
                Pixel[] p = new Pixel[remain];

                for (int j = 0; j < remain; j++)
                {
                    p[j] = edgeSegment.Pixels[j + division * splitlen];
                }

                pixels.Add(p);
            }

            return pixels;
        }
    }
}

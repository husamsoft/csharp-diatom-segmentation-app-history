﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace csharp_image_segmentation_app.Sources.Helpers
{
    public unsafe static class EdHelper
    {
        public static EdgeMap ToEdgeMap(Bitmap src, int threshold)
        {
            byte* pByteImage = Utils.BitmapToGrayScalePointerBytes(src);

            int* pEdgeImage = Utils.EdWrapper.WrapperGrayEDV(pByteImage, src.Width, src.Height, threshold);

            Marshal.FreeCoTaskMem((IntPtr)pByteImage);

            return Utils.FromSerialToEdgeMap(pEdgeImage, src.Width, src.Height);                        
        }
    }
}

﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace csharp_image_segmentation_app.Sources.Helpers
{
    public unsafe class PelHelper
    {
        public static EdgeMap ToEdgeMap(Bitmap src, int threshold, int len)
        {
            var p = Utils.BitmapToPointerEdgeData(src);

            var serial = Utils.PelWrapper.execute(p, src.Width, src.Height, len);

            Marshal.FreeCoTaskMem((IntPtr)p);

            return Utils.FromSerialToEdgeMap(serial, src.Width, src.Height);            
        }      
    }
}

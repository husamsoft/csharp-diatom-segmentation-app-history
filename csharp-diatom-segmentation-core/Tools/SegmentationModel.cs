﻿using csharp_image_segmentation_app.Helpers;
using csharp_image_segmentation_app.Sources.EdgeDetection;
using csharp_image_segmentation_app.Sources.EdgeDetection.Canny;
using csharp_image_segmentation_app.Sources.Viewing;
using edgedrawingwrapper;
using pelwrapper;
using System.Drawing;

namespace csharp_image_segmentation_app.Tools
{
    public class SegmentationModel
    {
        public Bitmap bmpDisImg {get; private set;}
        public Bitmap bmpCloseViewImg { get; private set; }

        private ViewImage _viewImage;

        private CloseViewRenderer _closeViewRenderer;

        public SegmentationModel(Bitmap bmpRawImg)
        {
            //Gaussian g = new Gaussian(bmpRawImg);

            //bmpRawImg = g.Process(5);  

            _viewImage = new ViewImage(bmpRawImg, GradientOperator.PREWITT_OPERATOR, 36);

            _closeViewRenderer = new CloseViewRenderer();

            _closeViewRenderer.Initialize(_viewImage);

            ReloadImage();
        }

        public void EdEdgeImage()
        {
            _viewImage.Intialize(GradientOperator.PREWITT_OPERATOR, 36);

            bmpDisImg = _viewImage.EdgeImage.BitmapColor;


            //bmpDisImg = c.();

        }

        public void CannyEdgeImage()
        {
            var canny = new Canny(_viewImage.RawImage);
            bmpDisImg = canny.EdgeMapAsBitmap();
        }

        public unsafe void BinarizeImageByOtsu()
        {
            // otsu
            var otsu = new OtsuBinarization();
            var b = otsu.ThresholdByOtsu(_viewImage.RawImage);


            // Ed
            //var edHelper = new EdHelper(_viewImage.RawImage, 36);
            var temp = new ViewImage(b, GradientOperator.PREWITT_OPERATOR, 36);

            bmpDisImg = temp.EdgeImage.BitmapColor;            


        }

        public void ReloadImage()
        {
            bmpDisImg = (Bitmap)_viewImage.RawImage.Clone();
        }

        public void RenderCloseView(int x, int y)
        {
            if (_viewImage.RawImage == null) return;

            _closeViewRenderer.Render(x, y, _viewImage.EdgeImage.BinaryEdge);

            bmpCloseViewImg = _closeViewRenderer.BmpCloseView;
        }

        public unsafe void PelEdgeMap(int threshold, int len)
        {
            byte* pByteImage = Utils.BitmapToGrayScalePointerBytes(_viewImage.RawImage);

            var edvWrapper = new Edgedrawingwrapper();

            int* pEdgeImage = edvWrapper.WrapperGrayEDV(pByteImage, _viewImage.RawImage.Width, _viewImage.RawImage.Height, threshold);

            var edgeMap = Utils.FromSerialToEdgeMap(pEdgeImage, _viewImage.RawImage.Width, _viewImage.RawImage.Height);
            
            var pelWrapper = new PelWrapper();

            var map = Utils.FromSerialToEdgeMap(pelWrapper.execute(edgeMap.ToPointerEdgeImage(), _viewImage.RawImage.Width, _viewImage.RawImage.Height, len), _viewImage.RawImage.Width, _viewImage.RawImage.Height);

            _viewImage.Intialize(map, GradientOperator.PREWITT_OPERATOR, 136);

            bmpDisImg = _viewImage.EdgeImage.BitmapColor;

        }
    }
}

﻿using csharp_image_segmentation_app.Model;
using csharp_image_segmentation_app.Sources.DataSet;
using csharp_image_segmentation_app.Sources.DetectionBenchmark;
using csharp_image_segmentation_app.Sources.Helpers;
using csharp_image_segmentation_app.Sources.Morphology;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csharp_image_segmentation_app.Tools
{
    public partial class FrmBenchmark : Form
    {

        private static string PathSrc = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + "Dataset";
        //private static string PathDst = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + "temp/";        
        private static string PathDst = @"\\vmware-host\Shared Folders\test\";

        public FrmBenchmark()
        {
            InitializeComponent();
        }

        private void btnBoundaryBenchmark_Click(object sender, EventArgs e)
        {
            var dataset = new ReadDataset(PathSrc);
            var index = 1;

            foreach (var dsi in dataset.DataSetTestImages)
            {
                var dir = dsi.GetRoot() + Path.DirectorySeparatorChar + GlobalParams.FOLDER_IMAGE + Path.DirectorySeparatorChar + dsi.GetName() + GlobalParams.ExtensionImage;

                var bitmap = (Bitmap)Bitmap.FromFile(dir);

                var sed = new SegmentByED(bitmap, dsi.Annotations, 60);

                var sedBitmap = sed.ToEdgeBitmap();

                sedBitmap.Save(PathDst + dsi.GetName() + ".bmp", ImageFormat.Bmp);

                lblProcessed.Text = index.ToString();
                lblProcessed.Update();            

                pBoxInputImg.Image = sedBitmap;
                pBoxInputImg.Update();

                index++;

                bitmap.Dispose();
                sedBitmap.Dispose();
            }
        }
    }
}

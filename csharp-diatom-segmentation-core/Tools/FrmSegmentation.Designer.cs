﻿namespace csharp_image_segmentation_app.UI
{
    partial class FrmSegmentation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpen = new System.Windows.Forms.Button();
            this.panelInputImage = new System.Windows.Forms.Panel();
            this.pBoxInputImg = new System.Windows.Forms.PictureBox();
            this.gBoxCloseView = new System.Windows.Forms.GroupBox();
            this.pBoxCloseView = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPEL = new System.Windows.Forms.Button();
            this.btnCanny = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnOtsuBinary = new System.Windows.Forms.Button();
            this.btnED = new System.Windows.Forms.Button();
            this.panelInputImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxInputImg)).BeginInit();
            this.gBoxCloseView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxCloseView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(8, 19);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(100, 30);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // panelInputImage
            // 
            this.panelInputImage.AutoScroll = true;
            this.panelInputImage.Controls.Add(this.pBoxInputImg);
            this.panelInputImage.Location = new System.Drawing.Point(432, 28);
            this.panelInputImage.Name = "panelInputImage";
            this.panelInputImage.Size = new System.Drawing.Size(804, 583);
            this.panelInputImage.TabIndex = 1;
            // 
            // pBoxInputImg
            // 
            this.pBoxInputImg.BackColor = System.Drawing.Color.Black;
            this.pBoxInputImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxInputImg.Location = new System.Drawing.Point(3, 3);
            this.pBoxInputImg.Name = "pBoxInputImg";
            this.pBoxInputImg.Size = new System.Drawing.Size(500, 400);
            this.pBoxInputImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBoxInputImg.TabIndex = 0;
            this.pBoxInputImg.TabStop = false;
            this.pBoxInputImg.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pBoxInputImg_MouseMove);
            // 
            // gBoxCloseView
            // 
            this.gBoxCloseView.Controls.Add(this.pBoxCloseView);
            this.gBoxCloseView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gBoxCloseView.Location = new System.Drawing.Point(12, 12);
            this.gBoxCloseView.Name = "gBoxCloseView";
            this.gBoxCloseView.Size = new System.Drawing.Size(414, 425);
            this.gBoxCloseView.TabIndex = 26;
            this.gBoxCloseView.TabStop = false;
            this.gBoxCloseView.Text = "Close View";
            // 
            // pBoxCloseView
            // 
            this.pBoxCloseView.BackColor = System.Drawing.Color.Black;
            this.pBoxCloseView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxCloseView.Location = new System.Drawing.Point(6, 18);
            this.pBoxCloseView.Name = "pBoxCloseView";
            this.pBoxCloseView.Size = new System.Drawing.Size(403, 403);
            this.pBoxCloseView.TabIndex = 1;
            this.pBoxCloseView.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPEL);
            this.groupBox1.Controls.Add(this.btnCanny);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btnOtsuBinary);
            this.groupBox1.Controls.Add(this.btnED);
            this.groupBox1.Controls.Add(this.btnOpen);
            this.groupBox1.Location = new System.Drawing.Point(12, 443);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 168);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // btnPEL
            // 
            this.btnPEL.Location = new System.Drawing.Point(164, 55);
            this.btnPEL.Name = "btnPEL";
            this.btnPEL.Size = new System.Drawing.Size(100, 30);
            this.btnPEL.TabIndex = 31;
            this.btnPEL.Text = "PEL";
            this.btnPEL.UseVisualStyleBackColor = true;
            this.btnPEL.Click += new System.EventHandler(this.btnPEL_Click);
            // 
            // btnCanny
            // 
            this.btnCanny.Location = new System.Drawing.Point(164, 90);
            this.btnCanny.Name = "btnCanny";
            this.btnCanny.Size = new System.Drawing.Size(100, 30);
            this.btnCanny.TabIndex = 30;
            this.btnCanny.Text = "Canny";
            this.btnCanny.UseVisualStyleBackColor = true;
            this.btnCanny.Click += new System.EventHandler(this.btnCanny_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 30);
            this.button1.TabIndex = 29;
            this.button1.Text = "Reload";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnOtsuBinary
            // 
            this.btnOtsuBinary.Location = new System.Drawing.Point(164, 126);
            this.btnOtsuBinary.Name = "btnOtsuBinary";
            this.btnOtsuBinary.Size = new System.Drawing.Size(100, 30);
            this.btnOtsuBinary.TabIndex = 28;
            this.btnOtsuBinary.Text = "Otsu\'s Binary";
            this.btnOtsuBinary.UseVisualStyleBackColor = true;
            this.btnOtsuBinary.Click += new System.EventHandler(this.btnOtsuBinary_Click);
            // 
            // btnED
            // 
            this.btnED.Location = new System.Drawing.Point(164, 19);
            this.btnED.Name = "btnED";
            this.btnED.Size = new System.Drawing.Size(100, 30);
            this.btnED.TabIndex = 28;
            this.btnED.Text = "ED";
            this.btnED.UseVisualStyleBackColor = true;
            this.btnED.Click += new System.EventHandler(this.btnED_Click);
            // 
            // FrmSegmentation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 636);
            this.Controls.Add(this.panelInputImage);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gBoxCloseView);
            this.Name = "FrmSegmentation";
            this.Text = "Benchmark";
            this.Resize += new System.EventHandler(this.FrmSegmentation_Resize);
            this.panelInputImage.ResumeLayout(false);
            this.panelInputImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxInputImg)).EndInit();
            this.gBoxCloseView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxCloseView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Panel panelInputImage;
        private System.Windows.Forms.PictureBox pBoxInputImg;
        private System.Windows.Forms.GroupBox gBoxCloseView;
        private System.Windows.Forms.PictureBox pBoxCloseView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnOtsuBinary;
        private System.Windows.Forms.Button btnED;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCanny;
        private System.Windows.Forms.Button btnPEL;
    }
}
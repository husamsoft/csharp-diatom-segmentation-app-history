﻿namespace csharp_image_segmentation_app
{
    partial class FrmFeatureExtraction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpen = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pBoxInputImg = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblGlobalStatsInfo = new System.Windows.Forms.Label();
            this.lblGlobalRMax = new System.Windows.Forms.Label();
            this.lblGlobalRMin = new System.Windows.Forms.Label();
            this.pBoxGlobalHstg = new System.Windows.Forms.PictureBox();
            this.gBoxInfo = new System.Windows.Forms.GroupBox();
            this.lblLocalStatsInfo = new System.Windows.Forms.Label();
            this.lblLocalRMax = new System.Windows.Forms.Label();
            this.lblLocalRMin = new System.Windows.Forms.Label();
            this.pBoxLocalHstg = new System.Windows.Forms.PictureBox();
            this.tBarW = new System.Windows.Forms.TrackBar();
            this.lblW = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblLocalBinaryPatternStatsInfo = new System.Windows.Forms.Label();
            this.lblLbpMax = new System.Windows.Forms.Label();
            this.lblLbpMin = new System.Windows.Forms.Label();
            this.pBoxLocalBinaryPatternHstg = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblCooccuranceMatrixInfo = new System.Windows.Forms.Label();
            this.lblCooccuranceMatrixRMax = new System.Windows.Forms.Label();
            this.lblCooccuranceMatrixRMin = new System.Windows.Forms.Label();
            this.pBoxCooccuranceMatrix = new System.Windows.Forms.PictureBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblSimilarity = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxInputImg)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxGlobalHstg)).BeginInit();
            this.gBoxInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLocalHstg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBarW)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLocalBinaryPatternHstg)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxCooccuranceMatrix)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(34, 42);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(100, 40);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pBoxInputImg);
            this.panel1.Location = new System.Drawing.Point(6, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(608, 409);
            this.panel1.TabIndex = 2;
            // 
            // pBoxInputImg
            // 
            this.pBoxInputImg.BackColor = System.Drawing.Color.Black;
            this.pBoxInputImg.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pBoxInputImg.Location = new System.Drawing.Point(3, 6);
            this.pBoxInputImg.Name = "pBoxInputImg";
            this.pBoxInputImg.Size = new System.Drawing.Size(600, 400);
            this.pBoxInputImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBoxInputImg.TabIndex = 1;
            this.pBoxInputImg.TabStop = false;
            this.pBoxInputImg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBoxInputImg_MouseDown);
            this.pBoxInputImg.MouseEnter += new System.EventHandler(this.pBoxInputImg_MouseEnter);
            this.pBoxInputImg.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pBoxInputImg_MouseMove);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(4, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(618, 422);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblGlobalStatsInfo);
            this.groupBox2.Controls.Add(this.lblGlobalRMax);
            this.groupBox2.Controls.Add(this.lblGlobalRMin);
            this.groupBox2.Controls.Add(this.pBoxGlobalHstg);
            this.groupBox2.Location = new System.Drawing.Point(628, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(306, 237);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Global Histogram";
            // 
            // lblGlobalStatsInfo
            // 
            this.lblGlobalStatsInfo.Location = new System.Drawing.Point(5, 185);
            this.lblGlobalStatsInfo.Name = "lblGlobalStatsInfo";
            this.lblGlobalStatsInfo.Size = new System.Drawing.Size(244, 49);
            this.lblGlobalStatsInfo.TabIndex = 6;
            this.lblGlobalStatsInfo.Text = "label global stats info";
            // 
            // lblGlobalRMax
            // 
            this.lblGlobalRMax.Location = new System.Drawing.Point(272, 169);
            this.lblGlobalRMax.Name = "lblGlobalRMax";
            this.lblGlobalRMax.Size = new System.Drawing.Size(31, 16);
            this.lblGlobalRMax.TabIndex = 5;
            this.lblGlobalRMax.Text = "0";
            this.lblGlobalRMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGlobalRMin
            // 
            this.lblGlobalRMin.Location = new System.Drawing.Point(2, 169);
            this.lblGlobalRMin.Name = "lblGlobalRMin";
            this.lblGlobalRMin.Size = new System.Drawing.Size(31, 16);
            this.lblGlobalRMin.TabIndex = 4;
            this.lblGlobalRMin.Text = "0";
            this.lblGlobalRMin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBoxGlobalHstg
            // 
            this.pBoxGlobalHstg.BackColor = System.Drawing.Color.White;
            this.pBoxGlobalHstg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxGlobalHstg.Location = new System.Drawing.Point(4, 12);
            this.pBoxGlobalHstg.Name = "pBoxGlobalHstg";
            this.pBoxGlobalHstg.Size = new System.Drawing.Size(300, 150);
            this.pBoxGlobalHstg.TabIndex = 0;
            this.pBoxGlobalHstg.TabStop = false;
            // 
            // gBoxInfo
            // 
            this.gBoxInfo.Controls.Add(this.lblLocalStatsInfo);
            this.gBoxInfo.Controls.Add(this.lblLocalRMax);
            this.gBoxInfo.Controls.Add(this.lblLocalRMin);
            this.gBoxInfo.Controls.Add(this.pBoxLocalHstg);
            this.gBoxInfo.Location = new System.Drawing.Point(938, 1);
            this.gBoxInfo.Name = "gBoxInfo";
            this.gBoxInfo.Size = new System.Drawing.Size(306, 237);
            this.gBoxInfo.TabIndex = 10;
            this.gBoxInfo.TabStop = false;
            this.gBoxInfo.Text = "Local Histogram";
            // 
            // lblLocalStatsInfo
            // 
            this.lblLocalStatsInfo.Location = new System.Drawing.Point(5, 185);
            this.lblLocalStatsInfo.Name = "lblLocalStatsInfo";
            this.lblLocalStatsInfo.Size = new System.Drawing.Size(244, 49);
            this.lblLocalStatsInfo.TabIndex = 6;
            this.lblLocalStatsInfo.Text = "label local stats info";
            // 
            // lblLocalRMax
            // 
            this.lblLocalRMax.Location = new System.Drawing.Point(272, 169);
            this.lblLocalRMax.Name = "lblLocalRMax";
            this.lblLocalRMax.Size = new System.Drawing.Size(31, 16);
            this.lblLocalRMax.TabIndex = 5;
            this.lblLocalRMax.Text = "0";
            this.lblLocalRMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLocalRMin
            // 
            this.lblLocalRMin.Location = new System.Drawing.Point(2, 169);
            this.lblLocalRMin.Name = "lblLocalRMin";
            this.lblLocalRMin.Size = new System.Drawing.Size(31, 16);
            this.lblLocalRMin.TabIndex = 4;
            this.lblLocalRMin.Text = "0";
            this.lblLocalRMin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBoxLocalHstg
            // 
            this.pBoxLocalHstg.BackColor = System.Drawing.Color.White;
            this.pBoxLocalHstg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxLocalHstg.Location = new System.Drawing.Point(3, 16);
            this.pBoxLocalHstg.Name = "pBoxLocalHstg";
            this.pBoxLocalHstg.Size = new System.Drawing.Size(300, 150);
            this.pBoxLocalHstg.TabIndex = 0;
            this.pBoxLocalHstg.TabStop = false;
            // 
            // tBarW
            // 
            this.tBarW.Location = new System.Drawing.Point(254, 49);
            this.tBarW.Maximum = 60;
            this.tBarW.Minimum = 10;
            this.tBarW.Name = "tBarW";
            this.tBarW.Size = new System.Drawing.Size(173, 45);
            this.tBarW.TabIndex = 11;
            this.tBarW.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.tBarW.Value = 20;
            this.tBarW.Scroll += new System.EventHandler(this.tBarW_Scroll);
            // 
            // lblW
            // 
            this.lblW.AutoSize = true;
            this.lblW.Location = new System.Drawing.Point(205, 55);
            this.lblW.Name = "lblW";
            this.lblW.Size = new System.Drawing.Size(43, 26);
            this.lblW.TabIndex = 12;
            this.lblW.Text = "w = 20 \r\nap = 41";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnRun);
            this.groupBox3.Controls.Add(this.btnClear);
            this.groupBox3.Controls.Add(this.lblW);
            this.groupBox3.Controls.Add(this.tBarW);
            this.groupBox3.Controls.Add(this.btnOpen);
            this.groupBox3.Location = new System.Drawing.Point(13, 446);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(609, 121);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Controls";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblLocalBinaryPatternStatsInfo);
            this.groupBox4.Controls.Add(this.lblLbpMax);
            this.groupBox4.Controls.Add(this.lblLbpMin);
            this.groupBox4.Controls.Add(this.pBoxLocalBinaryPatternHstg);
            this.groupBox4.Location = new System.Drawing.Point(628, 238);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(306, 237);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Local Binary Pattern Histogram";
            // 
            // lblLocalBinaryPatternStatsInfo
            // 
            this.lblLocalBinaryPatternStatsInfo.Location = new System.Drawing.Point(5, 185);
            this.lblLocalBinaryPatternStatsInfo.Name = "lblLocalBinaryPatternStatsInfo";
            this.lblLocalBinaryPatternStatsInfo.Size = new System.Drawing.Size(244, 49);
            this.lblLocalBinaryPatternStatsInfo.TabIndex = 6;
            this.lblLocalBinaryPatternStatsInfo.Text = "label local stats info";
            // 
            // lblLbpMax
            // 
            this.lblLbpMax.Location = new System.Drawing.Point(272, 169);
            this.lblLbpMax.Name = "lblLbpMax";
            this.lblLbpMax.Size = new System.Drawing.Size(31, 16);
            this.lblLbpMax.TabIndex = 5;
            this.lblLbpMax.Text = "0";
            this.lblLbpMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLbpMin
            // 
            this.lblLbpMin.Location = new System.Drawing.Point(2, 169);
            this.lblLbpMin.Name = "lblLbpMin";
            this.lblLbpMin.Size = new System.Drawing.Size(31, 16);
            this.lblLbpMin.TabIndex = 4;
            this.lblLbpMin.Text = "0";
            this.lblLbpMin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBoxLocalBinaryPatternHstg
            // 
            this.pBoxLocalBinaryPatternHstg.BackColor = System.Drawing.Color.White;
            this.pBoxLocalBinaryPatternHstg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxLocalBinaryPatternHstg.Location = new System.Drawing.Point(3, 16);
            this.pBoxLocalBinaryPatternHstg.Name = "pBoxLocalBinaryPatternHstg";
            this.pBoxLocalBinaryPatternHstg.Size = new System.Drawing.Size(300, 150);
            this.pBoxLocalBinaryPatternHstg.TabIndex = 0;
            this.pBoxLocalBinaryPatternHstg.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblCooccuranceMatrixInfo);
            this.groupBox5.Controls.Add(this.lblCooccuranceMatrixRMax);
            this.groupBox5.Controls.Add(this.lblCooccuranceMatrixRMin);
            this.groupBox5.Controls.Add(this.pBoxCooccuranceMatrix);
            this.groupBox5.Location = new System.Drawing.Point(937, 238);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(306, 237);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "TODO";
            // 
            // lblCooccuranceMatrixInfo
            // 
            this.lblCooccuranceMatrixInfo.Location = new System.Drawing.Point(5, 185);
            this.lblCooccuranceMatrixInfo.Name = "lblCooccuranceMatrixInfo";
            this.lblCooccuranceMatrixInfo.Size = new System.Drawing.Size(244, 49);
            this.lblCooccuranceMatrixInfo.TabIndex = 6;
            this.lblCooccuranceMatrixInfo.Text = "label local stats info";
            // 
            // lblCooccuranceMatrixRMax
            // 
            this.lblCooccuranceMatrixRMax.Location = new System.Drawing.Point(272, 169);
            this.lblCooccuranceMatrixRMax.Name = "lblCooccuranceMatrixRMax";
            this.lblCooccuranceMatrixRMax.Size = new System.Drawing.Size(31, 16);
            this.lblCooccuranceMatrixRMax.TabIndex = 5;
            this.lblCooccuranceMatrixRMax.Text = "0";
            this.lblCooccuranceMatrixRMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCooccuranceMatrixRMin
            // 
            this.lblCooccuranceMatrixRMin.Location = new System.Drawing.Point(2, 169);
            this.lblCooccuranceMatrixRMin.Name = "lblCooccuranceMatrixRMin";
            this.lblCooccuranceMatrixRMin.Size = new System.Drawing.Size(31, 16);
            this.lblCooccuranceMatrixRMin.TabIndex = 4;
            this.lblCooccuranceMatrixRMin.Text = "0";
            this.lblCooccuranceMatrixRMin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBoxCooccuranceMatrix
            // 
            this.pBoxCooccuranceMatrix.BackColor = System.Drawing.Color.White;
            this.pBoxCooccuranceMatrix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxCooccuranceMatrix.Location = new System.Drawing.Point(3, 16);
            this.pBoxCooccuranceMatrix.Name = "pBoxCooccuranceMatrix";
            this.pBoxCooccuranceMatrix.Size = new System.Drawing.Size(300, 150);
            this.pBoxCooccuranceMatrix.TabIndex = 0;
            this.pBoxCooccuranceMatrix.TabStop = false;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(459, 65);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(100, 40);
            this.btnClear.TabIndex = 13;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(459, 19);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(100, 40);
            this.btnRun.TabIndex = 14;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.lblSimilarity);
            this.groupBox6.Location = new System.Drawing.Point(628, 481);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(306, 100);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Info";
            // 
            // lblSimilarity
            // 
            this.lblSimilarity.Location = new System.Drawing.Point(59, 30);
            this.lblSimilarity.Name = "lblSimilarity";
            this.lblSimilarity.Size = new System.Drawing.Size(100, 17);
            this.lblSimilarity.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Similarity";
            // 
            // FrmFeatureExtraction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1251, 615);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gBoxInfo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmFeatureExtraction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Segmentation App";
            this.Load += new System.EventHandler(this.FrmFeatureExtraction_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxInputImg)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxGlobalHstg)).EndInit();
            this.gBoxInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLocalHstg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBarW)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLocalBinaryPatternHstg)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxCooccuranceMatrix)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pBoxInputImg;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblGlobalStatsInfo;
        private System.Windows.Forms.Label lblGlobalRMax;
        private System.Windows.Forms.Label lblGlobalRMin;
        private System.Windows.Forms.PictureBox pBoxGlobalHstg;
        private System.Windows.Forms.GroupBox gBoxInfo;
        private System.Windows.Forms.Label lblLocalStatsInfo;
        private System.Windows.Forms.Label lblLocalRMax;
        private System.Windows.Forms.Label lblLocalRMin;
        private System.Windows.Forms.PictureBox pBoxLocalHstg;
        private System.Windows.Forms.TrackBar tBarW;
        private System.Windows.Forms.Label lblW;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblLocalBinaryPatternStatsInfo;
        private System.Windows.Forms.Label lblLbpMax;
        private System.Windows.Forms.Label lblLbpMin;
        private System.Windows.Forms.PictureBox pBoxLocalBinaryPatternHstg;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblCooccuranceMatrixInfo;
        private System.Windows.Forms.Label lblCooccuranceMatrixRMax;
        private System.Windows.Forms.Label lblCooccuranceMatrixRMin;
        private System.Windows.Forms.PictureBox pBoxCooccuranceMatrix;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSimilarity;
    }
}


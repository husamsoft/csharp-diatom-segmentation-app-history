﻿namespace csharp_image_segmentation_app.Tools
{
    partial class FrmBenchmark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBoundaryBenchmark = new System.Windows.Forms.Button();
            this.lblProcessed = new System.Windows.Forms.Label();
            this.bgwBenchmark = new System.ComponentModel.BackgroundWorker();
            this.panelInputImage = new System.Windows.Forms.Panel();
            this.pBoxInputImg = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelInputImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxInputImg)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBoundaryBenchmark
            // 
            this.btnBoundaryBenchmark.Location = new System.Drawing.Point(8, 236);
            this.btnBoundaryBenchmark.Name = "btnBoundaryBenchmark";
            this.btnBoundaryBenchmark.Size = new System.Drawing.Size(100, 51);
            this.btnBoundaryBenchmark.TabIndex = 0;
            this.btnBoundaryBenchmark.Text = "Boundary Benchmark";
            this.btnBoundaryBenchmark.UseVisualStyleBackColor = true;
            this.btnBoundaryBenchmark.Click += new System.EventHandler(this.btnBoundaryBenchmark_Click);
            // 
            // lblProcessed
            // 
            this.lblProcessed.BackColor = System.Drawing.Color.Cornsilk;
            this.lblProcessed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProcessed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessed.Location = new System.Drawing.Point(114, 236);
            this.lblProcessed.Name = "lblProcessed";
            this.lblProcessed.Size = new System.Drawing.Size(45, 51);
            this.lblProcessed.TabIndex = 2;
            this.lblProcessed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelInputImage
            // 
            this.panelInputImage.AutoScroll = true;
            this.panelInputImage.Controls.Add(this.pBoxInputImg);
            this.panelInputImage.Location = new System.Drawing.Point(194, 11);
            this.panelInputImage.Name = "panelInputImage";
            this.panelInputImage.Size = new System.Drawing.Size(814, 612);
            this.panelInputImage.TabIndex = 3;
            // 
            // pBoxInputImg
            // 
            this.pBoxInputImg.BackColor = System.Drawing.Color.Black;
            this.pBoxInputImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxInputImg.Location = new System.Drawing.Point(8, 6);
            this.pBoxInputImg.Name = "pBoxInputImg";
            this.pBoxInputImg.Size = new System.Drawing.Size(800, 600);
            this.pBoxInputImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBoxInputImg.TabIndex = 0;
            this.pBoxInputImg.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblProcessed);
            this.groupBox1.Controls.Add(this.btnBoundaryBenchmark);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 609);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // FrmBenchmark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 633);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelInputImage);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBenchmark";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Boundary Benchmark";
            this.panelInputImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxInputImg)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBoundaryBenchmark;
        private System.Windows.Forms.Label lblProcessed;
        private System.ComponentModel.BackgroundWorker bgwBenchmark;
        private System.Windows.Forms.Panel panelInputImage;
        private System.Windows.Forms.PictureBox pBoxInputImg;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
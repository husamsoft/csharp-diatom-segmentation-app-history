﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace csharp_image_segmentation_app
{
    public unsafe partial class FrmFeatureExtraction : Form
    {
        private string filePath;

        private Bitmap bmpInputImg, bmpDisImg, bmpSegImg;

        private Bitmap bmpGlobalHstg, bmpLocalHstg, bmpLocalBinaryPatternHstg;

        private int imgW, imgH, imgSize;

        private int w, ap;

        private byte[] imgData;

        private int sX, sY;

        private byte[] sData;

        public FrmFeatureExtraction()
        {
            InitializeComponent();

            Initialize();

            UpdateCrosshair();
        }

        private void Initialize()
        {
            bmpLocalHstg = new Bitmap(pBoxLocalHstg.Width, pBoxLocalHstg.Height);
            bmpGlobalHstg = new Bitmap(pBoxGlobalHstg.Width, pBoxGlobalHstg.Height);
            bmpLocalBinaryPatternHstg = new Bitmap(pBoxGlobalHstg.Width, pBoxGlobalHstg.Height);

            w = tBarW.Value;

            ap = 2 * w + 1;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openImageDialog = new OpenFileDialog();

            if (openImageDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            filePath = openImageDialog.FileName;

            bmpInputImg = (Bitmap)Image.FromFile(filePath);

            bmpDisImg = (Bitmap)bmpInputImg.Clone();

            if (bmpInputImg.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                MessageBox.Show("The color format of this image is " + bmpInputImg.PixelFormat.ToString() + ".\nPlease select a valid grayscale image.", "Exception");
                return;
            }

            imgW = bmpInputImg.Width;
            imgH = bmpInputImg.Height;
            imgSize = imgW * imgH;
            imgData = new byte[imgSize];

            Rectangle rect = new Rectangle(0, 0, imgW, imgH);
            BitmapData inputData = bmpInputImg.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            byte* scan0 = (byte*)inputData.Scan0;
            for (int i = 0; i < imgSize; i++)
            {
                imgData[i] = scan0[i];
            }

            bmpInputImg.UnlockBits(inputData);

            PlotGlobalHistogram();

            ExtractLbpFeature();

            pBoxInputImg.Image = bmpDisImg;
        }

        private void ExtractLbpFeature()
        {
            //LBP.ProcessImage(imgData, imgW, imgH);
            //patterns = LBP.Patterns;
        }

        private byte[] ConvertRegionToHistogramData(int x, int y)
        {
            byte[] tmpData = imgData;

            byte[] data = new byte[ap * ap];

            int k = 0;

            for (int i = -w; i <= w; i++)
                for (int j = -w; j <= w; j++)
                    data[k++] = tmpData[(y + i) * imgW + x + j];

            return data;
        }

        public byte[] ConvertRegionToLbpHistogramData(int x, int y)
        {
            byte[] data = new byte[ap * ap];

            int k = 0;

            for (int i = -w; i <= w; i++)
            {
                for (int j = -w; j <= w; j++)
                {
                    //int pattern = Convert.ToInt32(patterns[y + i, x + j]);
                    //data[k++] = LBP.MappingMap[pattern];
                }
            }

            return data;
        }

        private void EvaluateHistogram()
        {
            //To do : update the appropriate histogram
            Histogram hstgGlobal = new Histogram(imgData, 0, 255);
            DisplayHistogram(hstgGlobal.GetHistogram(), bmpGlobalHstg);
            pBoxGlobalHstg.Image = bmpGlobalHstg;

            lblGlobalRMin.Text = hstgGlobal.GetRMin().ToString();
            lblGlobalRMax.Text = hstgGlobal.GetRMax().ToString();
            lblGlobalStatsInfo.Text = "Mean     : " + hstgGlobal.GetMean().ToString("0.00") + "\nVariance: " + hstgGlobal.GetVariance().ToString("0.00") + "\nRange   : " + hstgGlobal.GetRange().ToString();

            this.Cursor = Cursors.Arrow;
        }

        private void pBoxInputImg_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X < w || e.Y < w || e.X > imgW - w - 1 || e.Y > imgH - w - 1)
                return;

            int x = e.X, y = e.Y, k = 0;

            LocateCrosshair((PictureBox)sender, x, y);

            PlotLocalHistogram(x, y);

            PlotLbpHistogram(x, y);

            MeasureSimilarity(x, y);
        }

        private void PlotLbpHistogram(int x, int y)
        {
            byte[] data = ConvertRegionToLbpHistogramData(x, y);

            Histogram tmpHstgLBP = new Histogram(data, -1, -1);
            DisplayHistogram(tmpHstgLBP.GetHistogram(), bmpLocalBinaryPatternHstg);
            pBoxLocalBinaryPatternHstg.Image = bmpLocalBinaryPatternHstg;

            lblLbpMin.Text = tmpHstgLBP.GetRMin().ToString();
            lblLbpMax.Text = tmpHstgLBP.GetRMax().ToString();
            lblLocalBinaryPatternStatsInfo.Text = "Mean     : " + tmpHstgLBP.GetMean().ToString("0.00") + "\nVariance: " + tmpHstgLBP.GetVariance().ToString("0.00") + "\nRange   : " + tmpHstgLBP.GetRange().ToString();
        }

        private void PlotLocalHistogram(int x, int y)
        {
            byte[] data = ConvertRegionToHistogramData(x, y);

            Histogram tmpHstg = new Histogram(data, -1, -1);
            DisplayHistogram(tmpHstg.GetHistogram(), bmpLocalHstg);
            pBoxLocalHstg.Image = bmpLocalHstg;

            lblLocalRMin.Text = tmpHstg.GetRMin().ToString();
            lblLocalRMax.Text = tmpHstg.GetRMax().ToString();
            lblLocalStatsInfo.Text = "Mean     : " + tmpHstg.GetMean().ToString("0.00") + "\nVariance: " + tmpHstg.GetVariance().ToString("0.00") + "\nRange   : " + tmpHstg.GetRange().ToString();
        }

        private void PlotGlobalHistogram()
        {
            Histogram hstgGlobal = new Histogram(imgData, 0, 255);
            DisplayHistogram(hstgGlobal.GetHistogram(), bmpGlobalHstg);
            pBoxGlobalHstg.Image = bmpGlobalHstg;

            lblGlobalRMin.Text = hstgGlobal.GetRMin().ToString();
            lblGlobalRMax.Text = hstgGlobal.GetRMax().ToString();
            lblGlobalStatsInfo.Text = "Mean     : " + hstgGlobal.GetMean().ToString("0.00") + "\nVariance: " + hstgGlobal.GetVariance().ToString("0.00") + "\nRange   : " + hstgGlobal.GetRange().ToString();

            this.Cursor = Cursors.Arrow;
        }

        private void DisplayHistogram(int[] hstg, Bitmap dest)
        {
            int range = hstg.Length - 1;
            float hMax = hstg[0];

            Graphics graphics = Graphics.FromImage(dest);
            graphics.Clear(Color.White);

            for (int i = 1; i <= range; i++)
                if (hMax < hstg[i]) hMax = hstg[i];

            hMax = hMax * 1.1f;
            float height = dest.Height - 2;
            float width = dest.Width - 3;
            float yRate = height / hMax;
            float xRate = width / range;

            Pen penRed = new Pen(Color.Red, 1.5f);
            Pen penRBlue = new Pen(Color.RoyalBlue, 1.5f);
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            for (int i = 0; i < range; i++)
                graphics.DrawLine(penRBlue, i * xRate, height, i * xRate, height - hstg[i] * yRate);

            for (int i = 0; i < range; i++)
                graphics.DrawLine(penRed, i * xRate, height - hstg[i] * yRate - 1, (i + 1) * xRate, height - hstg[i + 1] * yRate - 1);

            graphics.Dispose();
        }

        private void LocateCrosshair(PictureBox pBox, int x, int y)
        {
            if (pBox.Controls[0] == null) return;

            pBox.Controls[0].Left = x - w;
            pBox.Controls[0].Top = y - w;

            pBox.Controls[1].Left = x - w;
            pBox.Controls[1].Top = y - w;

            pBox.Controls[2].Left = x - w;
            pBox.Controls[2].Top = y + w;

            pBox.Controls[3].Left = x + w;
            pBox.Controls[3].Top = y - w;
        }

        private void UpdateCrosshair()
        {
            PictureBox pBox = pBoxInputImg;

            pBox.Controls.Clear();

            Label[] crosshair = new Label[4];
            for (int i = 0; i < 4; i++)
            {
                crosshair[i] = new Label();
                crosshair[i].Text = "";
                crosshair[i].BackColor = Color.Red;
                crosshair[i].Visible = true;

                if (i % 2 == 0)
                {
                    crosshair[i].Width = ap;
                    crosshair[i].Height = 1;
                }
                else
                {
                    crosshair[i].Width = 1;
                    crosshair[i].Height = ap;
                }

                pBox.Controls.Add(crosshair[i]);
            }
        }

        private void tBarW_Scroll(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            w = tBarW.Value;
            ap = 2 * w + 1;

            lblW.Text = "w = " + w.ToString() + "\nap = " + ap.ToString();

            UpdateCrosshair();
        }

        private void FrmFeatureExtraction_Load(object sender, EventArgs e)
        {
            //TestApp();
        }

        private void pBoxInputImg_MouseEnter(object sender, EventArgs e)
        {
        }

        private void pBoxInputImg_MouseDown(object sender, MouseEventArgs e)
        {
            sX = e.X;
            sY = e.Y;

            sData = new byte[ap * ap];

            sData = ConvertRegionToLbpHistogramData(sX, sY);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {

        }

        private void btnRun_Click(object sender, EventArgs e)
        {                        

        }

        private void MeasureSimilarity(int x, int y)
        {

            if (sData == null) return;
            
            byte[] temp = new byte[ap * ap];

            temp = ConvertRegionToLbpHistogramData(x, y);

            lblSimilarity.Text = Convert.ToString(Similarity(temp, sData));             
        }

        public double Similarity(byte[] p1, byte[] p2)
        {
            double[] dist = new double[p1.Length];

            for (int i = 0; i < p1.Length; i++)
                dist[i] = Math.Sqrt((p1[i] - p2[i]) * (p1[i] - p2[i]));

            double sum = 0;

            for (int i = 0; i < p1.Length; i++)
            {
                sum += dist[i];
            }


            double similarity = sum / p1.Length;

            return similarity;

        }
    }
}

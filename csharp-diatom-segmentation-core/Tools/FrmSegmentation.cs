﻿using csharp_image_segmentation_app.Model;
using csharp_image_segmentation_app.Sources.DataSet;
using csharp_image_segmentation_app.Sources.DetectionBenchmark;
using csharp_image_segmentation_app.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace csharp_image_segmentation_app.UI
{
    public partial class FrmSegmentation : Form
    {
        private SegmentationModel model;

        public FrmSegmentation()
        {
            InitializeComponent();
                      
            WindowState = FormWindowState.Maximized;

            ResizeAll();                        
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            var openImageDialog = new OpenFileDialog();

            if (openImageDialog.ShowDialog() == DialogResult.Cancel) return;

            var filePath = openImageDialog.FileName;

            var bmpRawImg = (Bitmap)Image.FromFile(filePath);

            if (bmpRawImg.PixelFormat != PixelFormat.Format24bppRgb)
            {
                MessageBox.Show("The color format of this image is " + bmpRawImg.PixelFormat.ToString() + ".\nPlease select a valid grayscale image.", "Exception");
                return;
            }

            bmpRawImg = Utils.BitmapRegulation(bmpRawImg);

            model = new SegmentationModel(bmpRawImg);

            UpdatePreview();
        }

        private void UpdatePreview()
        {
            pBoxInputImg.Image = model.bmpDisImg;
            pBoxInputImg.Update();
        }

        private void pBoxInputImg_MouseMove(object sender, MouseEventArgs e)
        {
            if (model == null) return;

            model.RenderCloseView(e.X, e.Y);

            pBoxCloseView.Image = model.bmpCloseViewImg;                     
        }
        
        private void FrmSegmentation_Resize(object sender, EventArgs e)
        {
            ResizeAll();
        }

        private void ResizeAll()
        {
            panelInputImage.Size = new Size(this.ClientRectangle.Width - (pBoxCloseView.Width + 50), this.ClientRectangle.Height - 50);           
            pBoxInputImg.Size = new Size(panelInputImage.Size.Width - 8, panelInputImage.Size.Height - 5);
        }

        private void btnED_Click(object sender, EventArgs e)
        {
            model.EdEdgeImage();
            UpdatePreview();        }

        private void btnOtsuBinary_Click(object sender, EventArgs e)
        {
            model.BinarizeImageByOtsu();
            UpdatePreview();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            model.ReloadImage();
            UpdatePreview();
        }

        private void btnCanny_Click(object sender, EventArgs e)
        {
            model.CannyEdgeImage();
            UpdatePreview();
        }

        private void btnPEL_Click(object sender, EventArgs e)
        {
            model.PelEdgeMap(36, 100);
            UpdatePreview();
        }

        private void btnDetectionBenchmark_Click(object sender, EventArgs e)
        {
          
            var folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.Cancel) return;

            var path = folderBrowserDialog.SelectedPath;

            var dataset = new ReadDataset(path);

            foreach (var dsi in dataset.DataSetTestImages)
            {
                var dir = dsi.GetRoot() + Path.DirectorySeparatorChar + GlobalParams.FOLDER_IMAGE + Path.DirectorySeparatorChar + dsi.GetName() + GlobalParams.ExtensionImage;

                var bitmap = (Bitmap)Bitmap.FromFile(dir);

                var bed = new SegmentByED(bitmap, dsi.Annotations, 24);

                var edgeBitmap = bed.ToEdgeBitmap();

                dir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + "temp/" + dsi.GetName() + ".bmp";

                edgeBitmap.Save(dir, ImageFormat.Bmp);                                    
            }
        }

        private void btnSaveGT_Click(object sender, EventArgs e)
        {
            CopyImages(); 
        }

        private void CreateLabels()
        {
            Random rand = new Random();

            var setTest = new HashSet<string>();

            using (StreamWriter writetext = new StreamWriter(@"C:\Users\admin\Desktop\iids_test.txt"))
            {
                for (int i = 0; i < 200; i++)
                {
                    var s = "";
                    do {

                        s = rand.Next(1, 2828).ToString("000000");
                    }
                    while (setTest.Contains(s));
                    setTest.Add(s);

                    writetext.WriteLine(s);
                }
            }
            var setTrain = new HashSet<string>();

            using (StreamWriter writetext = new StreamWriter(@"C:\Users\admin\Desktop\iids_train.txt"))
            {
                for (int i = 0; i < 300; i++)
                {
                    var iid = "";
                    do
                    {
                        iid = rand.Next(1, 2828).ToString("000000");
                    }
                    while (setTest.Contains(iid) || setTrain.Contains(iid));

                    setTrain.Add(iid);
                    writetext.WriteLine(iid);
                }
            }

            foreach(string s in setTrain)
            {
                if (setTest.Contains(s))
                {
                    Console.WriteLine("HATA");
                }
            }
        }

        private void CopyImages()
        {
            var rootSrc = @"E:\Diatom Dataset\all\human_labels";
            var rootDst = @"C:\Users\admin\Desktop\temp2";
            var dirIids = @"C:\Users\admin\Desktop\iids_test.txt";

            string[] lines = null;

            try
            {
                lines = File.ReadAllLines(dirIids);
            }
            catch
            {
                throw new Exception(" Cannot read label file!");
            }

            foreach (var line in lines)
            {
                var filename = line + ".txt";

                File.Copy(rootSrc + "/" + filename, rootDst + "/" + filename);
            }
        }
    }
}

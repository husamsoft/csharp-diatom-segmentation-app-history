#include <stdio.h>
#include <string.h>
#include "csharp-segmentation-helper.h"

ROI *ExtractROI(unsigned char *srcImg, int width, int height, Pixel *pixels, int noPixels);   // Using an edge segment

///----------------------------------------------------------------------------------------
/// Extracts the ROI from within srcImg bounded by the closed contour in boundaryImg
/// A boundary pixel in boundaryImg is marked, whereas a non-boundary pixel is set to 0
///
void ExtractROIPixels(unsigned char *srcImg, int width, int height, ROI *roi){
	int left = roi->left;
	int right = roi->right;
	int up = roi->up;
	int down = roi->down;

	int cx = roi->cx;
	int cy = roi->cy;

	int w = roi->width;
	int h = roi->height;

	unsigned char *roiImg = roi->roiImg;

	// Now do Breath first search starting at (cx, cy) and find the pixels inside the roi
	struct Pixel {
		int r, c;
	};

	Pixel *Q = new Pixel[w*h];
	int front = 0;
	int rear = 0;

	Q[rear].r = cy;
	Q[rear].c = cx;
	rear++;

	while (front < rear){
		int r = Q[front].r;
		int c = Q[front].c;
		front++;

		roiImg[(r - up)*w + c - left] = 255; // Mark the pixel as inside

		// Add the neighbors to the Queue
		int nr, nc;

		// Left neighbor
		nr = r; nc = c - 1;
		if (nc >= left && roiImg[(nr - up)*w + nc - left] == 0){ Q[rear].r = nr; Q[rear].c = nc; rear++; roiImg[(nr - up)*w + nc - left] = 255; }

		// Right neighbor
		nr = r; nc = c + 1;
		if (nc <= right && roiImg[(nr - up)*w + nc - left] == 0){ Q[rear].r = nr; Q[rear].c = nc; rear++; roiImg[(nr - up)*w + nc - left] = 255; }

		// Up neighbor
		nr = r - 1; nc = c;
		if (nr >= up && roiImg[(nr - up)*w + nc - left] == 0){ Q[rear].r = nr; Q[rear].c = nc; rear++; roiImg[(nr - up)*w + nc - left] = 255; }

		// Down neighbor
		nr = r + 1; nc = c;
		if (nr <= down && roiImg[(nr - up)*w + nc - left] == 0){ Q[rear].r = nr; Q[rear].c = nc; rear++; roiImg[(nr - up)*w + nc - left] = 255; }
	} //end-while

	delete Q;

	// Now walk rwo by row. Copy the pixels from the srcImg to the roiImg
	for (int r = up; r <= down; r++){
		for (int c = left; c <= right; c++){
			if (roiImg[(r - up)*w + c - left]) roiImg[(r - up)*w + c - left] = srcImg[r*width + c];
		} //end-for
	} // end-for
} //end-ExtractROIPixels

///------------------------------------------------------------------------------------------------------------
/// Extract the region of interest (ROI) enclosed by a closed edge segment, i.e., a chain of pixels
///
ROI *ExtractROI(unsigned char *srcImg, int width, int height, Pixel *pixels, int noPixels){
	// First find the bounding box, and (cx, cy) of the polygon
	int cx = 0;
	int cy = 0;

	int left = width, right = 0, up = height, down = 0;
	for (int i = 0; i<noPixels; i++){
		if (pixels[i].c < left) left = pixels[i].c;
		else if (pixels[i].c > right) right = pixels[i].c;

		if (pixels[i].r < up) up = pixels[i].r;
		else if (pixels[i].r > down) down = pixels[i].r;

		cx += pixels[i].c;
		cy += pixels[i].r;
	} //end-for

	cx /= noPixels;
	cy /= noPixels;

	// Bounding box
	int w = right - left + 1;
	int h = down - up + 1;

	// Allocate the Roi structure, initialize its contents
	ROI *roi = new ROI();

	// Allocate the roi image and set all pixels to 0
	unsigned char *roiImg = new unsigned char[w*h];
	memset(roiImg, 0, w*h);

	roi->roiImg = roiImg;
	roi->width = w;
	roi->height = h;
	roi->left = left;
	roi->right = right;
	roi->up = up;
	roi->down = down;
	roi->cx = cx;
	roi->cy = cy;

	// Set all boundary pixels to 255
	for (int i = 0; i<noPixels; i++){
		roiImg[(pixels[i].r - up)*w + pixels[i].c - left] = 255;
	} //end-for

	// Extract roi pixels now
	ExtractROIPixels(srcImg, width, height, roi);

	return roi;
} //end-ExtractROI

void main(void)
{


}


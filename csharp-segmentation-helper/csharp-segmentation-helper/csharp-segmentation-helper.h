#ifndef _CSHARP_SEGMENTATION_HELPER H_
#define _CSHARP_SEGMENTATION_HELPER H_


struct Pixel { int r, c; };

struct ROI {
	unsigned char *roiImg;       // Buffer for the extracted image
	int width, height;           // width, height of the extracted image (outImg)
	int left, right, up, down;   // Bounding box coordinates within the original image
	int cx, cy;                  // Center x, y coordinates

	~ROI(){ delete roiImg; }
};

#endif